/***
 *  $Id$
 **
 *  File: cache-perf-test.cpp
 *  Created: Feb 23, 2024
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2024 Jaroslaw Zola
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#include <algorithm>
#include <iostream>
#include <random>

#include <dnasbt/dnasbt_memory_manager.hpp>
#include <dnasbt/dnasbt_storage_manager.hpp>


struct Foo {
    uint8_t buf[1024];
}; // struct Foo


int main(int argc, char* argv[]) {
    constexpr int NQ = 2000000;

    auto size = 1024ULL * 1024 * 1024 * 1024;
    auto capacity = 1024 * 1024;

    using storage_manager_type = sbt::null_storage_manager<Foo>;
    storage_manager_type storage(size);

    sbt::cache_memory_manager<storage_manager_type> cache(capacity, std::move(storage));

    #pragma omp parallel default(none) firstprivate(size, capacity) shared(cache, std::cout)
    #pragma omp single
    #pragma omp taskloop num_tasks(16)
    for (int i = 0; i < 16; ++i) {
        std::mt19937 gen(111 + i);

        //std::uniform_int_distribution<long long int> dist(0, size - 1);
        std::normal_distribution<double> dist(size >> 1, capacity >> 2);

        for (int j = 0; j < NQ; ++j) {
            auto key = std::min(std::max(0ULL, static_cast<unsigned long long int>(dist(gen))), size - 1);
            auto valh = cache[key];
        }
    } // for i


    return 0;
} // main
