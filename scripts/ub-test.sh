TEST="build/test/ub-tests --gtest_also_run_disabled_tests"

$TEST --gtest_filter=UB.*NullPtr
$TEST --gtest_filter=UB.*OutOfBounds
$TEST --gtest_filter=UB.*MemoryLeak
$TEST --gtest_filter=UB.*SignedOverflow
$TEST --gtest_filter=UB.*SignedUnderflow
$TEST --gtest_filter=UB.*UnsignedOverflow
$TEST --gtest_filter=UB.*UnsignedUnderflow
