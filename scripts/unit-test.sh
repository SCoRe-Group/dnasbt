#!/bin/bash

THIS_DIR=$(realpath $(dirname $0))
SRC_DIR=$(realpath $THIS_DIR/../)
TOOLS_DIR=$(realpath $SRC_DIR/build/tools)
TEST_DIR=$(realpath $SRC_DIR/test)

export TOOLS_DIR=$TOOLS_DIR
export TEST_TMP_DIR=$(realpath $SRC_DIR/tmp/)
export TEST_DATA_DIR=$(realpath $TEST_DIR/data/)

$SRC_DIR/build/test/unit-tests "$@"
