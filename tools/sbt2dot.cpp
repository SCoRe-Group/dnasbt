/***
 *  $Id$
 **
 *  File: sbt2dot.cpp
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *
 *  Copyright (c) 2021 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#include <iostream>
#include <dnasbt/dnasbt.hpp>


int main(int argc, char* argv[]) {
    if (argc < 3) {
        std::cout << "usage: " << argv[0] << " sbt dot" << std::endl;
        return -1;
    }

    sbt::CSBTree<sbt::csbtree_suffixes_per_node<DNASBT_PAGE_SIZE>()> sbt;
    sbt::csbtree_to_dot<sbt.suffixes_per_node()>(argv[1], argv[2]);

    return 0;
} // main
