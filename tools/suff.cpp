/***
 *  $Id$
 **
 *  File: suff.cpp
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *
 *  Copyright (c) 2021 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#include <fstream>
#include <filesystem>
#include <iostream>
#include <string>
#include <vector>

#include <dnasbt/dnasbt.hpp>


template <typename T>
bool read_file(const std::string& name, std::vector<T>& buf) {
    auto sz = std::filesystem::file_size(name);
    buf.resize(sz / sizeof(T));

    std::ifstream f(name, std::ios_base::binary);
    f.read(reinterpret_cast<char*>(buf.data()), sz);

    return true;
} // read_file

int main(int argc, char* argv[]) {
    if (argc < 5) {
        std::cout << "usage: " << argv[0] << " text sa lcp len" << std::endl;
        return -1;
    }

    std::string text_name = argv[1];
    std::string sa_name = argv[2];
    std::string lcp_name = argv[3];
    std::string len_name = argv[4];

    std::vector<char> T;
    std::vector<sbt::offset_type> SA;
    std::vector<sbt::offset_type> LCP;
    std::vector<sbt::length_type> L;

    read_file(text_name, T);
    read_file(sa_name, SA);
    read_file(lcp_name, LCP);
    read_file(len_name, L);

    std::cout << "i\tSA\tLCP\tL\tT\n";

    for (unsigned long i = 0; i < SA.size(); ++i) {
        std::cout << i << "\t";
        std::cout << SA[i] << "\t";
        std::cout << LCP[i] << "\t";
        std::cout << L[SA[i]] << "\t";
        std::cout << std::string(T.data() + SA[i], T.data() + SA[i] + L[SA[i]]) << std::endl;
    }

    return 0;
} // main
