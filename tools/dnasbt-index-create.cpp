/***
 *  $Id$
 **
 *  File: dnasbt-index-create.cpp
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *
 *  Copyright (c) 2021 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#include <cxxopts/cxxopts.hpp>

#include <Logger.hpp>

#include <filesystem>
#include <vector>

#include <dnasbt/dnasbt.hpp>
#include <dnasbt/dnasbt_filenames.hpp>


template <typename T, typename Logger>
bool read_file(const std::string& name, std::vector<T>& data, Logger log) {
    std::error_code ec;
    auto sz = std::filesystem::file_size(name, ec);

    if (ec || (sz < 2)) {
        log->error("unable to stat {}!", name);
        return false;
    }

    data.resize(sz / sizeof(T));

    std::ifstream f(name);

    if (!f) {
        log->error("unable to open {}!", name);
        return false;
    }

    f.read(reinterpret_cast<char*>(data.data()), sz);

    if (!f) {
        log->error("unable to read {}!", name);
        return -1;
    }

    f.close();

    return true;
} // read_file


int main(int argc, char* argv[]) {
    Logger::init();
    auto log = spdlog::stdout_color_st("dnasbt-index-create");
    spdlog::cfg::load_env_levels();

    log->info("{} ver. {}", argv[0], DNASBT_VERSION);

    std::string out_name;
    int limit = 15;
    bool use_mmap = false;
    int cache = 4;

    try {
        cxxopts::Options options(argv[0]);

        options.add_options()
            ("o,out", "output database name", cxxopts::value<std::string>(out_name))
            ("l,limit", "index only suffixes longer than limit", cxxopts::value<int>(limit)->default_value("15"))
            ("m,mmap", "use memory mapping for SA and LCP", cxxopts::value<bool>(use_mmap)->default_value("false"))
            ("c,cache", "use cache GBs to cache tree nodes", cxxopts::value<int>(cache)->default_value("4"))
            ("h,help", "print this help")
            ;

        auto opt_res = options.parse(argc, argv);

        if (!opt_res.count("out") || opt_res.count("help")) {
            std::cout << options.help() << std::endl;
            return 0;
        }

        if (argc != 1) throw cxxopts::OptionException("Unrecognized options");
    } catch (const cxxopts::OptionException& e) {
        log->error("{}", e.what());
        return -1;
    }


    if (limit < 1) {
        log->error("limit parameter too small ({}), can't index nonexistent suffixes...", limit);
        return -1;
    }

    if (limit < 4) {
        log->warn("limit parameter is pretty small ({}), are you sure it makes sense?", limit);
    }

    if (cache < 4) {
        log->error("cache parameter too small ({}), can't you spare at least 4GB?", cache);
        return -1;
    }

    if (DNASBT_PAGE_SIZE < DNASBT_DEV_BLOCK_SIZE) {
        log->warn("page size is very small ({}B), are you sure it makes sense?", DNASBT_PAGE_SIZE);
    }

    // we handle file errors without dealing with exceptions
    sbt::CSBTree<sbt::csbtree_suffixes_per_node<DNASBT_PAGE_SIZE>()> sbt;

    log->info("creating index, limit parameter {}...", limit);
    log->info("requested page size: {}B, actual sbt node size: {}B", DNASBT_PAGE_SIZE, sbt.node_size());
    log->info("packing b={} suffixes per sbt node", sbt.suffixes_per_node());

    // we need both text and suffixes length directly in the memory
    // because they will be random accessed during sbt construction

    // 1. get text into memory
    log->info("reading text...");

    std::vector<char> T;
    if (!read_file(sbt::text_name(out_name), T, log)) return -1;

    log->info("text ready for processing!");


    // 2. get suffixes length into memory
    log->info("reading lengths of suffixes...");

    std::vector<sbt::length_type> L;
    if (!read_file(sbt::len_name(out_name), L, log)) return -1;

    log->info("lengths ready for processing!");


    // 3. let's get to the construction business
    log->info("ready to construct csbtree...");

    int tree_size = 0;

    if (use_mmap) {
        log->info("constructing csbtree with memory mapping...");

        tree_size = sbt::create_csbtree<sbt.suffixes_per_node()>(sbt::csbt_name(out_name),
                                                                sbt::dsuf_name(out_name),
                                                                sbt::info_name(out_name),
                                                                T.data(),
                                                                L.data(),
                                                                sbt::sa_name(out_name),
                                                                sbt::lcp_name(out_name),
                                                                limit,
                                                                1LL * cache * 1024 * 1024 * 1024,
                                                                log);
    } else {
        // we need sa and lcp in memory
        log->info("reading suffix array...");

        std::vector<sbt::offset_type> SA;
        if (!read_file(sbt::sa_name(out_name), SA, log)) return -1;

        sbt::offset_type l = std::filesystem::file_size(sbt::sa_name(out_name)) / sizeof(sbt::offset_type);

        log->info("suffix array ready, {} suffixes!", l);


        log->info("reading lcp array...");

        std::vector<sbt::offset_type> LCP;
        if (!read_file(sbt::lcp_name(out_name), LCP, log)) return -1;

        log->info("lcp array ready!");

        log->info("constructing csbtree...");

        tree_size = sbt::create_csbtree<sbt.suffixes_per_node()>(sbt::csbt_name(out_name),
                                                                sbt::dsuf_name(out_name),
                                                                sbt::info_name(out_name),
                                                                T.data(),
                                                                L.data(),
                                                                SA.data(),
                                                                LCP.data(),
                                                                l,
                                                                limit,
                                                                1LL * cache * 1024 * 1024 * 1024,
                                                                log);
    }

    if (tree_size < 0) {
        log->error("unable to construct csbtree!");
        return -1;
    }

    log->info("csbtree with {} nodes ready!", tree_size);

    // dump final csbtree for debugging
    // sbt::csbtree_to_dot<sbt.b>(sbt_name(out_name), "sbtdump.dot");

    // we are done!!!
    log->info("done!");

    return 0;
} // main
