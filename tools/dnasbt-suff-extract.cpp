/***
 *  $Id$
 **
 *  File: dnasbt-suff-extract.cpp
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *
 *  Copyright (c) 2021 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#include <bio/fastx_iterator.hpp>
#include <cxxopts/cxxopts.hpp>
#include <jaz/algorithm.hpp>

#include <Logger.hpp>
#include <lcp.hpp>

#include <dnasbt/dnasbt.hpp>
#include <dnasbt/dnasbt_filenames.hpp>
#include <dnasbt/detail/utils.hpp>

#include <divsufsort64.h>

#include <string>
#include <vector>


int main(int argc, char* argv[]) {
    Logger::init();
    auto log = spdlog::stdout_color_st("dnasbt-suff-extract");
    spdlog::cfg::load_env_levels();

    log->info("{} ver. {}", argv[0], DNASBT_VERSION);

    std::string out_name;

    try {
        cxxopts::Options options(argv[0]);

        options.add_options()
            ("o,out", "output database name", cxxopts::value<std::string>(out_name))
            ("h,help", "print this help")
            ;

        auto opt_res = options.parse(argc, argv);

        if (!(opt_res.count("out")) || opt_res.count("help")) {
            std::cout << options.help() << std::endl;
            return 0;
        }

        if (argc != 1) throw cxxopts::OptionException("Unrecognized options");
    } catch (const cxxopts::OptionException& e) {
        log->error("{}", e.what());
        return -1;
    }


    // 1. get text into memory
    log->info("reading text...");

    std::error_code ec;

    // get number of sequences in the input
    // get text into memory
    auto l = std::filesystem::file_size(sbt::text_name(out_name), ec);

    if (ec || (l < 2)) {
        log->error("unable to stat input text!");
        return -1;
    }

    std::vector<char> T(l);

    std::ifstream f(sbt::text_name(out_name));
    if (!f) {
        log->error("unable to open input text!");
        return -1;
    }

    f.read(T.data(), l);
    if (!f) {
        log->error("unable to read input text!");
        return -1;
    }

    f.close();


    // 2. construct suffix array
    log->info("constructing suffix array...");
    log->info("size of sa index type: {}", sizeof(saidx64_t));

    auto sz = l * sizeof(saidx64_t);
    log->info("memory required: {}B ({})", sz, sbt::detail::format_size(sz));

    log->debug("allocating memory...");

    std::vector<saidx64_t> SA(l);

    log->debug("running divsufsort64...");
    log->info("please be patient...");

    if (divsufsort64((sauchar_t*)T.data(), SA.data(), l) != 0) {
        log->error("unable to construct suffix array!");
        return -1;
    }

    log->info("suffix array ready!");
    log->info("writing suffix array...");

    if (!sbt::detail::write_file(log, sbt::sa_name(out_name), reinterpret_cast<const char*>(SA.data()), l * sizeof(saidx64_t))) return -1;


    // 3. construct LCP
    log->info("mapping suffix array...");

    // we use SA to store LCP and memory map the actual SA to sa
    // for lcp construction we use currently NAIVE parallel lcp
    // this is because it seems to work well with SA in secondary storage
    // due to continuous access and parallelization, also because
    // we are dealing with generalized SA (where lcps are fairly short)
    // and I am too lazy to implement something proper
    io::mapped_file_source SA_mf;

    try {
        SA_mf.open(sbt::sa_name(out_name));
    } catch (...) {
        log->error("unable to map suffix array!");
        return -1;
    }

    const sbt::offset_type* sa = reinterpret_cast<const sbt::offset_type*>(SA_mf.data());

    log->info("constructing lcp array...");

    dna_naive_lcp(T.data(), sa, SA.data(), l);

    log->info("lcp array ready!");
    log->info("writing lcp array...");

    if (!sbt::detail::write_file(log, sbt::lcp_name(out_name), reinterpret_cast<const char*>(SA.data()), l * sizeof(saidx64_t))) return -1;

    log->info("done!");

    return 0;
} // main
