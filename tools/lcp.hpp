/***
 *  $Id$
 **
 *  File: lcp.hpp
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *
 *  Copyright (c) 2021 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#ifndef LCP_HPP
#define LCP_HPP

#include <dnasbt/dnasbt_helpers.hpp>
#include <divsufsort64.h>

#include <vector>


inline saidx64_t dna_lcp9_rank_next(const char* T, const saidx64_t* SA, saidx64_t* LCP, saidx64_t n) {
    const int SIGMA = 5;

    std::vector<saidx64_t> occ(SIGMA, 0);
    std::vector<saidx64_t> count(SIGMA, 0);

    // TODO: simple parallelization with OMP
    for (saidx64_t i = 0; i < n; ++i) occ[sbt::char2index(T[i])]++;
    for (int i = 1; i < SIGMA; ++i) count[i] = count[i - 1] + occ[i - 1];

    auto j = count[sbt::char2index(T[n - 1])]++;
    saidx64_t eos_pos = 0;

    for (saidx64_t i = 0; i < n; ++i) {
        if (SA[i] == 0) eos_pos = i;
        else {
            auto c = sbt::char2index(T[SA[i] - 1]);
            j = count[c]++;
            LCP[j] = i;
        }
    }

    return eos_pos;
} // dna_lcp9_rank_next

// direct implementation of the lcp9 algorithm by Manzini
inline void dna_lcp9(const char* T, const saidx64_t* SA, saidx64_t* LCP, saidx64_t n) {
    LCP[0] = 0;

    auto k = dna_lcp9_rank_next(T, SA, LCP, n);
    saidx64_t h = 0;

    for (saidx64_t i = 0; i < n; ++i) {
        auto next_k = LCP[k];
        if (k == 0) LCP[k] = -1;
        else {
            auto j = SA[k - 1];
            while ((i + h < n) && (j + h < n) && T[i + h] == T[j + h]) h++;
            LCP[k] = h;
        }
        if (h > 0) h--;
        k = next_k;
    }
} // dna_lcp9

inline void kasai_lcp(const char* T, const saidx64_t* SA, saidx64_t* LCP, saidx64_t* rank, saidx64_t n) {
    for (saidx64_t i = 0; i < n; ++i) rank[SA[i]] = i;

    LCP[0] = 0;
    saidx64_t h = 0;

    for (auto i = 0; i < n; ++i) {
        if (rank[i] != 0) {
            auto k = SA[rank[i] - 1];
            while (T[i + h] == T[k + h]) h++;
            LCP[rank[i]] = h;
            if (h > 0) h--;
        }
    }
} // kasai_lcp

inline void dna_naive_lcp(const char* T, const saidx64_t* SA, saidx64_t* LCP, saidx64_t n) {
    LCP[0] = 0;

    #pragma omp parallel for shared(T, SA, LCP, n) schedule(static)
    for (saidx64_t i = 1; i < n; ++i) {
        auto h = 0;

        auto j = SA[i - 1];
        auto k = SA[i];

        // we could use the first while variant when dealing with GSA
        while ((T[j + h] == T[k + h]) && (T[j + h] != '$') && (T[k + h] != '$')) h++;
        /* while (T[j + h] == T[k + h]) h++; */

        LCP[i] = h;
    }
} // dna_naive_lcp

#endif // LCP_HPP
