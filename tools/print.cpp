/***
 *  $Id$
 **
 *  File: print.cpp
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *
 *  Copyright (c) 2021 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#include <cinttypes>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

template <typename T>
bool print(const std::string& name) {
    auto sz = std::filesystem::file_size(name);
    std::vector<T> buf(sz / sizeof(T));
    std::ifstream f(name, std::ios_base::binary);
    f.read(reinterpret_cast<char*>(buf.data()), sz);
    std::copy(std::begin(buf), std::end(buf), std::ostream_iterator<T>(std::cout, "\n"));
    return true;
} // print

int main(int argc, char* argv[]) {
    if (argc < 2) {
        std::cout << "usage: " << argv[0] << " file [16|32|64]" << std::endl;
        return -1;
    }

    int size = 64;

    if (argc > 2) {
        size = std::atoi(argv[2]);
    }

    switch (size) {
      case 64:
          print<uint64_t>(argv[1]);
          break;
      case 32:
          print<uint32_t>(argv[1]);
          break;
      case 16:
          print<uint16_t>(argv[1]);
    }

    return 0;
} // main
