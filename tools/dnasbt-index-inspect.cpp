/***
 *  $Id$
 **
 *  File: dnasbt-index-inspect.cpp
 *  Author: Andrew Mikalsen <ajmikals@buffalo.edu>
 *
 *  Copyright (c) 2021 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#include <algorithm>
#include <filesystem>
#include <iterator>
#include <string>
#include <vector>

#include <cxxopts/cxxopts.hpp>

#include <Logger.hpp>
#include <dnasbt/dnasbt.hpp>
#include <dnasbt/dnasbt_filenames.hpp>
#include <dnasbt/dnasbt_memory_manager.hpp>
#include <dnasbt/dnasbt_storage_manager.hpp>


template <typename Index, typename Logger>
void find(Index& csbtree, const std::string& pattern, Logger log) {
    std::vector<typename Index::text_manager_type::size_type> occ;
    auto l = csbtree.find(pattern, std::back_inserter(occ));

    std::sort(occ.begin(), occ.end());

    std::string occ_str = "";
    for (auto& o : occ) {
        occ_str += std::to_string(o) + ", ";
    } // for o

    log->info("length:  {}", l);
    log->info("matches: {}", occ_str);
} // find

template <typename Index, typename Logger>
int run(const std::string& in, bool use_packed, const std::string& pattern, bool set_pattern, Logger log) {
    Index csbtree{};

    if (use_packed) {
        csbtree.load({sbt::text_name(in)}, {sbt::pcsbt_name(in)}, {sbt::dsuf_name(in)});
    } else {
        csbtree.load({sbt::text_name(in)}, {sbt::csbt_name(in)}, {sbt::dsuf_name(in)});
    }

    if (set_pattern) {
        find(csbtree, pattern, log);
    } else {
        std::string p{};
        while (std::cin >> p && std::cin && !std::cin.eof()) {
            find(csbtree, p, log);
        } // while cin
    }

    return 0;
} // run


int main(int argc, char* argv[]) {
    Logger::init();
    auto log = spdlog::stdout_color_st("dnasbt-index-inspect");
    spdlog::cfg::load_env_levels();

    log->info("{} ver. {}", argv[0], DNASBT_VERSION);

    std::string in;
    bool use_packed;
    std::string pattern;

    bool set_pattern = false;

    try {
        cxxopts::Options options(argv[0]);

        options.add_options()
            ("i,in", "input database name", cxxopts::value<std::string>(in))
            ("f,find", "find occurrences of a pattern", cxxopts::value<std::string>(pattern)->default_value(""))
            ("p,packed", "use packed index", cxxopts::value<bool>(use_packed))
            ("h,help", "print this help")
            ;

        auto opt_res = options.parse(argc, argv);

        if (opt_res.count("help") || !opt_res.count("in")) {
            std::cout << options.help() << std::endl;
            return 0;
        }

        if (opt_res.count("find")) {
            set_pattern = true;
        }

        if (argc != 1) throw cxxopts::OptionException("Unrecognized options");
    } catch (const cxxopts::OptionException& e) {
        log->error("{}", e.what());
        return -1;
    }

    if (use_packed) {
        log->debug("running queries with index compression...");

        using lz4_index_type = sbt::CSBTree<sbt::csbtree_suffixes_per_node<DNASBT_PAGE_SIZE>(),
                                            sbt::mmap_storage_manager,
                                            sbt::lz4_mmap_storage_manager,
                                            sbt::mmap_storage_manager,
                                            sbt::basic_memory_manager,
                                            sbt::basic_memory_manager,
                                            sbt::basic_memory_manager>;
        if (!sbt::csbtree_validate(sbt::pcsbt_name(in), sbt::info_name(in), DNASBT_PAGE_SIZE, true, log)) {
            log->error("validation failed for packed csbt file {} with sidecar file {}",
                       sbt::pcsbt_name(in), sbt::info_name(in));
            return -1;
        }
        return run<lz4_index_type>(in, use_packed, pattern, set_pattern, log);
    } else {
        log->debug("running queries with no index compression...");

        using mmap_index_type = sbt::CSBTree<sbt::csbtree_suffixes_per_node<DNASBT_PAGE_SIZE>(),
                                             sbt::mmap_storage_manager,
                                             sbt::mmap_storage_manager,
                                             sbt::mmap_storage_manager,
                                             sbt::basic_memory_manager,
                                             sbt::basic_memory_manager,
                                             sbt::basic_memory_manager>;
        if (!sbt::csbtree_validate(sbt::csbt_name(in), sbt::info_name(in), DNASBT_PAGE_SIZE, false, log)) {
            log->error("validation failed for csbt file {} with sidecar file {}",
                       sbt::csbt_name(in), sbt::info_name(in));
            return -1;
        }
        return run<mmap_index_type>(in, use_packed, pattern, set_pattern, log);
    }
} // main
