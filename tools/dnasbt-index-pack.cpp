/***
 *  $Id$
 **
 *  File: dnasbt-index-pack.cpp
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *
 *  Copyright (c) 2021 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#include <cxxopts/cxxopts.hpp>

#include <Logger.hpp>

#include <dnasbt/dnasbt_config.hpp>
#include <dnasbt/dnasbt_filenames.hpp>
#include <dnasbt/detail/omp_impl.hpp>
#include <dnasbt/detail/utils.hpp>

#include <filesystem>
#include <fstream>
#include <vector>

#include <lz4.h>


int main(int argc, char* argv[]) {
    Logger::init();
    auto log = spdlog::stdout_color_mt("dnasbt-index-pack");
    spdlog::cfg::load_env_levels();

    log->info("{} ver. {}", argv[0], DNASBT_VERSION);

    std::string out_name;

    try {
        cxxopts::Options options(argv[0]);

        options.add_options()
            ("o,out", "output database name", cxxopts::value<std::string>(out_name))
            ("h,help", "print this help")
            ;

        auto opt_res = options.parse(argc, argv);

        if (!opt_res.count("out") || opt_res.count("help")) {
            std::cout << options.help() << std::endl;
            return 0;
        }

        if (argc != 1) throw cxxopts::OptionException("Unrecognized options");
    } catch (const cxxopts::OptionException& e) {
        log->error("{}", e.what());
        return -1;
    }


    std::ifstream f(sbt::csbt_name(out_name), std::ios::binary);

    if (!f) {
        log->error("could not open index file");
        return -1;
    }

    std::ofstream of(sbt::pcsbt_name(out_name), std::ios::binary);

    if (!of) {
        log->error("unable to create compressed index file");
        return -1;
    }

    log->info("page size used: {}B", DNASBT_PAGE_SIZE);
    log->info("performing scan to identify compressed block size...");

    std::vector<char> buf(DNASBT_PAGE_SIZE);
    std::vector<char> out(2 * LZ4_compressBound(DNASBT_PAGE_SIZE) + sizeof(int)); // we need extra int to store block size

    int max_bs = 0;
    long long int num_blocks = std::filesystem::file_size(sbt::csbt_name(out_name)) / DNASBT_PAGE_SIZE;

    log->info("working with {} blocks...", num_blocks);

    int num_thr = 1;

    #pragma omp parallel shared(num_thr)
    #pragma omp single
    num_thr = omp_get_num_threads();

    if (num_blocks < num_thr) num_thr = num_blocks;
    long long int psize = std::ceil((1.0 * num_blocks) / num_thr);

    log->debug("using {} threads", num_thr);
    log->debug("{} blocks per thread", psize);

    #pragma omp parallel num_threads(num_thr) default(none)             \
        firstprivate(buf, out)                                          \
        shared(log, num_thr, num_blocks, psize, out_name) reduction(max:max_bs)
    {
        int tid = omp_get_thread_num();

        long long int first_block = tid * psize;
        long long int last_block = first_block + psize;

        if (tid == num_thr - 1) last_block = num_blocks;

        log->debug("thread {} working on blocks [{},{})...", tid, first_block, last_block);

        std::ifstream ft(sbt::csbt_name(out_name), std::ios::binary);
        ft.seekg(first_block * DNASBT_PAGE_SIZE, std::ios::beg);

        for (long long int i = first_block; i < last_block; ++i) {
            ft.read(buf.data(), DNASBT_PAGE_SIZE);
            auto res = LZ4_compress_default(buf.data(), out.data(), buf.size(), out.size());
            if (res <= 0) {
                log->error("whooops, compression failed, that is not supposed to happen!");
                throw std::runtime_error(std::string{"compression failed, res="} + std::to_string(res));
            }
            max_bs = std::max(max_bs, res);
        }

        ft.close();
    }

    max_bs += sizeof(int); // this is what we will write per-blocks

    // round up to the nearest multiple of the device block size (for page alignment purposes)
    if ((max_bs % DNASBT_DEV_BLOCK_SIZE) != 0) {
        log->debug("changing block size {} to align with device block size...", max_bs);
        max_bs += (DNASBT_DEV_BLOCK_SIZE - (max_bs % DNASBT_DEV_BLOCK_SIZE));
        log->debug("new block size is {}", max_bs);
    }
    assert(max_bs % DNASBT_DEV_BLOCK_SIZE == 0);

    log->info("compressed block size: {}B", max_bs);
    log->info("compression space saving: {0:.2f}", (1.0 * DNASBT_PAGE_SIZE - max_bs) / DNASBT_PAGE_SIZE);
    log->info("performing compression...");

    f.clear();
    f.seekg(0, std::ios::beg);

    // we use a whole device block for the metadata to satisfy direct-io alignment requirements
    static_assert(DNASBT_DEV_BLOCK_SIZE >= sizeof(max_bs));
    std::vector<char> metadata_block(DNASBT_DEV_BLOCK_SIZE);
    *reinterpret_cast<decltype(max_bs)*>(metadata_block.data()) = max_bs;
    of.write(metadata_block.data(), DNASBT_DEV_BLOCK_SIZE);

    for (long long int i = 0; i < num_blocks; ++i) {
        f.read(buf.data(), DNASBT_PAGE_SIZE);
        auto res = LZ4_compress_default(buf.data(), out.data() + sizeof(int), DNASBT_PAGE_SIZE, out.size() - sizeof(int));
        *reinterpret_cast<int*>(&out[0]) = res;
        of.write(out.data(), max_bs);
    }

    auto sz = std::filesystem::file_size(sbt::pcsbt_name(out_name));
    log->info("{} file size: {}B ({})", sbt::pcsbt_name(out_name), sz, sbt::detail::format_size(sz));

    // we are done!!!
    log->info("done!");

    return 0;
} // main
