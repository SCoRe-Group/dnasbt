/***
 *  $Id$
 **
 *  File: dscount.cpp
 *  Author: Andrew Mikalsen <ajmikals@buffalo.edu>
 *
 *  Copyright (c) 2021 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#include <filesystem>
#include <iostream>

#include <dnasbt/dnasbt_duplicate_suffixes.hpp>

int main(int argc, char* argv[]) {

    if (argc != 2) {
        std::cout << "usage: " << argv[0] << " input" << std::endl;
        return 0;
    }

    auto input = argv[1];

    sbt::dnasbt_duplicate_suffixes ds{};
    auto counts = ds.load_counts(input);

    for (auto& [k, v] : counts) {
        std::cout << k << " " << v << std::endl;
    }

    return 0;
} // main
