#include <iostream>

#include <dnasbt/dnasbt.hpp>
#include <dnasbt/dnasbt_config.hpp>
#include <dnasbt/dnasbt_types.hpp>

using namespace sbt;

template <int b> struct dna_sbt_node_base_1_ {
    offset_type A[b];
    length_type LCP[b];
    sbt_index_type child[dna_sbt_node_child_size<b>()];
    suffix_index_type size;
}; // dna_sbt_node_base_

template <int b> struct dna_sbt_node_base_2_ {
    offset_type A[b];
    sbt_index_type child[dna_sbt_node_child_size<b>()];
    suffix_index_type size;
    length_type LCP[b];
}; // dna_sbt_node_base_

template <int b> struct dna_sbt_node_base_3_ {
    offset_type A[b];
    length_type LCP[b + 1];
    sbt_index_type child[dna_sbt_node_child_size<b>()];
    suffix_index_type size;
}; // dna_sbt_node_base_

template <int b> struct dna_sbt_node_base_4_ {
    offset_type A[b];
    sbt_index_type child[dna_sbt_node_child_size<b>()];
    suffix_index_type size;
    length_type LCP[b + 1];
}; // dna_sbt_node_base_

int main() {
    //std::cout << "page size: " << DNASBT_PAGE_SIZE << std::endl;
    //std::cout << "node size: "
    //          << sizeof(sbt::dna_sbt_node<sbt::sbtree_suffixes_per_node<DNASBT_PAGE_SIZE>()>) << std::endl;
    //std::cout << "suffixes per node: " << sbt::sbtree_suffixes_per_node<DNASBT_PAGE_SIZE>() << std::endl;

    //std::cout << "size for 37448 suffixes: " << sizeof(sbt::dna_sbt_node<37448>) << std::endl;

    auto constexpr b = 37448;
    std::cout << sizeof(dna_sbt_node_base_<b>) << std::endl;
    std::cout << sizeof(dna_sbt_node_base_1_<b>) << std::endl;
    std::cout << sizeof(dna_sbt_node_base_2_<b>) << std::endl;
    std::cout << sizeof(dna_sbt_node_base_3_<b>) << std::endl;
    std::cout << sizeof(dna_sbt_node_base_4_<b>) << std::endl;

    return 0;
} // main
