/***
 *  $Id$
 **
 *  File: Logger.hpp
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *
 *  Copyright (c) 2021 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#ifndef LOGGER_HPP
#define LOGGER_HPP

#include <spdlog/spdlog.h>
#include <spdlog/cfg/env.h>
#include <spdlog/sinks/stdout_color_sinks.h>

#include <dnasbt/dnasbt_config.hpp>


struct Logger {
#ifdef DNASBT_BUILD_TESTING
    static void init() { spdlog::set_pattern("[%^%l%$] %n: %v"); }
#else
    static void init() { spdlog::set_pattern("%Y-%m-%d %H:%M:%S [%^%l%$] %n: %v"); }
#endif
}; // struct Logger

#endif // LOGGER_HPP
