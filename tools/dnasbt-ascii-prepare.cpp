/***
 *  $Id$
 **
 *  File: dnasbt-text-prepare.cpp
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *
 *  Copyright (c) 2021 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#include <bio/fastx_iterator.hpp>
#include <cxxopts/cxxopts.hpp>
#include <jaz/algorithm.hpp>

#include <Logger.hpp>

#include <dnasbt/dnasbt.hpp>
#include <dnasbt/dnasbt_filenames.hpp>
#include <dnasbt/detail/utils.hpp>

#include <string>
#include <vector>


template <typename FastaIter>
bool read_sequences(const std::string& in_name,
                    int size,
                    bool get_names,
                    std::string& T,
                    std::vector<sbt::offset_type>& Tidx,
                    std::vector<std::pair<sbt::offset_type, std::string>>& Tnames) {
    namespace fs = std::filesystem;
    if (!fs::exists(in_name)) return false;

    bio::fastx_files_iterator<FastaIter> fiter(in_name), end;

    T.clear();
    Tidx.clear();
    Tnames.clear();

    Tidx.push_back(0);

    std::string name;
    std::string s;

    for (; fiter != end; ++fiter) {
        auto& id = std::get<0>(*fiter);

        // extract short name
        name = id.substr(0, id.find(' '));
        auto rp = name.rfind('|');

        if (rp == std::string::npos) rp = 0; else ++rp;
        name = name.substr(rp);

        s = std::get<1>(*fiter);

        // do segmentation into valid substrings
        auto first = s.begin();
        auto last = s.end();

        auto end = last;
        if ((end - first) >= size) {
            T.append(first, end);
            T += "$";
            Tidx.push_back(T.size());
            if (get_names) Tnames.push_back({T.size(), name});
        }
    } // for fiter

    return true;
} // read_sequences


int main(int argc, char* argv[]) {
    Logger::init();
    auto log = spdlog::stdout_color_st("dnasbt-text-prepare");
    spdlog::cfg::load_env_levels();

    log->info("{} ver. {}", argv[0], DNASBT_VERSION);

    std::string in_name;
    std::string out_name;
    bool is_fastq = false;
    int size = 1;
    bool get_names = false;

    try {
        cxxopts::Options options(argv[0]);

        options.add_options()
            ("i,in", "directory with input sequences", cxxopts::value<std::string>(in_name))
            ("o,out", "output database name", cxxopts::value<std::string>(out_name))
            ("q,fastq", "input sequences in FASTQ format", cxxopts::value<bool>(is_fastq)->default_value("false"))
            ("s,size", "consider only sequences at least this size", cxxopts::value<int>(size)->default_value("1"))
            ("n,names", "create mapping between names and ids of sequences", cxxopts::value<bool>(get_names)->default_value("false"))
            ("h,help", "print this help")
            ;

        auto opt_res = options.parse(argc, argv);

        if (!(opt_res.count("in") && opt_res.count("out")) || opt_res.count("help")) {
            std::cout << options.help() << std::endl;
            return 0;
        }

        if (argc != 1) throw cxxopts::OptionException("Unrecognized options");
    } catch (const cxxopts::OptionException& e) {
        log->error("{}", e.what());
        return -1;
    }

    // some basic sanity check
    if (size < 1) {
        log->error("size parameter too small, must be >= 1");
        return -1;
    }

    // 1. extract text
    log->info("extracting ASCII sequences from {}, size parameter {}...", in_name, size);

    std::string T;
    std::vector<sbt::offset_type> Tidx;
    std::vector<std::pair<sbt::offset_type, std::string>> Tnames;

    bool res;

    if (is_fastq) res = read_sequences<bio::fastq_input_iterator>(in_name, size, get_names, T, Tidx, Tnames);
    else res = read_sequences<bio::fasta_input_iterator>(in_name, size, get_names, T, Tidx, Tnames);

    if (res < 1) {
        log->error("no input sequences found!");
        return -1;
    }

    uint64_t l = T.size();
    uint64_t n = Tidx.size() - 1;

    log->info("extracted {} sequence fragments, total text length: {}", n, l);


    // 2. write text index
    log->info("storing text and text index...");

    if (!sbt::detail::write_file(log, sbt::text_name(out_name), T.c_str(), T.size())) return -1;

    if (!sbt::detail::write_file(log, sbt::tidx_name(out_name),
                    reinterpret_cast<const char*>(Tidx.data()),
                    Tidx.size() * sizeof(sbt::offset_type))) return -1;

    if (get_names) {
        if (!sbt::detail::write_map(log, sbt::tnames_name(out_name), Tnames)) return -1;
    }

    std::vector<sbt::offset_type>().swap(Tidx);
    std::vector<std::pair<sbt::offset_type, std::string>>().swap(Tnames);

    // 3. get length of suffixes
    log->info("extracting length of suffixes...");

    std::vector<sbt::length_type> L(l);
    sbt::length_type len = 0;

    // TODO: add simple parallelism
    for (uint64_t i = 0; i < l; ++i) {
        if (T[l - i - 1] == '$') len = 0; else len++;
        L[l - i - 1] = len;
    }

    log->info("storing lengths of suffixes...");

    if (!sbt::detail::write_file(log, sbt::len_name(out_name),
                    reinterpret_cast<const char*>(L.data()),
                    L.size() * sizeof(sbt::length_type))) return -1;

    log->info("done!");

    return 0;
} // main
