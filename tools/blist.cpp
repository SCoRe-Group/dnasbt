/***
 *  $Id$
 **
 *  File: blist.cpp
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *
 *  Copyright (c) 2021 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#include <dnasbt/dnasbt.hpp>


int suffix_per_block(int PageSize) {
    int b = ((PageSize - sizeof(sbt::suffix_index_type) - sizeof(sbt::offset_type) - sizeof(char))
             / (sizeof(sbt::offset_type) + sizeof(sbt::length_type) + sizeof(sbt::sbt_index_type) + (2 * sizeof(char)))) & ~1;
    return b;
} // suffix_per_block

double mem_use(long long int n, int PageSize) {
    int b = suffix_per_block(PageSize);
    long long int nodes = (n - 1) / (b - 1);
    return static_cast<double>((nodes - (n / b)) * PageSize) / (1024 * 1024 * 1024);
} // mem_use


int main(int argc, char* argv[]) {
    if (argc != 2) {
        std::cout << "usage: " << argv[0] << " text_size" << std::endl;
        return -1;
    }

    long long int n = std::stoll(argv[1]);

    std::cout << "Block Size (KB)  Cache Size (GB)        b  Tree Depth" << std::endl;

    for (int PageSize = 4; PageSize <= 1024; PageSize += 4) {
        int b = suffix_per_block(PageSize * 1024);

        std::cout << std::setw(15) << PageSize << " "
                  << std::setw(16) << mem_use(n, PageSize * 1024) << " "
                  << std::setw(8) << b << " "
                  << std::setw(11) << static_cast<int>(std::ceil(std::log(n) / std::log(b)))
                  << std::endl;
    }

    return 0;
} // main
