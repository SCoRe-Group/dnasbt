# DNAsbt

A header-only C++20 library for the Compacted String B-Tree (CSBT) data structure, equipped with tools for indexing DNA data. The DNAsbt library is designed with mobile devices in mind, supporting both Intel and ARM processors.

This library builds on the ideas described in the following ISMB 2023 paper:
> A.J. Mikalsen and J. Zola. "[Coriolis: Enabling metagenomic classification on lightweight mobile devices](https://academic.oup.com/bioinformatics/article/39/Supplement_1/i66/7210430)", In: _Intelligent Systems for Molecular Biology_ (ISMB), 2023.

This project has been supported by [NSF](https://www.nsf.gov/) under the award [CNS-1910193](https://www.nsf.gov/awardsearch/showAward?AWD_ID=1910193).

## Table of Contents
[[_TOC_]]

## Requirements

 * A C++20-compliant compiler with OpenMP support. We tested with `g++` and `clang++`.
 * CMake version 3.12 or higher.
 * Boost version 1.71 or higher (for the Boost.Iostreams module).
 * The LZ4 library version 1.9.3 or higher.
 * The [spdlog](https://github.com/gabime/spdlog) library version 1.6 or higher when compiling with tools (see the `-I` option).
 * We currently support only Linux, however support for macOS should be feasible if certain features (e.g., direct I/O) are not used.

## Installation

First clone the desired release of DNAsbt from the [Releases](https://gitlab.com/SCoRe-Group/dnasbt/-/releases) page and `cd` into the root of the repository. For example:

```
git clone https://gitlab.com/SCoRe-Group/dnasbt.git DNAsbt
cd DNAsbt/
```

Then, call the build script to install the library:

```
./build.sh
```

By default DNAsbt will install in the `release/` directory of the current directory. After running:

```
ls release/
```
you should see:

```
bin include lib
```

You can customize the install path, by using the `-i <DIR>` option. For example, run:

```
./build.sh -i /usr/local/
```

to install DNAsbt system-wide (may require root privileges).

The `build.sh` script can be used to customize a number of other settings. Run `./build.sh -h` to get the full list.

Specifically, you may consider tuning the following three options:

* Use the `-s <SIZE>` flag to set the CSBT page size (desired number of bytes per CSBT node). The default is 49152B, which we found to be optimal in the majority of deployments. Note that this default may change in the future.
* Use the `-B <SIZE>` flag to set the device block size. Note that you should use this option only if you expect that your operating system uses different block size than the common 4096B.

Finally, if you want to install only the library headers (e.g., when deploying in a mobile environment as a dependency) use the `-I` flag.


## Getting Started

### Building the index

After installing the library, a DNAsbt CSBT index can be constructed by using the `dnasbt-*` tools as follows:

1. Prepare FASTA/FASTQ files for indexing by running `dnasbt-text-prepare`. For example:
   ```
   dnasbt-text-prepare -i in/ -o out
   ```
   will extract all reads in FASTA files found in the `in/` directory, and will store the prepared text in `out.*` files.

2. Precompute the suffix and LCP arrays by running `dnasbt-suff-extract`. The `-o` parameter passed must be exactly the same as the one used during text prepare stage. For example:
   ```
   dnasbt-suff-extract -o out
   ```
   This stage is quite computationally intensive, especially given that it is only partially parallelized and also involves memory mapping of the suffix array. If fast construction of the suffix array is needed you can use [parallel suffix array construction](https://doi.org/10.1145/2807591.2807609) tools instead (e.g., [psac](https://github.com/patflick/psac)).

3. Create the final CSBT index by running `dnasbt-index-create`. Again, the `-o` parameter passed must be exactly the same as in the previous steps:
   ```
   dnasbt-index-create -o out -m -c 32
   ```
   You most likely want to also pass the `-m` option to force memory mapping of the suffix array and LCP array (unless you have TBs of RAM at your disposal, or your input text is very small). By default, a 4GB cache is used during the index construction (note that this has no impact on the produced index itself). This cache size can be changed with the `-c` option.

4. When working with block sizes above 8K, we recommend packing the CSBT index for compatibility with the `lz4_*_storage_manager`s by running `dnasbt-index-pack`. The `-o` parameter passed must be exactly the same as in the previous steps:
   ```
   dnasbt-index-pack -o out
   ```
   The index packing usually provides ~20% space reduction. When the index is used, packing will incur only minimal CPU overheads, and packing the CSBT nodes improves I/O-efficiency.

The final index consists of the following files:

* `.dnasbt_text` and `.dnasbt_tidx` storing the actual extracted text and offsets of individual reads.
* `.dnasbt_csbt` storing the unpacked CSBT index.
* `.dnasbt_pcsbt` storing the packed CSBT index.
* `.dnasbt_dsuf64` storing a binary representation of the positions of all equivalent suffixes in the original reference sequences.
* `.dnasbt_info` storing the sidecar information used to validate the index.
* `.dnasbt_tnames` storing the mapping between the sequence ID of each reference string and its end position in `.dnasbt_text`.
* The remaining files, specifically `.len16` storing the length of individual suffixes, along with `.lcp64` and `.sa64` storing the LCP and suffix array respectively, are temporary and can be safely removed once the CSBT index is ready.

### Library usage

The main functionality of the DNAsbt library is provided by the `CSBTree` class in `dnasbt/dnasbt.hpp`. We give a comprehensive example of using the class in the code snippet below. `CSBTree` takes an `int` template parameter `b` to specify the number of suffixes in a CSBT node, which also corresponds to the branching factor. If using a CSBT built with `dnasbt-index-create`, the easiest thing to do is to set `b` as `sbt::csbtree_suffixes_per_node<DNASBT_PAGE_SIZE>()`, using the `DNASBT_PAGE_SIZE` macro (e.g., `sbt::CSBTree<sbt::csbtree_suffixes_per_node<DNASBT_PAGE_SIZE>()>`). The file `dnasbt/dnasbt_config.hpp` that contains `DNASBT_PAGE_SIZE` is generated at build time, and sets `DNASBT_PAGE_SIZE` to the CSBT page size chosen for the build (i.e., by the `-s` option). `CSBTree` also takes six parameters for the storage and memory managers of the text, nodes, and duplicate suffixes, which can be found in the `dnasbt/dnasbt_storage_manager.hpp` and `dnasbt/dnasbt_storage_manager.hpp` files respectively. In most cases, these template parameters should be set as follows:

* `mmap_storage_manager` for `TextStorageManager`
* `lz4_direct_io_storage_manager` for `SbtStorageManager` (if the packed CSBT is used, otherwise use `direct_io_storage_manager`)
* `mmap_storage_manager` for `DupStorageManager`
* `basic_memory_manager` for `TextMemoryManager`
* `tree_memory_manager` for `SbtMemoryManager` (if locality in accessing CSBT leaves is expected, try `cache_memory_manager`)
* `basic_memory_manager` for `DupMemoryManager`

To use the CSBT, it must first be loaded via the `CSBTree::load` member function. The input to this function depends on the storage and memory managers used. However, the `CSBTree::load` function assumes that all input data is valid. To verify that this is the case, you should first use the `sbt::csbtree_validate` function.

Once loaded, the CSBT can be queried with the `CSBTree::find` member function. The `find` member function takes a pattern `P` and an output iterator `occ`, and returns an integer `lcp`. The value of `lcp` is the length of the longest prefix of `P` that matches some region of the text indexed by the CSBT, and the function will insert the positions of all such matching regions into `occ`.

Below we provide a fully functional example.

```c++
#include <cstddef>
#include <iterator>
#include <vector>

#include <dnasbt/dnasbt.hpp>

int main() {
    constexpr auto b = sbt::csbtree_suffixes_per_node<DNASBT_PAGE_SIZE>();
    using index_type = sbt::CSBTree<b, sbt::mmap_storage_manager,
                                       sbt::lz4_direct_io_storage_manager,
                                       sbt::mmap_storage_manager,
                                       sbt::basic_memory_manager,
                                       sbt::tree_memory_manager,
                                       sbt::basic_memory_manager>;

    if (!sbt::csbtree_validate("index.dnasbt_pcsbt", "index.dnasbt_info", DNASBT_PAGE_SIZE, true)) {
        return 0;
    }

    index_type T{};

    constexpr auto cache_size = 1.25;
    T.load({"index.dnasbt_text"}, {cache_size, "index.dnasbt_pcsbt"}, {"index.dnasbt_dsuf64"});

    std::vector<std::size_t> output{};
    auto lcp = T.find("AACCGGTT", std::back_inserter(output));

    return 0;
} // main
```

In the above snippet, we use the packed CSBT (recommended). To use the unpacked CSBT, you would instead define `index_type` as
```c++
using index_type = sbt::CSBTree<b, sbt::mmap_storage_manager,
                                   sbt::direct_io_storage_manager,
                                   sbt::mmap_storage_manager,
                                   sbt::basic_memory_manager,
                                   sbt::tree_memory_manager,
                                   sbt::basic_memory_manager>;

```
and pass in the corresponding filename for the `SbtStorageManager` in the call to `load`. The call to `sbt::csbtree_validate` would instead be
```c++
sbt::csbtree_validate("index.dnasbt_csbt", "index.dnasbt_info", DNASBT_PAGE_SIZE, false)
```
to indicate that the unpacked CSBT should be validated.

For convenience, `dnasbt/dnasbt_names.hpp` contains functions to obtain the names of files produced by the index creation tools, e.g., `text_name` for text, `csbt_name` for unpacked CSBT nodes, `pcsbt_name` for packed CSBT nodes, `dsuf_name` for duplicate suffixes, etc. Using these functions, the original snippet can be re-written as shown below.
```c++
#include <cstddef>
#include <iterator>
#include <vector>

#include <dnasbt/dnasbt.hpp>
#include <dnasbt/dnasbt_names.hpp>

int main() {
    constexpr auto b = sbt::csbtree_suffixes_per_node<DNASBT_PAGE_SIZE>();
    using index_type = sbt::CSBTree<b, sbt::mmap_storage_manager,
                                       sbt::lz4_direct_io_storage_manager,
                                       sbt::mmap_storage_manager,
                                       sbt::basic_memory_manager,
                                       sbt::cache_memory_manager,
                                       sbt::basic_memory_manager>;

    if (!sbt::csbtree_validate(sbt::pcsbt_name("index"), sbt::info_name("index"), DNASBT_PAGE_SIZE, true)) {
        return 0;
    }

    index_type T{};

    constexpr auto cache_size = 1.25;
    T.load({sbt::text_name("index")}, {cache_size, sbt::pcsbt_name("index")}, {sbt::dsuf_name("index")});

    std::vector<std::size_t> output{};
    auto lcp = T.find("AACCGGTT", std::back_inserter(output));

    return 0;
} // main
```


### CMake integration

The library supports CMake's `find_package`. A minimal example of a `CMakeLists.txt` that uses DNAsbt with a project is given below.

```cmake
cmake_minimum_required(VERSION 3.12)
project(example
    VERSION 0.0.1
    LANGUAGES CXX
    DESCRIPTION "Example of a project that uses the DnaSbt library."
)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

find_package(Boost 1.74 REQUIRED COMPONENTS iostreams)
find_package(DnaSbt 0.1.0 REQUIRED)
if(${DnaSbt_FOUND})
    message(STATUS "Found DnaSbt ${DnaSbt_VERSION} at ${DnaSbt_DIR}")
endif()

add_executable(main main.cpp)
target_link_libraries(main DnaSbt::DnaSbt)

install(TARGETS main)
```


## For Developers

### Configuring Tests

Some tests depend on a few environment variables being set. With respect to the project root, they should be defined as follows.

 * `export TOOLS_DIR=$(realpath ./build/tools)`
 * `export TEST_TMP_DIR=$(realpath ./tmp)`
 * `export TEST_DATA_DIR=$(realpath ./test/data)`

Additionally, system tests require the following environment variables.
 * `SYS_TEST_DB_LOC`
 * `SYS_TEST_DB_LIM`

### Containers

Note that the containers in `container/` are for developers only. We may provide out-of-the-box containers in the future, but for now the only supported distribution method is building from source.


## License

This code is distributed under the [Boost Software License - Version 1.0](LICENSE).

## How to Cite

If you use DNAsbt, please cite our paper:

> A.J. Mikalsen and J. Zola. "[Coriolis: Enabling metagenomic classification on lightweight mobile devices](https://academic.oup.com/bioinformatics/article/39/Supplement_1/i66/7210430)", In: _Intelligent Systems for Molecular Biology_ (ISMB), 2023.
