#include <gtest/gtest.h>

#include <dnasbt/dnasbt_memory_manager.hpp>
/* #include <dnasbt/experimental/dnasbt_experimental_memory_manager.hpp> */


namespace sbt::test {

  /// Cache size to use for memory managers that depend on caching.
  constexpr auto cache_size = 8ull;

  /// Used to construct an arbitrary memory_manager from just a storage_manager. In other words, for memory_managers
  /// whose constructor takes more than just the storage_manager as its arguments, this will provide some "default"
  /// values to that constructor for the purpose of the test.
  template <typename MemoryManager>
  MemoryManager make_memory_manager(typename MemoryManager::storage_manager_type&& storage) {
      using storage_type = typename MemoryManager::storage_manager_type;
      if constexpr (std::is_same<MemoryManager, sbt::basic_memory_manager<storage_type>>()) {
          return MemoryManager{std::move(storage)};
      } else if constexpr (std::is_same<MemoryManager, sbt::alloc_memory_manager<storage_type>>()) {
          return MemoryManager{std::move(storage)};
      }
      /* else if constexpr (std::is_same<MemoryManager, sbt::experimental::experimental_memory_manager<storage_type>>()) { */
      /*     return MemoryManager{cache_size, std::move(storage)}; */
      /* } */
      else if constexpr (std::is_same<MemoryManager, sbt::cache_memory_manager<storage_type>>()) {
          return MemoryManager{cache_size, std::move(storage)};
      }
      else {
          FAIL() << "The provided memory_manager does not have a case defined in make_memory_manager";
          return MemoryManager{};
      }
  } // make_memory_manager

} // sbt::test
