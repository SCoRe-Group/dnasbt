#include <cstddef>
#include <cstdint>

#include <dnasbt/dnasbt_config.hpp>

namespace sbt::test {

  /// Number of integers in an aligned_block.
  constexpr std::size_t aligned_block_size = DNASBT_DEV_BLOCK_SIZE / sizeof(std::uint64_t);

  /// A repeating array of integers.
  struct aligned_block {
      std::uint64_t values[aligned_block_size];
  }; // struct aligned_block

  inline aligned_block make_aligned_block(std::uint64_t value) {
      aligned_block block{};
      for (auto i = 0ull; i < aligned_block_size; ++i) {
          block.values[i] = value;
      } // for i
      return block;
  } // make_aligned_block

  inline bool operator==(const aligned_block& lhs, const aligned_block& rhs) {
       for (auto i = 0ull; i < aligned_block_size; ++i) {
          if (lhs.values[i] != rhs.values[i]) {
              return false;
          }
      } // for i
      return true;
  } // operator==

  inline bool operator==(const aligned_block& lhs, const std::uint64_t& rhs) {
      for (auto i = 0ull; i < aligned_block_size; ++i) {
          if (lhs.values[i] != rhs) {
              return false;
          }
      } // for i
      return true;
  } // operator==

} // namespace sbt::test
