#ifndef DNASBT_TEST_HPP
#define DNASBT_TEST_HPP

#include <cstdlib>
#include <filesystem>
#include <spdlog/common.h>
#include <string>

#include <gtest/gtest.h>
#include <spdlog/spdlog.h>
#include <spdlog/cfg/env.h>
#include <spdlog/sinks/stdout_color_sinks.h>

#include <dnasbt/dnasbt.hpp>
#include <dnasbt/dnasbt_filenames.hpp>

#include "environments.hpp"


namespace sbt::test {

  inline std::vector<std::string::size_type> find_all_occ(const std::string& pattern, const std::string& text) {
      std::vector<std::string::size_type> occ;

      auto searcher = std::boyer_moore_horspool_searcher(pattern.begin(), pattern.end());

      auto it = text.begin();
      while (it != text.end()) {
          it = std::search(it, text.end(), searcher);
          if (it != text.end()) {
              occ.push_back(it - text.begin());
              it += 1;
          }
      } // while it

      return occ;
  } // find_all_occ


  class null_logger {
  public:
      template <typename FormatString, typename... Args>
      void trace(const FormatString&, Args&&...) {}

      template <typename FormatString, typename... Args>
      void debug(const FormatString&, Args&&...) {}

      template <typename FormatString, typename... Args>
      void info(const FormatString&, Args&&...) {}

      template <typename FormatString, typename... Args>
      void warn(const FormatString&, Args&&...) {}

      template <typename FormatString, typename... Args>
      void error(const FormatString&, Args&&...) {}

      template <typename FormatString, typename... Args>
      void critical(const FormatString&, Args&&...) {}
  }; // class null_logger


  template <typename T>
  inline bool read_file(const std::string& name, std::vector<T>& data) {
      std::error_code ec;
      auto sz = std::filesystem::file_size(name, ec);

      if (ec || (sz < 2)) {
          return false;
      }

      data.resize(sz / sizeof(T));

      std::ifstream f(name);

      if (!f) {
          return false;
      }

      f.read(reinterpret_cast<char*>(data.data()), sz);

      if (!f) {
          return -1;
      }

      f.close();

      return true;
  } // read_file


  /// Preprocesses data from ref_name (in ref_loc) and stores it in db_loc.
  inline void sbt_preprocess(const std::string& db_loc, const std::string& ref_name,
                             const std::string& ref_loc = TestEnv::test_data_dir) {

      std::string text_cmd = TestEnv::tools_dir + "/dnasbt-text-prepare -i "+ ref_loc + "/" + ref_name
                                                + " -o " + db_loc
                                                + " -s 4 -n > /dev/null";

      std::string suff_cmd = TestEnv::tools_dir + "/dnasbt-suff-extract -o " + db_loc + " > /dev/null";

      // system is expected to return the exit code of the command (i.e., 0 iff success)
      if (std::system((std::string{"mkdir -p "} + TestEnv::test_tmp_dir).c_str()) != 0) {
          constexpr auto setup_error = "Failed to create temporary directory";
          FAIL() << setup_error;
          return;
      }

      if (std::system(text_cmd.c_str()) != 0) {
          constexpr auto setup_error = "Failed to prepare text";
          FAIL() << setup_error;
          return;
      }

      if (std::system(suff_cmd.c_str()) != 0) {
          constexpr auto setup_error = "Failed to extract suffixes";
          FAIL() << setup_error;
          return;
      }
  } // sbt_preprocess


  /// Preprocesses ASCII data from ref_name (in ref_loc) and stores it in db_loc.
  inline void sbt_ascii_preprocess(const std::string& db_loc, const std::string& ref_name,
                                   const std::string& ref_loc = TestEnv::test_data_dir) {

      std::string text_cmd = TestEnv::tools_dir + "/dnasbt-ascii-prepare -i "+ ref_loc + "/" + ref_name
                                                + " -o " + db_loc
                                                + " -s 1 -n > /dev/null";

      std::string suff_cmd = TestEnv::tools_dir + "/dnasbt-suff-extract -o " + db_loc + " > /dev/null";

      // system is expected to return the exit code of the command (i.e., 0 iff success)
      if (std::system((std::string{"mkdir -p "} + TestEnv::test_tmp_dir).c_str()) != 0) {
          constexpr auto setup_error = "Failed to create temporary directory";
          FAIL() << setup_error;
          return;
      }

      if (std::system(text_cmd.c_str()) != 0) {
          constexpr auto setup_error = "Failed to prepare text";
          FAIL() << setup_error;
          return;
      }

      if (std::system(suff_cmd.c_str()) != 0) {
          constexpr auto setup_error = "Failed to extract suffixes";
          FAIL() << setup_error;
          return;
      }
  } // sbt_ascii_preprocess

  /// Constructs uncompressed csbt from db_loc with limit 1 and outputs a csbt with the same name plus an optional suffix.
  template <int b>
  inline void sbt_construct(const std::string& db_loc, long long int cache, const std::string& sbt_name_suffix = "",
                            int limit = 1, bool logging = true) {
      std::vector<char> T;
      if (!read_file(sbt::text_name(db_loc), T)) {
          constexpr auto setup_error = "Failed to read text";
          FAIL() << setup_error;
          return;
      }

      std::vector<sbt::length_type> L;
      if (!read_file(sbt::len_name(db_loc), L)) {
          constexpr auto setup_error = "Failed to read suffix lengths";
          FAIL() << setup_error;
          return;
      }

      auto log = spdlog::get("dnasbt_test");
      if (log == nullptr) {
          log = spdlog::stdout_color_mt("dnasbt_test");
      }

      if (logging) {
          log->set_level(spdlog::level::debug);
      } else {
          log->set_level(spdlog::level::warn);
      }

      if (sbt::create_csbtree<b>(sbt::csbt_name(db_loc + sbt_name_suffix), sbt::dsuf_name(db_loc), sbt::info_name(db_loc),
                                T.data(), L.data(), sbt::sa_name(db_loc), sbt::lcp_name(db_loc), limit, cache, log) < 0) {
          constexpr auto setup_error = "Failed to construct CSBT";
          FAIL() << setup_error;
          return;
      }

      auto node_size = sbt::CSBTree<b>::node_size();
      if (!sbt::csbtree_validate(sbt::csbt_name(db_loc + sbt_name_suffix), sbt::info_name(db_loc), node_size, false, log)) {
          FAIL() << "CSBT validation failed";
          return;
      }
  } // sbt_construct


  /// Constructs compressed csbt from csbt named db_loc with an optional suffix.
  inline void sbt_compress(const std::string& db_loc, const std::string& sbt_name_suffix = "") {
      std::string comp_cmd = TestEnv::tools_dir + "/dnasbt-index-pack -o " + db_loc + sbt_name_suffix; // + " > /dev/null";

      // try to make temporary directory if it doesn't exist
      if (std::system((std::string{"mkdir -p "} + TestEnv::test_tmp_dir).c_str()) != 0) {
          constexpr auto setup_error = "Failed to create temporary directory";
          FAIL() << setup_error;
          return;
      }

      // try to compress the csbt
      if (std::system(comp_cmd.c_str()) != 0) {
          FAIL() << "Failed to pack CSBT";
          return;
      }

      if (!sbt::csbtree_validate(sbt::pcsbt_name(db_loc + sbt_name_suffix), sbt::info_name(db_loc), DNASBT_PAGE_SIZE, true)) {
          FAIL() << "Packed CSBT validation failed";
          return;
      }
  } // sbt_compress


  /// Returns the text of the dna text file if it was read successfully, and nothing otherwise.
  inline std::optional<std::string> load_text(const std::string& text_file_name) {
      std::vector<char> data;
      if (!read_file(text_file_name, data)) {
          return {};
      }
      return std::string{data.data(), data.size()};
  } // load_text

} // namespace sbt::test

#endif // DNASBT_TEST_HPP
