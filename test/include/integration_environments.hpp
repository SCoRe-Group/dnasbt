#ifndef DNASBT_TEST_INTEGRATION_ENVIRONMENTS_HPP
#define DNASBT_TEST_INTEGRATION_ENVIRONMENTS_HPP

#include <dnasbt/dnasbt.hpp>
#include <dnasbt/dnasbt_config.hpp>
#include <gtest/gtest.h>

#include "test.hpp"


namespace sbt::test {

  class IntegrationTestEnv : public ::testing::Environment {
  public:

      static constexpr auto b = sbt::csbtree_suffixes_per_node<DNASBT_PAGE_SIZE>();
      static constexpr auto db_name = "integration-test";

      static inline std::string db_loc = "";

      void SetUp() override {
          db_loc = sbt::test::TestEnv::test_tmp_dir + "/" + db_name;

          ASSERT_NO_FATAL_FAILURE(sbt_preprocess(db_loc, "algaereduced"));
          ASSERT_NO_FATAL_FAILURE(sbt_construct<b>(db_loc, 1024 * 1024 * 1024, "", 1, false));
          ASSERT_NO_FATAL_FAILURE(sbt_compress(db_loc));
      } // SetUpTestSuite

  }; // class IntegrationTestEnv

} // namespace sbt::test

#endif //DNASBT_TEST_INTEGRATION_ENVIRONMENTS_HPP
