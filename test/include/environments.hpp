#ifndef DNASBT_TEST_ENVIRONMENTS_HPP
#define DNASBT_TEST_ENVIRONMENTS_HPP

#include <cstdlib>
#include <string>

#include <gtest/gtest.h>


namespace sbt::test {

  // Testing environment that loads all general environment variables.
  class TestEnv : public ::testing::Environment {
  public:

      static inline std::string tools_dir = "";     // location of dnasbt tools executables
      static inline std::string test_tmp_dir = "";  // location for storing temporary data
      static inline std::string test_data_dir = ""; // location of test data to operate on

      void SetUp() override {
          if (!std::getenv("TOOLS_DIR")) {
              constexpr auto setup_error = "Environment variable TOOLS_DIR was undefined";
              FAIL() << setup_error;
              return;
          }
          tools_dir = std::getenv("TOOLS_DIR");
          SUCCEED() << "TOOLS_DIR: " << tools_dir;

          if (!std::getenv("TEST_TMP_DIR")) {
              constexpr auto setup_error = "Environment variable TEST_TMP_DIR was undefined";
              FAIL() << setup_error;
              return;
          }
          test_tmp_dir = std::getenv("TEST_TMP_DIR");
          SUCCEED() << "TEST_TMP_DIR: " << test_tmp_dir;

          if (!std::getenv("TEST_DATA_DIR")) {
              constexpr auto setup_error = "Environment variable TEST_DATA_DIR was undefined";
              FAIL() << setup_error;
              return;
          }
          test_data_dir = std::getenv("TEST_DATA_DIR");
          SUCCEED() << "TEST_DATA_DIR: " << test_data_dir;
      } // SetUp

      void TearDown() override {
          // nop
      } // TearDown
  }; // class TestEnv

} // namespace dnasbt::test


#endif // DNASBT_TEST_ENVIRONMENTS_HPP
