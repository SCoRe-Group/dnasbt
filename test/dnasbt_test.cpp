#include <cstdlib>
#include <iterator>
#include <string>

#include <gtest/gtest.h>

#include <dnasbt/dnasbt.hpp>
#include <dnasbt/dnasbt_filenames.hpp>
#include <dnasbt/dnasbt_memory_manager.hpp>
#include <dnasbt/dnasbt_storage_manager.hpp>

#include <environments.hpp>
#include <test.hpp>


namespace {

  using namespace sbt::test;

  class FindTest : public ::testing::Test {
  protected:
      static constexpr auto ref0_db_name = "test-db";
      static constexpr auto b2_sbt_suff = ".b2";
      static constexpr auto b4_sbt_suff = ".b4";

      static inline std::string ref0_db_loc = "";
      static inline std::string b2_ref0_db_loc= "";
      static inline std::string b4_ref0_db_loc = "";

      using sbt_b2_type = sbt::CSBTree<2, sbt::mmap_storage_manager,
                                          sbt::mmap_storage_manager,
                                          sbt::mmap_storage_manager,
                                          sbt::basic_memory_manager,
                                          sbt::alloc_memory_manager,
                                          sbt::basic_memory_manager>;

      using sbt_b4_type = sbt::CSBTree<4, sbt::mmap_storage_manager,
                                          sbt::mmap_storage_manager,
                                          sbt::mmap_storage_manager,
                                          sbt::basic_memory_manager,
                                          sbt::alloc_memory_manager,
                                          sbt::basic_memory_manager>;

      static inline int b2_cache = 2 * sbt_b2_type::node_size();
      static inline int b4_cache = 2 * sbt_b4_type::node_size();

      static void SetUpTestSuite() {
          ref0_db_loc = TestEnv::test_tmp_dir + "/" + ref0_db_name;
          b2_ref0_db_loc = ref0_db_loc + b2_sbt_suff;
          b4_ref0_db_loc = ref0_db_loc + b4_sbt_suff;

          int num_thr = 1;

          #pragma omp parallel shared(num_thr)
          #pragma omp single
          num_thr = omp_get_num_threads();

          ASSERT_NO_FATAL_FAILURE(sbt_preprocess(ref0_db_loc, "ref0"));
          ASSERT_NO_FATAL_FAILURE(sbt_construct<2>(ref0_db_loc, b2_cache * num_thr, b2_sbt_suff));
          ASSERT_NO_FATAL_FAILURE(sbt_construct<4>(ref0_db_loc, b4_cache * num_thr, b4_sbt_suff));
     } // SetUpTestSuite

      static void TearDownTestSuite() {
          // clean up temp files?
      } // TearDownTestSuite


      sbt_b2_type T_b2{};
      sbt_b4_type T_b4{};

      std::string ref0_text{};

      std::vector<std::string::size_type> occ;
      std::vector<std::string::size_type> exp;

      void SetUp() override {
          ASSERT_TRUE(T_b2.load({sbt::text_name(ref0_db_loc)}, {sbt::csbt_name(b2_ref0_db_loc)}, {sbt::dsuf_name(ref0_db_loc)}))
              << "Could not load sbt from " << ref0_db_loc;
          ASSERT_TRUE(T_b4.load({sbt::text_name(ref0_db_loc)}, {sbt::csbt_name(b4_ref0_db_loc)}, {sbt::dsuf_name(ref0_db_loc)}))
              << "Could not load sbt from " << ref0_db_loc;

          auto text_opt = load_text(sbt::text_name(ref0_db_loc));

          ASSERT_TRUE(text_opt.has_value()) << "Failed to load text from " << ref0_db_loc;
          ref0_text = text_opt.value();
      } // SetUp

      void TearDown() override { }
  }; // FindTest


  TEST(FindAllOccTest, Empty) {
      auto occ = find_all_occ("", "ABCDEFGH");

      std::sort(occ.begin(), occ.end());
      ASSERT_EQ(occ.size(), 8);
      for (auto i = 0u; i < occ.size(); ++i) {
          EXPECT_EQ(occ[i], i);
      } // for i
  } // FindAllOcc Empty


  TEST(FindAllOccTest, Single) {
      auto occ = find_all_occ("CAD", "ABRA$CADABRA$");

      std::sort(occ.begin(), occ.end());
      ASSERT_EQ(occ.size(), 1);
      EXPECT_EQ(occ[0], 5);
  } // FindAllOcc Single


  TEST(FindAllOccTest, Many) {
      auto occ = find_all_occ("BRA", "ABRA$CADABRA$");

      std::sort(occ.begin(), occ.end());
      ASSERT_EQ(occ.size(), 2);
      EXPECT_EQ(occ[0], 1);
      EXPECT_EQ(occ[1], 9);
  } // FindAllOcc Many


  TEST_F(FindTest, Load) { /* nop */ }


  TEST_F(FindTest, Empty) {
      auto T = std::move(T_b4);
      EXPECT_EQ(T.find("", std::back_inserter(occ)), 0);
      std::sort(occ.begin(), occ.end());
      exp = { 0, 1, 2, 3, 5, 6, 7, 8, 10, 11, 12, 13, 15, 16, 17, 18, 19, 20, 21, 22 };
      EXPECT_EQ(occ, exp);
  } // FindTest Empty


  TEST_F(FindTest, ScanBreakLeft) {
      auto T = std::move(T_b4);
      EXPECT_EQ(T.find("GC", std::back_inserter(occ)), 2);
      std::sort(occ.begin(), occ.end());
      exp = find_all_occ("GC", ref0_text);
      EXPECT_EQ(occ, exp);
  } // FindTest ScanBreakLeft


  TEST_F(FindTest, ScanBreakRight) {
      auto T = std::move(T_b4);
      EXPECT_EQ(T.find("CGA", std::back_inserter(occ)), 2);
      std::sort(occ.begin(), occ.end());
      exp = find_all_occ("CG", ref0_text);
      EXPECT_EQ(occ, exp);
  } // FindTest ScanBreakRight


  TEST_F(FindTest, ScanBreakBoth) {
      auto T = std::move(T_b4);
      EXPECT_EQ(T.find("ACG", std::back_inserter(occ)), 3);
      std::sort(occ.begin(), occ.end());
      exp = find_all_occ("ACG", ref0_text);
      EXPECT_EQ(occ, exp);
  } // FindTest ScanBreakBoth


  TEST_F(FindTest, BeforeAll) {
      auto T = std::move(T_b4);
      EXPECT_EQ(T.find("A", std::back_inserter(occ)), 1);
      std::sort(occ.begin(), occ.end());
      exp = find_all_occ("A", ref0_text);
      EXPECT_EQ(occ, exp);
  } // FindTest BeforeAll


  TEST_F(FindTest, AfterAll) {
      auto T = std::move(T_b4);
      EXPECT_EQ(T.find("TTT", std::back_inserter(occ)), 2);
      std::sort(occ.begin(), occ.end());
      exp = find_all_occ("TT", ref0_text);
      EXPECT_EQ(occ, exp);
  } // FindTest AfterAll


  TEST_F(FindTest, B2OccsInDiffNodeThanPos) {
      auto T = std::move(T_b2);
      EXPECT_EQ(T.find("GTTT", std::back_inserter(occ)), 3);
      std::sort(occ.begin(), occ.end());
      exp = find_all_occ("GTT", ref0_text);
      EXPECT_EQ(occ, exp);
  } // FindTest B2OccsInDiffNodeThanPos


  TEST_F(FindTest, B4OccsInDiffNodeThanPos) {
      auto T = std::move(T_b4);
      EXPECT_EQ(T.find("GTTT", std::back_inserter(occ)), 3);
      std::sort(occ.begin(), occ.end());
      exp = find_all_occ("GTT", ref0_text);
      EXPECT_EQ(occ, exp);
  } // FindTest B4OccsInDiffNodeThanPos


  TEST_F(FindTest, ThresholdBelowMatch) {
      auto T = std::move(T_b2);
      EXPECT_EQ(T.find("GTC", std::back_inserter(occ), 1), 2);
      std::sort(occ.begin(), occ.end());
      exp = find_all_occ("GT", ref0_text);
      EXPECT_EQ(occ, exp);
  } // FindTest ThresholdBelowMatch


  TEST_F(FindTest, ThresholdEqualsMatch) {
      auto T = std::move(T_b2);
      EXPECT_EQ(T.find("GTC", std::back_inserter(occ), 2), 2);
      std::sort(occ.begin(), occ.end());
      exp = find_all_occ("GT", ref0_text);
      EXPECT_EQ(occ, exp);
  } // FindTest ThresholdEqualsMatch


  TEST_F(FindTest, ThresholdAboveMatch) {
      auto T = std::move(T_b2);
      EXPECT_EQ(T.find("GTC", std::back_inserter(occ), 3), 2);
      std::sort(occ.begin(), occ.end());
      exp = {};
      EXPECT_EQ(occ, exp);
  } // FindTest ThresholdAboveMatch


  TEST_F(FindTest, LastEntryDuplicates) {
      int num_thr = 1;

      #pragma omp parallel shared(num_thr)
      #pragma omp single
      num_thr = omp_get_num_threads();

      auto db_loc = TestEnv::test_tmp_dir + "/" + "ref1";

      ASSERT_NO_FATAL_FAILURE(sbt_preprocess(db_loc, "ref1"));
      ASSERT_NO_FATAL_FAILURE(sbt_construct<4>(db_loc, b4_cache * num_thr));

      sbt_b4_type T{};
      ASSERT_TRUE(T.load({sbt::text_name(db_loc)}, {sbt::csbt_name(db_loc)}, {sbt::dsuf_name(db_loc)}))
          << "Could not load sbt from " << db_loc;

      EXPECT_EQ(T.find("T", std::back_inserter(occ)), 1);

      auto text = load_text(sbt::text_name(db_loc));
      ASSERT_TRUE(text.has_value()) << "Failed to load text from " << db_loc;

      std::sort(occ.begin(), occ.end());
      exp = find_all_occ("T", text.value());
      EXPECT_EQ(occ, exp);
  } // FindTest LastEntryDuplicates


  template <int b>
  using mmap_sbtree = sbt::CSBTree<b, sbt::mmap_storage_manager, sbt::mmap_storage_manager, sbt::mmap_storage_manager,
                                        sbt::basic_memory_manager, sbt::basic_memory_manager, sbt::basic_memory_manager>;


  TEST(CreateTest, B2IntraNodeLCP) {
      auto db_loc = TestEnv::test_tmp_dir + "/intra_node_lcp_b2";
      sbt_preprocess(db_loc, "intra_node_lcp_b2");

      constexpr auto b = 2;
      mmap_sbtree<b> T;

      int num_thr = 1;

      #pragma omp parallel shared(num_thr)
      #pragma omp single
      num_thr = omp_get_num_threads();

      auto cache = 2 * T.node_size() * num_thr;
      sbt_construct<b>(db_loc, cache, "");

      ASSERT_TRUE(T.load({sbt::text_name(db_loc)}, {sbt::csbt_name(db_loc)}, {sbt::dsuf_name(db_loc)}))
          << "Could not load sbt from " << db_loc;

      auto& n = T.csbt_manager();
      auto r = n[n.size() - 1];

      { // prev lcp
          auto i = 0;

          // root
          EXPECT_EQ(r->LCP[0], 0);

          // leaves
          EXPECT_EQ(n[i++]->LCP[0], 0);
          EXPECT_EQ(n[i++]->LCP[0], 3);
          EXPECT_EQ(n[i++]->LCP[0], 2);
          EXPECT_EQ(n[i++]->LCP[0], 1);
          EXPECT_EQ(n[i++]->LCP[0], 0);
          EXPECT_EQ(n[i++]->LCP[0], 0);
          EXPECT_EQ(n[i++]->LCP[0], 2);
          EXPECT_EQ(n[i++]->LCP[0], 4);

          // first layer
          EXPECT_EQ(n[i++]->LCP[0], 0);
          EXPECT_EQ(n[i++]->LCP[0], 2);
          EXPECT_EQ(n[i++]->LCP[0], 0);
          EXPECT_EQ(n[i++]->LCP[0], 2);

          // second layer
          EXPECT_EQ(n[i++]->LCP[0], 0);
          EXPECT_EQ(n[i++]->LCP[0], 0);
      }

      { // next lcp
          // auto i = 0;

          // // root
          // EXPECT_EQ(r->LCP[b], 0);

          // // leaves
          // EXPECT_EQ(n[i++]->LCP[b], 3);
          // EXPECT_EQ(n[i++]->LCP[b], 2);
          // EXPECT_EQ(n[i++]->LCP[b], 1);
          // EXPECT_EQ(n[i++]->LCP[b], 0);
          // EXPECT_EQ(n[i++]->LCP[b], 0);
          // EXPECT_EQ(n[i++]->LCP[b], 2);
          // EXPECT_EQ(n[i++]->LCP[b], 4);
          // EXPECT_EQ(n[i++]->LCP[b], 0);

          // // first layer
          // EXPECT_EQ(n[i++]->LCP[b], 2);
          // EXPECT_EQ(n[i++]->LCP[b], 0);
          // EXPECT_EQ(n[i++]->LCP[b], 2);
          // EXPECT_EQ(n[i++]->LCP[b], 2);

          // // second layer
          // EXPECT_EQ(n[i++]->LCP[0], 0);
          // EXPECT_EQ(n[i++]->LCP[0], 0);
      }
  } // CreateTest B2IntraNodeLCP


  TEST(CreateTest, B4IntraNodeLCP) {
      auto db_loc = TestEnv::test_tmp_dir + "/intra_node_lcp_b4";
      sbt_preprocess(db_loc, "intra_node_lcp_b4");

      constexpr auto b = 4;
      mmap_sbtree<b> T;

      int num_thr = 1;

      #pragma omp parallel shared(num_thr)
      #pragma omp single
      num_thr = omp_get_num_threads();

      auto cache = 2 * T.node_size() * num_thr;
      sbt_construct<b>(db_loc, cache, "");

      ASSERT_TRUE(T.load({sbt::text_name(db_loc)}, {sbt::csbt_name(db_loc)}, {sbt::dsuf_name(db_loc)}))
          << "Could not load sbt from " << db_loc;

      auto r = T.csbt_manager()[T.csbt_manager().size() - 1];
      auto n1 = T.csbt_manager()[0];
      auto n2 = T.csbt_manager()[1];
      auto n3 = T.csbt_manager()[2];
      auto n4 = T.csbt_manager()[3];

      // prev lcp
      EXPECT_EQ(r->LCP[0], 0);
      EXPECT_EQ(n1->LCP[0], 0);
      EXPECT_EQ(n2->LCP[0], 3);
      EXPECT_EQ(n3->LCP[0], 1);
      EXPECT_EQ(n4->LCP[0], 0);

      // next lcp
      // EXPECT_EQ(r->LCP[b], 0);
      // EXPECT_EQ(n1->LCP[b], 3);
      // EXPECT_EQ(n2->LCP[b], 1);
      // EXPECT_EQ(n3->LCP[b], 0);
      // EXPECT_EQ(n4->LCP[b], 0);
  } // CreateTest B4IntraNodeLCP


  TEST(CreateTest, LCPAfterSkippedSuffix) {
      auto db_loc = TestEnv::test_tmp_dir + "/intra_node_lcp_b4";
      sbt_preprocess(db_loc, "ref_skip_short_suffix");

      constexpr auto b = 5;
      mmap_sbtree<b> T;

      int num_thr = 1;

      #pragma omp parallel shared(num_thr)
      #pragma omp single
      num_thr = omp_get_num_threads();

      auto cache = 2 * T.node_size() * num_thr;
      sbt_construct<b>(db_loc, cache, "", 5);

      ASSERT_TRUE(T.load({sbt::text_name(db_loc)}, {sbt::csbt_name(db_loc)}, {sbt::dsuf_name(db_loc)}))
          << "Could not load sbt from " << db_loc;
      ASSERT_EQ(T.csbt_manager().size(), 1);

      auto r = T.csbt_manager()[T.csbt_manager().size() - 1];

      // WARNING: this test assumes that the text fits in a single block!c
      ASSERT_EQ(r->size, 5);
      ASSERT_STREQ(T.text_manager()[0]->T + r->A[0], "ACGGTCGGT$");
      ASSERT_STREQ(T.text_manager()[0]->T + r->A[1], "CGGTCGGT$");
      ASSERT_STREQ(T.text_manager()[0]->T + r->A[2], "GGTCGGT$");
      ASSERT_STREQ(T.text_manager()[0]->T + r->A[3], "GTCGGT$");
      ASSERT_STREQ(T.text_manager()[0]->T + r->A[4], "TCGGT$");

      EXPECT_EQ(r->LCP[0], 0);
      EXPECT_EQ(r->LCP[1], 0); // if the lcp construction wasn't careful, this could be 4 from the skipped suffix
      EXPECT_EQ(r->LCP[2], 0);
      EXPECT_EQ(r->LCP[3], 1);
      EXPECT_EQ(r->LCP[4], 0);
  } // CreateTest LCPAfterSkippedSuffix


  TEST(CreateTest, CharArray) {
      auto db_loc = TestEnv::test_tmp_dir + "/intra_node_lcp_b2";
      sbt_preprocess(db_loc, "intra_node_lcp_b2");

      constexpr auto b = 2;
      mmap_sbtree<b> T;

      int num_thr = 1;

      #pragma omp parallel shared(num_thr)
      #pragma omp single
      num_thr = omp_get_num_threads();

      auto cache = 2 * T.node_size() * num_thr;
      sbt_construct<b>(db_loc, cache, "");

      ASSERT_TRUE(T.load({sbt::text_name(db_loc)}, {sbt::csbt_name(db_loc)}, {sbt::dsuf_name(db_loc)}))
          << "Could not load sbt from " << db_loc;

      auto& n_mngr = T.csbt_manager();
      auto& t_mngr = T.text_manager();

      for (auto i = 0ull; i < n_mngr.size(); ++i) {
          auto n = n_mngr[i];
          for (auto j = 1; j < n->size; ++j) {
              {
                  auto [bid, boff] = sbt::text_block_pos(n->A[j - 1] + n->LCP[j]);
                  EXPECT_EQ(n->C_L[j], t_mngr[bid]->T[boff]);
              }

              {
                  auto [bid, boff] = sbt::text_block_pos(n->A[j] + n->LCP[j]);
                  EXPECT_EQ(n->C_R[j], t_mngr[bid]->T[boff]);
              }
          }
      }
  } // CreateTest CharArray


  TEST(CreateTest, B2IntraNodeLCPWithSmallCache) {
      auto db_loc = TestEnv::test_tmp_dir + "/intra_node_lcp_b2";
      sbt_preprocess(db_loc, "intra_node_lcp_b2");

      constexpr auto b = 2;
      mmap_sbtree<b> T;

      int num_thr = 1;

      #pragma omp parallel shared(num_thr)
      #pragma omp single
      num_thr = omp_get_num_threads();

      // construct using a cache fitting only a single node
      auto cache = T.node_size() * num_thr;
      sbt_construct<b>(db_loc, cache, "");

      ASSERT_TRUE(T.load({sbt::text_name(db_loc)}, {sbt::csbt_name(db_loc)}, {sbt::dsuf_name(db_loc)}))
          << "Could not load sbt from " << db_loc;

      auto& n = T.csbt_manager();
      auto r = n[n.size() - 1];

      { // prev lcp
          auto i = 0;

          // root
          EXPECT_EQ(r->LCP[0], 0);

          // leaves
          EXPECT_EQ(n[i++]->LCP[0], 0);
          EXPECT_EQ(n[i++]->LCP[0], 3);
          EXPECT_EQ(n[i++]->LCP[0], 2);
          EXPECT_EQ(n[i++]->LCP[0], 1);
          EXPECT_EQ(n[i++]->LCP[0], 0);
          EXPECT_EQ(n[i++]->LCP[0], 0);
          EXPECT_EQ(n[i++]->LCP[0], 2);
          EXPECT_EQ(n[i++]->LCP[0], 4);

          // first layer
          EXPECT_EQ(n[i++]->LCP[0], 0);
          EXPECT_EQ(n[i++]->LCP[0], 2);
          EXPECT_EQ(n[i++]->LCP[0], 0);
          EXPECT_EQ(n[i++]->LCP[0], 2);

          // second layer
          EXPECT_EQ(n[i++]->LCP[0], 0);
          EXPECT_EQ(n[i++]->LCP[0], 0);
      }

      { // next lcp
          // auto i = 0;

          // // root
          // EXPECT_EQ(r->LCP[b], 0);

          // // leaves
          // EXPECT_EQ(n[i++]->LCP[b], 3);
          // EXPECT_EQ(n[i++]->LCP[b], 2);
          // EXPECT_EQ(n[i++]->LCP[b], 1);
          // EXPECT_EQ(n[i++]->LCP[b], 0);
          // EXPECT_EQ(n[i++]->LCP[b], 0);
          // EXPECT_EQ(n[i++]->LCP[b], 2);
          // EXPECT_EQ(n[i++]->LCP[b], 4);
          // EXPECT_EQ(n[i++]->LCP[b], 0);

          // // first layer
          // EXPECT_EQ(n[i++]->LCP[b], 2);
          // EXPECT_EQ(n[i++]->LCP[b], 0);
          // EXPECT_EQ(n[i++]->LCP[b], 2);
          // EXPECT_EQ(n[i++]->LCP[b], 2);

          // // second layer
          // EXPECT_EQ(n[i++]->LCP[0], 0);
          // EXPECT_EQ(n[i++]->LCP[0], 0);
      }
  } // CreateTest B2IntraNodeLCPWithSmallCache


  TEST(CreateTest, CharArrayWithSmallCache) {
      auto db_loc = TestEnv::test_tmp_dir + "/intra_node_lcp_b2";
      sbt_preprocess(db_loc, "intra_node_lcp_b2");

      constexpr auto b = 2;
      mmap_sbtree<b> T;

      int num_thr = 1;

      #pragma omp parallel shared(num_thr)
      #pragma omp single
      num_thr = omp_get_num_threads();

      // construct using a cache fitting only a single node
      auto cache = T.node_size() * num_thr;
      sbt_construct<b>(db_loc, cache, "");

      ASSERT_TRUE(T.load({sbt::text_name(db_loc)}, {sbt::csbt_name(db_loc)}, {sbt::dsuf_name(db_loc)}))
          << "Could not load sbt from " << db_loc;

      auto& n_mngr = T.csbt_manager();
      auto& t_mngr = T.text_manager();

      for (auto i = 0ull; i < n_mngr.size(); ++i) {
          auto n = n_mngr[i];
          for (auto j = 1; j < n->size; ++j) {
              {
                  auto [bid, boff] = sbt::text_block_pos(n->A[j - 1] + n->LCP[j]);
                  EXPECT_EQ(n->C_L[j], t_mngr[bid]->T[boff]);
              }

              {
                  auto [bid, boff] = sbt::text_block_pos(n->A[j] + n->LCP[j]);
                  EXPECT_EQ(n->C_R[j], t_mngr[bid]->T[boff]);
              }
          }
      }
  } // CreateTest WithSmallCache


  TEST(CreateTest, B2IntraNodeLCPWithLargeCache) {
      auto db_loc = TestEnv::test_tmp_dir + "/intra_node_lcp_b2";
      sbt_preprocess(db_loc, "intra_node_lcp_b2");

      constexpr auto b = 2;
      mmap_sbtree<b> T;

      int num_thr = 1;

      #pragma omp parallel shared(num_thr)
      #pragma omp single
      num_thr = omp_get_num_threads();

      // construct using a cache fitting many nodes
      auto cache = 32 * T.node_size() * num_thr;
      sbt_construct<b>(db_loc, cache, "");

      ASSERT_TRUE(T.load({sbt::text_name(db_loc)}, {sbt::csbt_name(db_loc)}, {sbt::dsuf_name(db_loc)}))
          << "Could not load sbt from " << db_loc;

      auto& n = T.csbt_manager();
      auto r = n[n.size() - 1];

      { // prev lcp
          auto i = 0;

          // root
          EXPECT_EQ(r->LCP[0], 0);

          // leaves
          EXPECT_EQ(n[i++]->LCP[0], 0);
          EXPECT_EQ(n[i++]->LCP[0], 3);
          EXPECT_EQ(n[i++]->LCP[0], 2);
          EXPECT_EQ(n[i++]->LCP[0], 1);
          EXPECT_EQ(n[i++]->LCP[0], 0);
          EXPECT_EQ(n[i++]->LCP[0], 0);
          EXPECT_EQ(n[i++]->LCP[0], 2);
          EXPECT_EQ(n[i++]->LCP[0], 4);

          // first layer
          EXPECT_EQ(n[i++]->LCP[0], 0);
          EXPECT_EQ(n[i++]->LCP[0], 2);
          EXPECT_EQ(n[i++]->LCP[0], 0);
          EXPECT_EQ(n[i++]->LCP[0], 2);

          // second layer
          EXPECT_EQ(n[i++]->LCP[0], 0);
          EXPECT_EQ(n[i++]->LCP[0], 0);
      }

      { // next lcp
          // auto i = 0;

          // // root
          // EXPECT_EQ(r->LCP[b], 0);

          // // leaves
          // EXPECT_EQ(n[i++]->LCP[b], 3);
          // EXPECT_EQ(n[i++]->LCP[b], 2);
          // EXPECT_EQ(n[i++]->LCP[b], 1);
          // EXPECT_EQ(n[i++]->LCP[b], 0);
          // EXPECT_EQ(n[i++]->LCP[b], 0);
          // EXPECT_EQ(n[i++]->LCP[b], 2);
          // EXPECT_EQ(n[i++]->LCP[b], 4);
          // EXPECT_EQ(n[i++]->LCP[b], 0);

          // // first layer
          // EXPECT_EQ(n[i++]->LCP[b], 2);
          // EXPECT_EQ(n[i++]->LCP[b], 0);
          // EXPECT_EQ(n[i++]->LCP[b], 2);
          // EXPECT_EQ(n[i++]->LCP[b], 2);

          // // second layer
          // EXPECT_EQ(n[i++]->LCP[0], 0);
          // EXPECT_EQ(n[i++]->LCP[0], 0);
      }
  } // CreateTest B2IntraNodeLCPWithLargeCache


  TEST(CreateTest, CharArrayWithLargeCache) {
      auto db_loc = TestEnv::test_tmp_dir + "/intra_node_lcp_b2";
      sbt_preprocess(db_loc, "intra_node_lcp_b2");

      constexpr auto b = 2;
      mmap_sbtree<b> T;

      int num_thr = 1;

      #pragma omp parallel shared(num_thr)
      #pragma omp single
      num_thr = omp_get_num_threads();

      // construct using a cache fitting many nodes
      auto cache = 32 * T.node_size() * num_thr;
      sbt_construct<b>(db_loc, cache, "");

      ASSERT_TRUE(T.load({sbt::text_name(db_loc)}, {sbt::csbt_name(db_loc)}, {sbt::dsuf_name(db_loc)}))
          << "Could not load sbt from " << db_loc;

      auto& n_mngr = T.csbt_manager();
      auto& t_mngr = T.text_manager();

      for (auto i = 0ull; i < n_mngr.size(); ++i) {
          auto n = n_mngr[i];
          for (auto j = 1; j < n->size; ++j) {
              {
                  auto [bid, boff] = sbt::text_block_pos(n->A[j - 1] + n->LCP[j]);
                  EXPECT_EQ(n->C_L[j], t_mngr[bid]->T[boff]);
              }

              {
                  auto [bid, boff] = sbt::text_block_pos(n->A[j] + n->LCP[j]);
                  EXPECT_EQ(n->C_R[j], t_mngr[bid]->T[boff]);
              }
          }
      }
  } // CreateTest WithLargeCache



  TEST_F(FindTest, ConstructCharArray) {
      auto T = std::move(T_b4);
      auto& n_mngr = T.csbt_manager();
      auto& t_mngr = T.text_manager();


      for (auto i = 0ull; i < n_mngr.size(); ++i) {
          auto n = n_mngr[i];
          for (auto j = 1; j < n->size; ++j) {
              {
                  auto [bid, boff] = sbt::text_block_pos(n->A[j - 1] + n->LCP[j]);
                  EXPECT_EQ(n->C_L[j], t_mngr[bid]->T[boff]);
              }

              {
                  auto [bid, boff] = sbt::text_block_pos(n->A[j] + n->LCP[j]);
                  EXPECT_EQ(n->C_R[j], t_mngr[bid]->T[boff]);
              }
          }
      }

      ASSERT_EQ(n_mngr.size(), 5);

      auto r = n_mngr[n_mngr.size() - 1];
      auto n1 = n_mngr[0];
      auto n2 = n_mngr[1];
      auto n3 = n_mngr[2];
      auto n4 = n_mngr[3];

      ASSERT_EQ(r->size, 4);
      ASSERT_EQ(n1->size, 4);
      ASSERT_EQ(n2->size, 4);
      ASSERT_EQ(n3->size, 4);

      // root
      EXPECT_EQ(r->C_L[1], 'A');
      EXPECT_EQ(r->C_L[2], 'C');
      EXPECT_EQ(r->C_L[3], 'G');

      EXPECT_EQ(r->C_R[1], 'C');
      EXPECT_EQ(r->C_R[2], 'G');
      EXPECT_EQ(r->C_R[3], 'T');

      // node 1
      EXPECT_EQ(n1->C_L[1], '$');
      EXPECT_EQ(n1->C_L[2], '$');
      EXPECT_EQ(n1->C_L[3], 'C');

      EXPECT_EQ(n1->C_R[1], 'C');
      EXPECT_EQ(n1->C_R[2], 'T');
      EXPECT_EQ(n1->C_R[3], 'T');

      // node 2
      EXPECT_EQ(n2->C_L[1], '$');
      EXPECT_EQ(n2->C_L[2], 'A');
      EXPECT_EQ(n2->C_L[3], '$');

      EXPECT_EQ(n2->C_R[1], 'A');
      EXPECT_EQ(n2->C_R[2], 'G');
      EXPECT_EQ(n2->C_R[3], 'T');

      // node 3
      EXPECT_EQ(n3->C_L[1], '$');
      EXPECT_EQ(n3->C_L[2], 'C');
      EXPECT_EQ(n3->C_L[3], '$');

      EXPECT_EQ(n3->C_R[1], 'A');
      EXPECT_EQ(n3->C_R[2], 'T');
      EXPECT_EQ(n3->C_R[3], 'T');

      // node 4
      EXPECT_EQ(n4->C_L[1], '$');
      EXPECT_EQ(n4->C_L[2], '$');
      EXPECT_EQ(n4->C_L[3], 'G');

      EXPECT_EQ(n4->C_R[1], 'G');
      EXPECT_EQ(n4->C_R[2], 'A');
      EXPECT_EQ(n4->C_R[3], 'T');

  } // FindTest ConstructCharArray

} // namespace
