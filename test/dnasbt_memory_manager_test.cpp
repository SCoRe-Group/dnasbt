#include <algorithm>
#include <cstdint>
#include <random>
#include <utility>

#include <gtest/gtest.h>

#include <dnasbt/dnasbt_config.hpp>
#include <dnasbt/dnasbt_memory_manager.hpp>
#include <dnasbt/dnasbt_storage_manager.hpp>

/* #include <dnasbt/experimental/dnasbt_experimental_memory_manager.hpp> */

#include "aligned_block.hpp"
#include "environments.hpp"
#include "make_memory_manager.hpp"
#include "test.hpp"


namespace {

  using namespace sbt::test;


  using value_type = std::uint64_t;

  using storage_manager_type = sbt::basic_storage_manager<value_type>;

  template <typename MemoryManager>
  class MemoryManagerTest : public ::testing::Test {
  protected:
      using manager_type = MemoryManager;
  }; // class MemoryManagerTest

  using MemoryManagerTestTypes = ::testing::Types<sbt::basic_memory_manager<storage_manager_type>,
                                                  sbt::alloc_memory_manager<storage_manager_type>,
                                                  sbt::cache_memory_manager<storage_manager_type>
                                                  //sbt::experimental::experimental_memory_manager<storage_manager_type>
                                                  >;

  TYPED_TEST_SUITE(MemoryManagerTest, MemoryManagerTestTypes, );


  /// Checks (via gtest asserts/expectations) that the contents of the given manager match that of the given data.
  template <typename MemoryManager>
  void validate_memory_manager(const MemoryManager& memory, const std::vector<value_type>& data) {
      ASSERT_EQ(memory.size(), data.size());
      for (auto i = 0ull; i < data.size(); ++i) {
          auto ptr = memory[i];
          EXPECT_EQ(*ptr, data[i]);
      } // for i
  } // validate_memory_manager


  TYPED_TEST(MemoryManagerTest, SequentialAccess) {
      using memory_manager_type = typename TestFixture::manager_type;
      constexpr auto data_size = 16ull;

      // fill storage s.t. storage[i] = 2 * i
      std::vector<value_type> data{};
      for (auto i = 0ull; i < data_size; ++i) {
          data.push_back(2 * i);
      } // for i

      sbt::basic_storage_manager<value_type> storage{data.data(), data.size()};
      auto memory = make_memory_manager<memory_manager_type>(std::move(storage));

      ASSERT_EQ(memory.size(), data_size);

      validate_memory_manager(memory, data);
  } // SequentialAccess


  TYPED_TEST(MemoryManagerTest, RepeatedAccess) {
      using memory_manager_type = typename TestFixture::manager_type;
      constexpr auto data_size = 16ull;

      // fill storage s.t. storage[i] = 2 * i
      std::vector<value_type> data{};
      for (auto i = 0ull; i < data_size; ++i) {
          data.push_back(2 * i);
      } // for i

      // follow a zipfian distribution
      std::vector<std::size_t> frequencies{1 << data_size};
      for (auto i = 1ull; i < data_size; ++i) {
          frequencies.push_back(frequencies[i - 1] / 2);
      } // for i

      sbt::basic_storage_manager<value_type> storage{data.data(), data.size()};
      auto memory = make_memory_manager<memory_manager_type>(std::move(storage));

      ASSERT_EQ(memory.size(), data_size);

      for (auto i = 0ull; i < data.size(); ++i) {
          auto times = frequencies[i];
          for (auto j = 0ull; j < times; ++j) {
              auto ptr = memory[i];
              EXPECT_EQ(*ptr, data[i]);
          } // for j
      } // for i

      // do the same queries in reverse order
      for (auto k = 0ull; k < data.size(); ++k) {
          auto i = data.size() - k - 1;
          auto times = frequencies[i];
          for (auto j = 0ull; j < times; ++j) {
              auto ptr = memory[i];
              EXPECT_EQ(*ptr, data[i]);
          } // for j
      } // for i
  } // RepeatedAccess


  TYPED_TEST(MemoryManagerTest, Move) {
      using memory_manager_type = typename TestFixture::manager_type;
      constexpr auto data_size = 16ull;

      // fill storage s.t. storage[i] = 2 * i
      std::vector<value_type> data{};
      for (auto i = 0ull; i < data_size; ++i) {
          data.push_back(2 * i);
      } // for i

      sbt::basic_storage_manager<value_type> storage{data.data(), data.size()};
      auto memory_original = make_memory_manager<memory_manager_type>(std::move(storage));

      ASSERT_EQ(memory_original.size(), data_size);
      validate_memory_manager(memory_original, data);

      // test move constructor
      memory_manager_type memory_move_ctor{std::move(memory_original)};

      ASSERT_EQ(memory_move_ctor.size(), data_size);
      validate_memory_manager(memory_move_ctor, data);

      // test move assignment
      memory_manager_type memory_move_asgn;
      memory_move_asgn = std::move(memory_move_ctor);

      ASSERT_EQ(memory_move_asgn.size(), data_size);
      validate_memory_manager(memory_move_asgn, data);
  } // Move


  TYPED_TEST(MemoryManagerTest, MovePointer) {
      using memory_manager_type = typename TestFixture::manager_type;

      std::vector<value_type> data{0, 1, 2, 3};

      sbt::basic_storage_manager<value_type> storage{data.data(), data.size()};
      auto memory = make_memory_manager<memory_manager_type>(std::move(storage));

      auto ptr_orig = memory[0];
      EXPECT_EQ(*ptr_orig, 0);

      auto ptr_ctor{std::move(ptr_orig)};
      EXPECT_EQ(*ptr_ctor, 0);

      auto ptr_assn = std::move(ptr_ctor);
      EXPECT_EQ(*ptr_assn, 0);
  } // Move


  TYPED_TEST(MemoryManagerTest, RandomAccess) {
      using memory_manager_type = typename TestFixture::manager_type;
      constexpr auto data_size = 16ull;

      // fill storage s.t. storage[i] = 2 * i
      std::vector<value_type> data{};
      std::vector<typename memory_manager_type::size_type> access_order{};
      for (auto i = 0ull; i < data_size; ++i) {
          data.push_back(2 * i);
          access_order.push_back(i);
      } // for i

      std::random_device rd;
      std::mt19937 g{rd()};
      std::shuffle(access_order.begin(), access_order.end(), g);

      sbt::basic_storage_manager<value_type> storage{data.data(), data.size()};
      auto memory = make_memory_manager<memory_manager_type>(std::move(storage));

      ASSERT_EQ(memory.size(), data_size);

      for (auto i : access_order) {
          auto ptr = memory[i];
          EXPECT_EQ(*ptr, data[i]);
      } // for i
  } // RandomAccess


  TYPED_TEST(MemoryManagerTest, ConcurrentAccess) {
      using memory_manager_type = typename TestFixture::manager_type;
      constexpr auto data_size = 16ull;

      int num_threads = 1;
      #pragma omp parallel shared(num_threads)
      #pragma omp single
      num_threads = omp_get_num_threads();

      // check that the test is actually multi-threaded
      ASSERT_GT(num_threads, 1) << "There are only " << num_threads <<  " available to the system";
      ASSERT_LE(num_threads, sbt::test::cache_size) << "The number of threads (" << num_threads
                                                    << ") exceeds the test cache size, which violates the assumptions "
                                                    << "of some cache managers";

      // fill storage s.t. storage[i] = 2 * i
      std::vector<value_type> data{};
      for (auto i = 0ull; i < data_size; ++i) {
          data.push_back(2 * i);
      } // for i

      // use separate buffers for the outputs for each thread
      std::vector<std::vector<value_type>> outputs(num_threads);
      for (int i = 0; i < num_threads; ++i) {
          outputs[i] = std::vector<value_type>(data_size);
      } // for i

      sbt::basic_storage_manager<value_type> storage{data.data(), data.size()};
      auto memory = make_memory_manager<memory_manager_type>(std::move(storage));

      ASSERT_EQ(memory.size(), data_size);

      // get outputs for each thread
      #pragma omp parallel for
      for (int i = 0; i < num_threads; ++i) {
          for (auto j = 0ull; j < memory.size(); ++j) {
              auto ptr = memory[j];
              outputs[i][j] = *ptr;
          } // for j
      } // for i

      // validate outputs
      for (int i = 0; i < num_threads; ++i) {
          for (auto j = 0ull; j < data_size; ++j) {
              EXPECT_EQ(outputs[i][j], data[j]);
          } // for j
      } // for i
  } // ConcurrentAccess

} // namespace

namespace sbt {

  class CacheMemoryManagerTest : public testing::Test {
  protected:
  }; // class CacheMemoryManagerTest

  TEST_F(CacheMemoryManagerTest, UsesLfuPolicy) {
      auto constexpr cache_size = 4ull;

      std::vector<value_type> data{0, 1, 2, 3, 4, 5, 6};
      basic_storage_manager<value_type> storage{data.data(), data.size()};
      cache_memory_manager<basic_storage_manager<value_type>> memory{cache_size, std::move(storage)};

      ASSERT_EQ(memory.size(), data.size());
      ASSERT_EQ(memory.cache_capacity(), cache_size);
      ASSERT_EQ(memory.cache_size(), 0);

      // we are trying to evaluate the side-effects on cache
      // we scope this way so the handles are released before the next is accessed
      { auto _ = memory[1]; } // 2x 1
      { auto _ = memory[1]; }
      { auto _ = memory[2]; } // 5x 2
      { auto _ = memory[2]; }
      { auto _ = memory[2]; }
      { auto _ = memory[2]; }
      { auto _ = memory[2]; }
      { auto _ = memory[3]; } // 3x 3
      { auto _ = memory[3]; }
      { auto _ = memory[3]; }
      { auto _ = memory[4]; } // 1x 4
      { auto _ = memory[5]; } // 4x 5
      { auto _ = memory[5]; }
      { auto _ = memory[5]; }
      { auto _ = memory[5]; }

      { auto _ = memory[2]; }

      EXPECT_EQ(memory.cache_size(), cache_size);
      EXPECT_NE(std::find(memory.data_.begin(), memory.data_.end(), 1), memory.data_.end());
      EXPECT_NE(std::find(memory.data_.begin(), memory.data_.end(), 2), memory.data_.end());
      EXPECT_NE(std::find(memory.data_.begin(), memory.data_.end(), 3), memory.data_.end());
      EXPECT_NE(std::find(memory.data_.begin(), memory.data_.end(), 5), memory.data_.end());

      { auto _ = memory[6]; }

      EXPECT_EQ(memory.cache_size(), cache_size);
      EXPECT_NE(std::find(memory.data_.begin(), memory.data_.end(), 2), memory.data_.end());
      EXPECT_NE(std::find(memory.data_.begin(), memory.data_.end(), 3), memory.data_.end());
      EXPECT_NE(std::find(memory.data_.begin(), memory.data_.end(), 5), memory.data_.end());
      EXPECT_NE(std::find(memory.data_.begin(), memory.data_.end(), 6), memory.data_.end());
  } // UsesLfuPolicy
} // namespace sbt
