/// Some simple tests to check that sanitizers are working (these are expected to cause a sanitizer error, and thus
/// are disabled by default). Note that the asserts here don't mean much, and sometimes are intentionally wrong, since
/// the goal is for the sanitizer to catch the errors.

#include <iostream>
#include <limits>
#include <vector>

#include <gtest/gtest.h>

TEST(UB, DISABLED_NullPtr) {
    int* p1 = nullptr;
    int* p2 = nullptr;
    std::cout << (*p1 == *p2) << std::endl;
}

TEST(UB, DISABLED_OutOfBounds) {
    std::vector vec{0, 1, 2, 3};
    std::cout << vec[-1] << " " << vec[4] << std::endl;
}

TEST(UB, DISABLED_MemoryLeak) {
    int* p = new int;
    *p = 0;

    std::cout << (*p) << std::endl;

    p = new int;
    *p = 1;

    std::cout << (*p) << std::endl;
}

TEST(UB, DISABLED_SignedOverflow) {
    int i = std::numeric_limits<int>::max();
    ++i;
    std::cout << (i > std::numeric_limits<int>::max()) << std::endl;
}

TEST(UB, DISABLED_SignedUnderflow) {
    int i = std::numeric_limits<int>::min();
    --i;
    std::cout << (i < std::numeric_limits<int>::min()) << std::endl;
}

TEST(UB, DISABLED_UnsignedOverflow) {
    unsigned int i = std::numeric_limits<unsigned int>::max();
    ++i;
    std::cout << (i > std::numeric_limits<unsigned int>::max()) << std::endl;
}

TEST(UB, DISABLED_UnsignedUnderflow) {
    unsigned int i = std::numeric_limits<unsigned int>::min();
    --i;
    std::cout << (i < std::numeric_limits<unsigned int>::min()) << std::endl;
}
