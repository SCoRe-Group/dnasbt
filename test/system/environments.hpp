#ifndef DNASBT_TEST_SYSTEM_ENVIRONMENTS_HPP
#define DNASBT_TEST_SYSTEM_ENVIRONMENTS_HPP

#include <cstdlib>
#include <string>

#include <gtest/gtest.h>

#include <dnasbt/dnasbt.hpp>

#include <test.hpp>


namespace sbt::test {

  // Testing environment for system tests.
  class SysTestEnv : public ::testing::Environment {
  public:
      // names of databases constructed in the environment
      static constexpr auto sys_test_db = "sys_test_db";

      // block size used for construction
      static constexpr auto b = sbt::csbtree_suffixes_per_node<DNASBT_PAGE_SIZE>();

      // loaded environment variables
      //static inline std::string sys_test_tmp_dir = "";
      //static inline std::string sys_test_reference_dir = "";
      static inline std::string sys_test_db_loc = "";
      static inline int sys_test_db_lim = -1;


      void SetUp() override {
          // load environment variables
          constexpr auto sys_test_db_loc_var = "SYS_TEST_DB_LOC";
          if (!std::getenv(sys_test_db_loc_var)) {
              FAIL() << "Environment variable " << sys_test_db_loc_var << " was undefined";
              return;
          }
          sys_test_db_loc = std::getenv(sys_test_db_loc_var);
          SUCCEED() << sys_test_db_loc_var << ": " << sys_test_db_loc;

          constexpr auto sys_test_db_lim_var = "SYS_TEST_DB_LIM";
          if (!std::getenv(sys_test_db_lim_var)) {
              FAIL() << "Environment variable " << sys_test_db_lim_var << " was undefined";
              return;
          }
          sys_test_db_lim = std::atoi(std::getenv(sys_test_db_lim_var));
          SUCCEED() << sys_test_db_lim_var << ": " << sys_test_db_lim;
      } // SetUp

      void TearDown() override {
          // nop
      } // TearDown
  }; // class SysTestEnv

} // namespace sbt::test

#endif // DNASBT_TEST_SYSTEM_ENVIRONMENTS_HPP
