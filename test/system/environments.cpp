#include <gtest/gtest.h>

#include "environments.hpp"

namespace sbt::test {
  // add all global system testing environments
  static auto sys_test_env = ::testing::AddGlobalTestEnvironment(new SysTestEnv);
}


