#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <optional>
#include <type_traits>

#include <gtest/gtest.h>

#include <dnasbt/dnasbt.hpp>
#include <dnasbt/dnasbt_memory_manager.hpp>
#include <dnasbt/dnasbt_storage_manager.hpp>

#include <environments.hpp>
#include <test.hpp>

#include "environments.hpp"


namespace {

  using namespace sbt::test;

  struct with_compression {
      static constexpr bool compression = true;
      static constexpr std::size_t cache = 0;
  };

  struct no_compression {
      static constexpr bool compression = false;
      static constexpr std::size_t cache = 0;
  };

  struct with_cache {
      static constexpr bool compression = false;
      static constexpr std::size_t cache = 4096;
  };

  template <typename T>
  class SbtTest : public ::testing::Test {
  protected:
      using index_type = typename T::first_type;
      using config_type = typename T::second_type;

      index_type index;
      std::size_t limit;

      void SetUp() override {
          // load the index depending on whether compression, caching are used or not
          typename index_type::sbt_manager_type sbt_manager;
          if constexpr (config_type::cache > 0) {
              sbt_manager = std::move(typename index_type::sbt_manager_type{config_type::cache,
                                      {sbt::pcsbt_name(SysTestEnv::sys_test_db_loc)}});
          } else {
              sbt_manager = std::move(typename index_type::sbt_manager_type{sbt::pcsbt_name(SysTestEnv::sys_test_db_loc)});
          }

          if constexpr (config_type::compression) {
              ASSERT_TRUE(index.load({sbt::text_name(SysTestEnv::sys_test_db_loc)}, std::move(sbt_manager),
                                     {sbt::dsuf_name(SysTestEnv::sys_test_db_loc)}))
                  << "Failed to load index";
          } else {
              ASSERT_TRUE(index.load({sbt::text_name(SysTestEnv::sys_test_db_loc)}, std::move(sbt_manager),
                                     {sbt::dsuf_name(SysTestEnv::sys_test_db_loc)}))
                  << "Failed to load index";
          }

          ASSERT_GT(SysTestEnv::sys_test_db_lim, 0) << "Invalid limit parameter";
          limit = SysTestEnv::sys_test_db_lim;

      } // SetUp

  }; // class SbtTest


  template<template<typename> typename SbtStorageManager, template<typename> typename SbtMemoryManager>
  using sbtree_type = sbt::CSBTree<SysTestEnv::b, sbt::mmap_storage_manager, SbtStorageManager, sbt::mmap_storage_manager,
                                                    sbt::basic_memory_manager, SbtMemoryManager, sbt::basic_memory_manager>;

  using SbtTestTypes = ::testing::Types<std::pair<sbtree_type<sbt::mmap_storage_manager, sbt::basic_memory_manager>, no_compression>,
                                        std::pair<sbtree_type<sbt::lz4_mmap_storage_manager, sbt::alloc_memory_manager>, with_compression>,
                                        std::pair<sbtree_type<sbt::direct_io_storage_manager, sbt::cache_memory_manager>, with_cache>>;

  TYPED_TEST_SUITE(SbtTest, SbtTestTypes, );


  TYPED_TEST(SbtTest, ContainsAllWholeSequences) {
      // load text
      auto o_text = load_text(sbt::text_name(SysTestEnv::sys_test_db_loc));
      ASSERT_TRUE(o_text) << "Failed to load text";

      // search for every complete sequence from the reference string
      const std::string& text = *o_text;
      std::string::size_type first = 0, last = text.find_first_of("$", 0);
      while (last != std::string::npos) {
          ASSERT_LT(first, last) << "Bug in test: beginning of sequence came after end of sequence"
                                 << " (note that this failure could also occur if the reference contains $$)";
          std::string::size_type seq_len = last - first + 1;

          // search for the current sequence (omitting terminating $)
          std::vector<std::string::size_type> occ;
          auto lcp = this->index.find(std::string_view(text.c_str() + first, seq_len - 1), std::back_inserter(occ));

          ASSERT_EQ(lcp, seq_len - 1) << "Failed on sequence " << first;
          ASSERT_GT(occ.size(), 0) << "Failed on sequence " << first;
          ASSERT_NE(std::find(occ.begin(), occ.end(), first), occ.end()) << "Failed on sequence " << first;

          // go to next reference sequence
          first = last + 1;
          last = text.find_first_of("$", last + 1);
      } // while last
  } // ContainsAllWholeSequences


  TYPED_TEST(SbtTest, DISABLED_EachSuffixLcpMatchesLength) {
      // load text
      auto o_text = load_text(sbt::text_name(SysTestEnv::sys_test_db_loc));
      ASSERT_TRUE(o_text) << "Failed to load text";

      // search for every suffix from the reference string
      const std::string& text = *o_text;
      std::string::size_type first = 0, last = text.find_first_of("$", 0);
      while (last != std::string::npos) {
          ASSERT_LT(first, last) << "Bug in test: beginning of suffix came after end of suffix"
                                 << " (note that this failure could also occur if the reference contains $$)";
          std::string::size_type suff_len = last - first + 1; // suffix length including $

          // skip suffixes too small to be indexed
          if (suff_len - 1 >= this->limit) {
              // search for the current suffix (omitting terminating $)
              std::vector<std::string::size_type> occ;
              auto lcp = this->index.find(std::string_view(text.c_str() + first, suff_len - 1), std::back_inserter(occ), false);

              ASSERT_EQ(lcp, suff_len - 1) << "Failed on suffix " << first;
          }


          // go to next suffix
          first += 1;
          if (first >= last) {
              first += 1; // skip suffix $
              last = text.find_first_of("$", last + 1);
          }
      } // while last
  } // EachSuffixLcpMatchesLength


  TYPED_TEST(SbtTest, FindsAllAtLimit) {
      // load test
      auto o_text = load_text(sbt::text_name(SysTestEnv::sys_test_db_loc));
      ASSERT_TRUE(o_text) << "Failed to load text";

      // search for the lim-length suffix from every reference sequence (i.e., the smallest indexed suffix)
      const std::string& text = *o_text;
      std::string::size_type first = 0, last = text.find_first_of("$", 0);
      while (last != std::string::npos) {
          ASSERT_LT(first, last) << "Bug in test: beginning of suffix came after end of suffix"
                                 << " (note that this failure could also occur if the reference contains $$)";
          std::string::size_type seq_len = last - first + 1;
          if (seq_len < this->limit) {
              std::cout << "Found sequence smaller than limit. Is this a bug or was the database misconfigured? Skipping sequence..."
                        << std::endl;
          } else {
              ASSERT_GE(last, this->limit) << "Error: extracted suffix was invalid (further execution would underflow)";

              // extract suffix of length limit from the sequence (i.e., smallest indexed suffix of the sequence)
              std::string::size_type suff_pos = last - this->limit;
              std::string_view suff{text.c_str() + suff_pos, this->limit};

              // search for the suffix (omitting terminating $)
              std::vector<std::string::size_type> occ;
              auto lcp = this->index.find(suff, std::back_inserter(occ));

              ASSERT_EQ(lcp, this->limit) << "Failed on sequence " << first << " with suffix " << suff_pos;
              ASSERT_GT(occ.size(), 0) << "Failed on sequence " << first << " with suffix " << suff_pos;
              ASSERT_NE(std::find(occ.begin(), occ.end(), suff_pos), occ.end()) << "Failed on sequence " << first
                                                                                << " with suffix " << suff_pos;
          }

          // go to next reference sequence
          first = last + 1;
          last = text.find_first_of("$", last + 1);
      }
  }


  TYPED_TEST(SbtTest, SmallerThanLimitDoesNotOccur) {
      if (this->limit > 1) {
          // load test
          auto o_text = load_text(sbt::text_name(SysTestEnv::sys_test_db_loc));
          ASSERT_TRUE(o_text) << "Failed to load text";

          const std::string& text = *o_text;
          std::string::size_type first = 0, last = text.find_first_of("$", 0);
          while (last != std::string::npos) {
              ASSERT_LT(first, last) << "Bug in test: beginning of suffix came after end of suffix"
                                    << " (note that this failure could also occur if the reference contains $$)";
              std::string::size_type seq_len = last - first + 1;
              if (seq_len < this->limit) {
                  std::cout << "Found sequence smaller than limit. Is this a bug or was the database misconfigured? Skipping sequence..."
                            << std::endl;
              } else {
                  ASSERT_GE(last + 1, this->limit) << "Error: extracted suffix was invalid (further execution would underflow)";

                  // extract suffix smaller than the smallest indexed, i.e., one that should not occur in the index
                  std::string::size_type suff_pos = last + 1 - this->limit;
                  std::string_view suff{text.c_str() + suff_pos, this->limit};

                  // search for the suffix (omitting terminating $)
                  std::vector<std::string::size_type> occ;
                  this->index.find(suff, std::back_inserter(occ));

                  ASSERT_EQ(std::find(occ.begin(), occ.end(), suff_pos), occ.end()) << "Failed on sequence " << first
                                                                                    << " with suffix " << suff_pos
                                                                                    << ": the suffix shouldn't have "
                                                                                    << "been indexed (too small) "
                                                                                    << "yet it was reported as an "
                                                                                    << "occurrence";
              }

              // go to next reference sequence
              first = last + 1;
              last = text.find_first_of("$", last + 1);
          } // while last
      }
  } // SmallerThanLimitDoesNotOccur

} // namespace
