#!/bin/python3

import random

OUT_FILE="./0.fa"

COUNT=100
MIN_LEN=64
MAX_LEN=128

with open(OUT_FILE, 'w') as file:
    for i in range(COUNT):
        len=random.randint(MIN_LEN, MAX_LEN)
        file.write(">SEQ " + str(i) + "\n")
        file.write(''.join(random.choices(['A', 'C', 'G', 'T'], k=len)) + '\n')
