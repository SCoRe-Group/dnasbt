#!/bin/python3

import random

OUT_FILE="./reads.raw"

COUNT=100
MIN_LEN=16
MAX_LEN=64

with open(OUT_FILE, 'w') as file:
    for _ in range(COUNT):
        len=random.randint(MIN_LEN, MAX_LEN)
        file.write(''.join(random.choices(['A', 'C', 'G', 'T'], k=len)) + '\n')

