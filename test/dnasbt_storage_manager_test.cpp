#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <tuple>
#include <utility>

#include <gtest/gtest.h>

#include <dnasbt/dnasbt_config.hpp>
#include <dnasbt/dnasbt_storage_manager.hpp>

#include "aligned_block.hpp"
#include "test.hpp"


namespace {

  using namespace sbt::test;

  using block_type = aligned_block; // value type used for tests


  TEST(BasicStorageManager, SequentialAccess) {
      constexpr auto storage_size = 16ull;

      // fill storage s.t. storage[i] = 2 * i
      std::vector<block_type> storage{};
      for (auto i = 0ull; i < storage_size; ++i) {
          storage.push_back(make_aligned_block(2 * i));
      } // for i

      sbt::basic_storage_manager<block_type> manager{storage.data(), storage.size()};
      EXPECT_EQ(manager.size(), storage.size());

      for (auto i = 0ull; i < storage_size; ++i) {
          // pass in a null address
          auto [ptr, own] = manager.load(i, nullptr);
          ASSERT_NE(ptr, nullptr);
          EXPECT_EQ(*ptr, 2 * i);
          EXPECT_EQ(own, false);

          // pass in a non-null address
          block_type b;
          std::tie(ptr, own) = manager.load(i, &b);
          ASSERT_NE(ptr, nullptr);
          EXPECT_NE(ptr, &b); // this check is specific to basic_storage_manager
          EXPECT_EQ(*ptr, 2 * i);
          EXPECT_EQ(own, false);
      } // for i
  } // SequentialAccess


  TEST(BasicStorageManager, Move) {
      const std::vector<block_type> storage{make_aligned_block(1), make_aligned_block(2),
                                            make_aligned_block(4), make_aligned_block(8)};
      sbt::basic_storage_manager<block_type> manager_original{storage.data(), storage.size()};

      // check original storage contents
      for (auto i = 0ull; i < storage.size(); ++i) {
          // pass in null address
          EXPECT_EQ((manager_original.load(i, nullptr)), (std::pair{&storage[i], false}));

          // pass in non-null address
          block_type b;
          EXPECT_EQ((manager_original.load(i, &b)), (std::pair{&storage[i], false}));
      } // for i

      // use move ctor
      sbt::basic_storage_manager<block_type> manager_move_ctor{std::move(manager_original)};
      for (auto i = 0ull; i < storage.size(); ++i) {
          // pass in null address
          EXPECT_EQ((manager_move_ctor.load(i, nullptr)), (std::pair{&storage[i], false}));

          // pass in non-null address
          block_type b;
          EXPECT_EQ((manager_move_ctor.load(i, &b)), (std::pair{&storage[i], false}));
      } // for i

      // use move assignment
      sbt::basic_storage_manager<block_type> manager_move_assign;
      manager_move_assign = std::move(manager_move_ctor);

      for (auto i = 0ull; i < storage.size(); ++i) {
          // pass in null address
          EXPECT_EQ((manager_move_assign.load(i, nullptr)), (std::pair{&storage[i], false}));

          // pass in non-null address
          block_type b;
          EXPECT_EQ((manager_move_assign.load(i, &b)), (std::pair{&storage[i], false}));
      } // for i
  } // Move


  template <typename StorageManager>
  class FileBackedStorageManagerTest : public ::testing::Test {
  protected:
      using manager_type = StorageManager;

      static inline std::string tmp_file_name;
      static inline std::size_t file_size;
      static inline std::vector<block_type> buf;

      static void SetUpTestSuite() {
          tmp_file_name = TestEnv::test_tmp_dir + "/FileBackedkStorageManagerTest.Data."
                                                + std::to_string(typeid(manager_type).hash_code());
          file_size = 1024ull;

          buf = std::vector<block_type>(file_size);
          for (auto i = 0ull; i < file_size; ++i) {
              buf[i] = make_aligned_block(2 * i);
          } // for i

          // try creating tmp directory
          if (std::system((std::string{"mkdir -p "} + TestEnv::test_tmp_dir).c_str())) {
              constexpr auto setup_error = "Failed to create temporary directory";
              FAIL() << setup_error;
              return;
          }

          std::ofstream of{tmp_file_name};
          of.write(reinterpret_cast<const char*>(buf.data()), sizeof(block_type) * buf.size());
      } // SetUpTestSuite

  }; // class FileBackedStorageManagerTest


  using FileBackedStorageManagerTestTypes = ::testing::Types<sbt::mmap_storage_manager<block_type>,
                                                             sbt::direct_io_storage_manager<block_type>>;

  TYPED_TEST_SUITE(FileBackedStorageManagerTest, FileBackedStorageManagerTestTypes, );

  /// Allocate a block_type with the alignment required by the given mngr. The returned pointer must be released
  /// via free.
  template <typename StorageManager>
  block_type* alloc_block(const StorageManager& mngr) {
      // we increase the size of the allocation to be a multiple of the alignment
      auto size = sizeof(block_type) + (sizeof(block_type) % mngr.alignment);
      return static_cast<block_type*>(std::aligned_alloc(mngr.alignment, size));
  } // alloc_block


  TYPED_TEST(FileBackedStorageManagerTest, SequentialAccess) {
      typename TestFixture::manager_type mngr{TestFixture::tmp_file_name};

      ASSERT_EQ(mngr.size(), TestFixture::buf.size());
      for (auto i = 0ull; i < mngr.size(); ++i) {
          // pass in non-null address
          block_type* b = alloc_block(mngr);
          auto [ptr, own] = mngr.load(i, b);
          ASSERT_NE(ptr, nullptr);
          EXPECT_EQ(*ptr, TestFixture::buf[i]);

          if (own) {
              std::free(const_cast<block_type*>(ptr));
          }
          std::free(b);

          // pass in null address
          std::tie(ptr, own) = mngr.load(i, nullptr);
          ASSERT_NE(ptr, nullptr);
          EXPECT_EQ(*ptr, TestFixture::buf[i]);

          if (own) {
              std::free(const_cast<block_type*>(ptr));
          }
      } // for i
  } // SequentialAccess


  TYPED_TEST(FileBackedStorageManagerTest, MoveConstructorAndAssignment) {
      // test move constructor
      typename TestFixture::manager_type mngr_orig{TestFixture::tmp_file_name};
      typename TestFixture::manager_type mngr_mv_ctor{std::move(mngr_orig)};

      EXPECT_EQ(mngr_mv_ctor.size(), TestFixture::buf.size());
      for (auto i = 0ull; i < mngr_mv_ctor.size(); ++i) {
          // pass in non-null address
          block_type* b = alloc_block(mngr_mv_ctor);
          auto [ptr, own] = mngr_mv_ctor.load(i, b);
          ASSERT_NE(ptr, nullptr);
          EXPECT_EQ(*ptr, TestFixture::buf[i]);

          if (own) {
              std::free(const_cast<block_type*>(ptr));
          }

          // pass in null adddress
          std::tie(ptr, own) = mngr_mv_ctor.load(i, nullptr);
          ASSERT_NE(ptr, nullptr);
          EXPECT_EQ(*ptr, TestFixture::buf[i]);

          if (own) {
              std::free(const_cast<block_type*>(ptr));
          }

          std::free(b);
      } // for i

      // test move assignment operator
      typename TestFixture::manager_type mngr_mv_asg;
      mngr_mv_asg = std::move(mngr_mv_ctor);

      EXPECT_EQ(mngr_mv_asg.size(), TestFixture::buf.size());
      for (auto i = 0ull; i < mngr_mv_asg.size(); ++i) {
          // pass in non-null address
          block_type* b = alloc_block(mngr_mv_asg);
          auto [ptr, own] = mngr_mv_asg.load(i, b);
          ASSERT_NE(ptr, nullptr);
          EXPECT_EQ(*ptr, TestFixture::buf[i]);

          if (own) {
              std::free(const_cast<block_type*>(ptr));
          }

          // pass in null adddress
          std::tie(ptr, own) = mngr_mv_asg.load(i, nullptr);
          ASSERT_NE(ptr, nullptr);
          EXPECT_EQ(*ptr, TestFixture::buf[i]);

          if (own) {
              std::free(const_cast<block_type*>(ptr));
          }

          std::free(b);
      } // for i
  } // MoveConstructorAndAssignment


  TYPED_TEST(FileBackedStorageManagerTest, ConcurrentAccess) {
      // check that the test is actually multi-threaded
      int num_threads = 1;
      #pragma omp parallel shared(num_threads)
      #pragma omp single
      num_threads = omp_get_num_threads();
      ASSERT_GT(num_threads, 1) << "There are only " << num_threads <<  " available to the system";

      // use seperate buffers for the outputs for each thread
      std::vector<std::vector<block_type>> outputs_nonnull(num_threads);
      std::vector<std::vector<block_type>> outputs_null(num_threads);
      for (int i = 0; i < num_threads; ++i) {
          outputs_nonnull[i] = std::vector<block_type>(TestFixture::buf.size());
          outputs_null[i] = std::vector<block_type>(TestFixture::buf.size());
      } // for i

      // create manager
      typename TestFixture::manager_type mngr{TestFixture::tmp_file_name};
      ASSERT_EQ(mngr.size(), TestFixture::buf.size());

      // get outputs for each thread
      #pragma omp parallel for
      for (int i = 0; i < num_threads; ++i) {
          for (auto j = 0ul; j < mngr.size(); ++j) {
              // pass in non-null address
              block_type* b = alloc_block(mngr);
              auto [ptr, own] = mngr.load(j, b);
              if (ptr != nullptr) {
                  outputs_nonnull[i][j] = *ptr;
                  if (own) {
                      std::free(const_cast<block_type*>(ptr));
                  }
              } else {
                  // we throw an exception just to be safe as far a gtest and omp compatibility goes
                  std::free(b);
                  throw std::runtime_error("obtained a nullptr from the manager in the ConcurrentAccess test");
              }
              std::free(b);

              // pass in null address
              std::tie(ptr, own) = mngr.load(j, nullptr);
              if (ptr != nullptr) {
                  outputs_null[i][j] = *ptr;
                  if (own) {
                      std::free(const_cast<block_type*>(ptr));
                  }
              } else {
                  // we throw an exception just to be safe as far a gtest and omp compatibility goes
                  throw std::runtime_error("obtained a nullptr from the manager in the ConcurrentAccess test");
              }
          } // for j
      } // for i

      // validate outputs
      for (int i = 0; i < num_threads; ++i) {
          for (auto j = 0ull; j < TestFixture::buf.size(); ++j) {
              EXPECT_EQ(outputs_nonnull[i][j], TestFixture::buf[j]);
              EXPECT_EQ(outputs_null[i][j], TestFixture::buf[j]);
          } // for j
      } // for i
  } // ConcurrentAccess

} // namespace
