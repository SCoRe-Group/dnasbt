#include <gtest/gtest.h>

#include "environments.hpp"
#include "integration_environments.hpp"
#include "test.hpp"

namespace sbt::test {

  // add all global testing environments
  static auto test_env = ::testing::AddGlobalTestEnvironment(new TestEnv);
  /* static auto integration_test_env = ::testing::AddGlobalTestEnvironment(new IntegrationTestEnv); */

}
