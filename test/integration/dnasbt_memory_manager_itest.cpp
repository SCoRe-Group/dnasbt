#include <algorithm>
#include <cstdint>
#include <random>
#include <utility>

#include <gtest/gtest.h>

#include <dnasbt/dnasbt_config.hpp>
#include <dnasbt/dnasbt_memory_manager.hpp>
#include <dnasbt/dnasbt_storage_manager.hpp>

/* #include <dnasbt/experimental/dnasbt_experimental_memory_manager.hpp> */

#include "aligned_block.hpp"
#include "make_memory_manager.hpp"
#include "test.hpp"


namespace {

  using namespace sbt::test;


  using value_type = aligned_block;

  template <typename MemoryManager>
  class FileBackedMemoryManagerIntegrationTest : public ::testing::Test {
  protected:
      using memory_manager_type = MemoryManager;
      using storage_manager_type = typename memory_manager_type::storage_manager_type;

      static inline std::string tmp_file_name;
      static inline std::size_t file_size;
      static inline std::vector<value_type> data;

      static void SetUpTestSuite() {
          tmp_file_name = TestEnv::test_tmp_dir + "/FileBackedMemoryManagerTest.Data."
                                                + std::to_string(typeid(memory_manager_type).hash_code());
          file_size = 1024ull;

          data = std::vector<value_type>(file_size);
          for (auto i = 0ull; i < file_size; ++i) {
              data[i] = make_aligned_block(2 * i);
          } // for i

          // try creating tmp directory
          if (std::system((std::string{"mkdir -p "} + TestEnv::test_tmp_dir).c_str())) {
              constexpr auto setup_error = "Failed to create temporary directory";
              FAIL() << setup_error;
              return;
          }

          std::ofstream of{tmp_file_name};
          of.write(reinterpret_cast<const char*>(data.data()), sizeof(value_type) * data.size());
      } // SetUpTestSuite
  }; // class FileBackedMemoryManagerIntegrationTest


  using FileBackedMemoryManagerIntegrationTestTypes = ::testing::Types<
                                                          sbt::basic_memory_manager<sbt::mmap_storage_manager<value_type>>,
                                                          sbt::basic_memory_manager<sbt::direct_io_storage_manager<value_type>>,
                                                          sbt::alloc_memory_manager<sbt::mmap_storage_manager<value_type>>,
                                                          sbt::alloc_memory_manager<sbt::direct_io_storage_manager<value_type>> //,
                                                          /* sbt::experimental::experimental_memory_manager<sbt::mmap_storage_manager<value_type>> */
                                                          /* sbt::experimental::experimental_memory_manager<sbt::direct_io_storage_manager<value_type>> */
                                                      >;

  TYPED_TEST_SUITE(FileBackedMemoryManagerIntegrationTest, FileBackedMemoryManagerIntegrationTestTypes, );


  /// Checks (via getest asserts/expectations) that the contents of the given manager match that of the given data.
  template <typename MemoryManager>
  void validate_memory_manager(const MemoryManager& memory, const std::vector<value_type>& data) {
      ASSERT_EQ(memory.size(), data.size());
      for (auto i = 0ull; i < data.size(); ++i) {
          auto ptr = memory[i];
          EXPECT_EQ(*ptr, data[i]);
      } // for i
  } // validate_memory_manager


  TYPED_TEST(FileBackedMemoryManagerIntegrationTest, SequentialAccess) {
      typename TestFixture::storage_manager_type storage{TestFixture::tmp_file_name};
      auto memory = make_memory_manager<typename TestFixture::memory_manager_type>(std::move(storage));

      ASSERT_EQ(memory.size(), TestFixture::data.size());
      validate_memory_manager(memory, TestFixture::data);
  }

} // namespace
