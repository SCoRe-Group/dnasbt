#include <filesystem>
#include <optional>
#include <string>
#include <type_traits>

#include <gtest/gtest.h>

#include <dnasbt/dnasbt.hpp>
#include <dnasbt/dnasbt_config.hpp>
#include <dnasbt/dnasbt_memory_manager.hpp>
#include <dnasbt/dnasbt_storage_manager.hpp>
#include <dnasbt/dnasbt_filenames.hpp>

#include <environments.hpp>
#include <integration_environments.hpp>
#include <test.hpp>



namespace {

  using namespace sbt::test;

  static constexpr auto b = sbt::csbtree_suffixes_per_node<DNASBT_PAGE_SIZE>();

  /// An integration test for using the DNASBTree with compression enabled.
  template <typename T>
  class DNASBTreeIntegrationTest : public ::testing::Test {
  protected:
      static constexpr auto has_caching = std::tuple_element<0, T>::type::value;
      static constexpr auto file_name = std::tuple_element<1, T>::type::value;
      using csbtree_type = typename std::tuple_element<2, T>::type;

      static void load_index(csbtree_type& index) {
          if constexpr (has_caching) {
              ASSERT_TRUE(index.load({sbt::text_name(IntegrationTestEnv::db_loc)},
                                     {32, file_name(IntegrationTestEnv::db_loc)},
                                     {sbt::dsuf_name(IntegrationTestEnv::db_loc)}));
          } else {
              ASSERT_TRUE(index.load({sbt::text_name(IntegrationTestEnv::db_loc)},
                                     {file_name(IntegrationTestEnv::db_loc)},
                                     {sbt::dsuf_name(IntegrationTestEnv::db_loc)}));
          }
      } // load_index
  }; // class DNASBTreeIntegrationTest


  using mmap_type = sbt::CSBTree<b, sbt::mmap_storage_manager,
                                    sbt::mmap_storage_manager,
                                    sbt::mmap_storage_manager,
                                    sbt::basic_memory_manager,
                                    sbt::basic_memory_manager,
                                    sbt::basic_memory_manager>;

  using cache_mmap_type = sbt::CSBTree<b, sbt::mmap_storage_manager,
                                          sbt::mmap_storage_manager,
                                          sbt::mmap_storage_manager,
                                          sbt::basic_memory_manager,
                                          sbt::cache_memory_manager,
                                          sbt::basic_memory_manager>;

  using cache_direct_io_type = sbt::CSBTree<b, sbt::mmap_storage_manager,
                                               sbt::direct_io_storage_manager,
                                               sbt::mmap_storage_manager,
                                               sbt::basic_memory_manager,
                                               sbt::cache_memory_manager,
                                               sbt::basic_memory_manager>;

  using compression_cache_mmap_type = sbt::CSBTree<b, sbt::mmap_storage_manager,
                                                      sbt::lz4_mmap_storage_manager,
                                                      sbt::mmap_storage_manager,
                                                      sbt::basic_memory_manager,
                                                      sbt::cache_memory_manager,
                                                      sbt::basic_memory_manager>;

  using compression_cache_direct_io_type = sbt::CSBTree<b, sbt::mmap_storage_manager,
                                                           sbt::lz4_direct_io_storage_manager,
                                                           sbt::mmap_storage_manager,
                                                           sbt::basic_memory_manager,
                                                           sbt::cache_memory_manager,
                                                           sbt::basic_memory_manager>;

  template <bool B>
  struct has_caching {
      static constexpr bool value = B;
  }; // has_caching

  template <std::string F(const std::string&)>
  struct file_name {
      static constexpr auto value = F;
  }; // node_file_name

  using DNASBTreeIntegrationTestTypes = ::testing::Types<
      std::tuple<has_caching<false>, file_name<sbt::csbt_name>, mmap_type>,
      std::tuple<has_caching<true>, file_name<sbt::csbt_name>, cache_mmap_type>,
      std::tuple<has_caching<true>, file_name<sbt::csbt_name>, cache_direct_io_type>,
      std::tuple<has_caching<true>, file_name<sbt::pcsbt_name>, compression_cache_mmap_type>,
      std::tuple<has_caching<true>, file_name<sbt::pcsbt_name>, compression_cache_direct_io_type>>;

  TYPED_TEST_SUITE(DNASBTreeIntegrationTest, DNASBTreeIntegrationTestTypes);


  TYPED_TEST(DNASBTreeIntegrationTest, NodeManagerSize) {
      typename TestFixture::csbtree_type index;
      TestFixture::load_index(index);

      // get the number of nodes in the tree
      auto size = std::filesystem::file_size(sbt::csbt_name(IntegrationTestEnv::db_loc)) / DNASBT_PAGE_SIZE;

      EXPECT_EQ(index.csbt_manager().size(), size);
  } // NodeManagerSize


  TYPED_TEST(DNASBTreeIntegrationTest, ContainsAllWholeSequences) {
      typename TestFixture::csbtree_type index;
      TestFixture::load_index(index);

      // load text
      auto o_text = load_text(sbt::text_name(IntegrationTestEnv::db_loc));
      ASSERT_TRUE(o_text) << "Failed to load text";

      // search for every complete sequence from the reference string
      const std::string& text = *o_text;
      std::string::size_type first = 0, last = text.find_first_of("$", 0);
      while (last != std::string::npos) {
          ASSERT_LT(first, last) << "Bug in test: beginning of sequence came after end of sequence"
                                 << " (note that this failure could also occur if the reference contains $$)";
          std::string::size_type seq_len = last - first + 1;

          // search for the current sequence (omitting terminating $)
          std::vector<std::string::size_type> occ;
          auto lcp = index.find(std::string_view(text.c_str() + first, seq_len - 1), std::back_inserter(occ));

          ASSERT_EQ(lcp, seq_len - 1) << "Failed on sequence " << first;
          ASSERT_GT(occ.size(), 0) << "Failed on sequence " << first;
          ASSERT_NE(std::find(occ.begin(), occ.end(), first), occ.end()) << "Failed on sequence " << first;

          // go to next reference sequence
          first = last + 1;
          last = text.find_first_of("$", last + 1);
      } // while last
  } // ContainsAllWholeSequences


  template <typename Index>
  void contains_all_splits(Index& index, const std::string text, const std::string& seperators, int& word_count) {
      std::string::size_type first = 0, last = text.find_first_of(seperators, 0);
      while (last != std::string::npos) {
          word_count += 1;

          ASSERT_LT(first, last) << "Bug in test: beginning of sequence came after end of sequence"
                                 << " (note that this failure could also occur if the reference contains $$)";
          std::string::size_type seq_len = last - first + 1;

          // search for the current sequence (omitting terminating $)
          std::vector<std::string::size_type> occ;
          auto lcp = index.find(std::string_view(text.c_str() + first, seq_len - 1), std::back_inserter(occ));

          // lcp must match and output must contain the query sequence
          ASSERT_EQ(lcp, seq_len - 1) << "Failed on sequence " << first;
          ASSERT_GT(occ.size(), 0) << "Failed on sequence " << first;
          ASSERT_NE(std::find(occ.begin(), occ.end(), first), occ.end()) << "Failed on sequence " << first;

          // output must match all ground-truth occurrences
          auto exp = find_all_occ(std::string{text.c_str() + first, seq_len - 1}, text);
          std::sort(occ.begin(), occ.end());
          std::sort(exp.begin(), exp.end());
          ASSERT_EQ(occ, exp);

          // go to next reference sequence
          first = last + 1;
          last = text.find_first_of(seperators, last + 1);
      } // while last
  } // contains_all_splits


  TEST(DeclarationOfIndependence, ContainsAllWords) {
      constexpr auto b = 16;
      constexpr auto cache = 8;
      using index_type = sbt::CSBTree<b, sbt::mmap_storage_manager, sbt::direct_io_storage_manager, sbt::mmap_storage_manager,
                                         sbt::basic_memory_manager, sbt::cache_memory_manager, sbt::basic_memory_manager>;

      int num_thr = 1;

      #pragma omp parallel shared(num_thr)
      #pragma omp single
      num_thr = omp_get_num_threads();

      auto db_loc = TestEnv::test_tmp_dir + "/" + "declaration";

      ASSERT_NO_FATAL_FAILURE(sbt_ascii_preprocess(db_loc, "declaration"));
      ASSERT_NO_FATAL_FAILURE(sbt_construct<16>(db_loc, index_type::node_size() * num_thr));

      index_type index;
      index.load({sbt::text_name(db_loc)}, {cache, sbt::csbt_name(db_loc)}, {sbt::dsuf_name(db_loc)});

      auto o_text = load_text(sbt::text_name(db_loc));
      ASSERT_TRUE(o_text);

      auto text = std::move(*o_text);

      int word_count = 0;
      ASSERT_NO_FATAL_FAILURE(contains_all_splits(index, text, " $", word_count));

      EXPECT_EQ(word_count, 1330);
  } // DeclarationOfIndependence ContainsAllWords


} // namespace
