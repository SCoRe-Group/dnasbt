#include <gtest/gtest.h>

#include <dnasbt/detail/dnasbt_algorithms.hpp>
#include <dnasbt/dnasbt_memory_manager.hpp>
#include <dnasbt/dnasbt_storage_manager.hpp>

namespace {

  inline const sbt::text_block* string2text(std::string& s) {
      return reinterpret_cast<const sbt::text_block*>(s.c_str());
  } // string2text


  using basic_manager = sbt::basic_memory_manager<sbt::basic_storage_manager<sbt::text_block>>;

  /// Tests case where neither the pattern nor the other string are exhausted during comparison.
  TEST(PatternSuffixLcp, NeitherExhausted) {
      std::string s1_text = "ACGTAACCGG$";
      std::string s2_text = "ACGGTT$";

      std::string text = s1_text + s2_text;
      const sbt::text_block* T = string2text(text);

      basic_manager manager{{T, text.size()}};

      // offsets of strings in the manager
      auto s1 = 0;
      auto s2 = s1_text.size();

      std::string pattern = "ACGTACGT";

      // query suffixes
      auto [lcp_1, c_1] = sbt::detail::pattern_suffix_lcp(manager, pattern, s1, 0);
      auto [lcp_2, c_2] = sbt::detail::pattern_suffix_lcp(manager, pattern, s2, 0);

      // confirm that the patterns weren't exhausted
      EXPECT_LT(lcp_1, pattern.size());
      EXPECT_LT(lcp_1, s1_text.size());
      EXPECT_LT(lcp_2, pattern.size());
      EXPECT_LT(lcp_2, s2_text.size());

      // verify the results
      EXPECT_EQ(lcp_1, 5);
      EXPECT_EQ(c_1, 'A');
      EXPECT_EQ(lcp_2, 3);
      EXPECT_EQ(c_2, 'G');
  } // PatternSuffixLcp NeitherExhausted


  TEST(PatternSuffixLcp, PatternExhausted) {
      std::string pattern = "BAN";

      std::string text = "BANANA$";
      const sbt::text_block* T = string2text(text);

      basic_manager manager{{T, text.size()}};

      auto [lcp, c] = sbt::detail::pattern_suffix_lcp(manager, pattern, 0, 0);

      EXPECT_EQ(lcp, 3);
      EXPECT_EQ(c, 'A');
  } // PatternSuffixLcp PatternExhausted


  TEST(PatternSuffixLcp, SuffixExhausted) {
      std::string pattern = "ABRACADABRA";

      std::string text = "ABRA$";
      const sbt::text_block* T = string2text(text);

      basic_manager manager{{T, text.size()}};

      auto [lcp, c] = sbt::detail::pattern_suffix_lcp(manager, pattern, 0, 0);

      EXPECT_EQ(lcp, 4);
      EXPECT_EQ(c, '$');
  } // PatternSuffixLcp SuffixExhausted


  TEST(PatternSuffixLcp, WithApriori) {
      std::string text_1 = "ACGTAACCGG$";
      std::string text_2 = "ACGGTT$";
      std::string text = text_1 + text_2;

      const sbt::text_block* T = string2text(text);

      basic_manager manager{{T, text.size()}};

      std::string pattern = "ACGTACGT";

      // query suffixes denoted by first and second texts respectively
      auto [lcp_1, c_1] = sbt::detail::pattern_suffix_lcp(manager, pattern, 0, 2);
      auto [lcp_2, c_2] = sbt::detail::pattern_suffix_lcp(manager, pattern, text_1.size(), 2);

      EXPECT_EQ(lcp_1, 5);
      EXPECT_EQ(c_1, 'A');
      EXPECT_EQ(lcp_2, 3);
      EXPECT_EQ(c_2, 'G');
  } // PatternSuffixLcp WithApriori


  template <int Size>
  std::pair<sbt::dna_sbt_node<Size>, std::string> create_sbt_node_and_text(std::array<std::string, Size> strings) {
      sbt::dna_sbt_node<Size> node;
      std::string text = strings[0];

      std::sort(strings.begin(), strings.end());

      node.size = Size;

      node.A[0] = 0;
      node.LCP[0] = 0;
      node.C_L[0] = '\0';
      node.C_R[0] = '\0';

      for (int i = 1; i < Size; ++i) {
          sbt::length_type lcp = 0;
          while (lcp < strings[i - 1].size() && lcp < strings[i].size() && strings[i - 1][lcp] == strings[i][lcp]) {
              ++lcp;
          } // while lcp

          node.A[i] = node.A[i - 1] + strings[i - 1].size();
          node.LCP[i] = lcp;
          node.C_L[i] = strings[i - 1][lcp];
          node.C_R[i] = strings[i][lcp];

          text += strings[i];
      }

      auto tb_sz = sizeof(sbt::text_block);
      if (text.size() % tb_sz != 0) {
          auto pad = ((text.size() / tb_sz) + 1) * tb_sz - text.size();
          text.append(pad, '$');
      }

      return {node, text};
  } // create_sbt_node


  TEST(ParrayBlindSearch, Occurs) {
      auto [node, text] = create_sbt_node_and_text<4>(std::array<std::string, 4>{"AAB$", "ABC$", "BBA$", "CCC$"});
      const sbt::text_block* T = string2text(text);

      basic_manager manager{{T, text.size()}};

      std::string pattern = "ABC";

      auto j = sbt::detail::csbt_parray_internal_search::m_blind_search__(node, pattern);

      EXPECT_EQ(j, 1);
  } // ParrayBlindSearch Occurs


  TEST(ParrayBlindSearch, NotInA) {
      auto [node, text] = create_sbt_node_and_text<4>(std::array<std::string, 4>{"AAB$", "ABC$", "BBA$", "CCC$"});
      const sbt::text_block* T = string2text(text);

      basic_manager manager{{T, text.size()}};

      std::string pattern = "BDD";

      auto j = sbt::detail::csbt_parray_internal_search::m_blind_search__(node, pattern);

      EXPECT_EQ(j, 2);
  } // ParrayBlindSearch NotInA


  TEST(ParrayBlindSearch, PatternShorter) {
      auto [node, text] = create_sbt_node_and_text<4>(std::array<std::string, 4>{"AAB$", "ABC$", "BBA$", "CCC$"});
      const sbt::text_block* T = string2text(text);

      basic_manager manager{{T, text.size()}};

      std::string pattern = "CA";

      auto j = sbt::detail::csbt_parray_internal_search::m_blind_search__(node, pattern);

      EXPECT_EQ(j, 3);
  } // ParrayBlindSearch PatternShorter


  TEST(ParrayBlindSearch, PatternLonger) {
      auto [node, text] = create_sbt_node_and_text<4>(std::array<std::string, 4>{"AAB$", "ABC$", "BBA$", "CCC$"});
      const sbt::text_block* T = string2text(text);

      basic_manager manager{{T, text.size()}};

      std::string pattern = "BBAD";

      auto j = sbt::detail::csbt_parray_internal_search::m_blind_search__(node, pattern);

      EXPECT_EQ(j, 2);
  } // ParrayBlindSearch PatternLonger


  TEST(ParrayBlindSearch, IsPrefix) {
      auto [node, text] = create_sbt_node_and_text<4>(std::array<std::string, 4>{"AAB$", "ABC$", "BBA$", "CCC$"});
      const sbt::text_block* T = string2text(text);

      basic_manager manager{{T, text.size()}};

      std::string pattern = "AB";

      auto j = sbt::detail::csbt_parray_internal_search::m_blind_search__(node, pattern);

      EXPECT_EQ(j, 1);
  } // ParrayBlindSearch IsPrefix


  TEST(ParrayBlindSearch, MultipleCandidates) {
      auto [node, text] = create_sbt_node_and_text<4>(std::array<std::string, 4>{"AAB$", "ABC$", "BBA$", "CCC$"});
      const sbt::text_block* T = string2text(text);

      basic_manager manager{{T, text.size()}};

      std::string pattern = "AD";

      auto j = sbt::detail::csbt_parray_internal_search::m_blind_search__(node, pattern);

      EXPECT_GE(j, 0);
      EXPECT_LE(j, 1);
  } // ParrayBlindSearch MultipleCandidates


  TEST(ParraySearchPos, EmptyPattern) {
      auto [node, text] = create_sbt_node_and_text<4>(std::array<std::string, 4>{"AAB$", "ABC$", "BBA$", "CCC$"});
      const sbt::text_block* T = string2text(text);

      basic_manager manager{{T, text.size()}};

      std::string pattern = "";

      auto [j, l, has_max] = sbt::detail::csbt_parray_internal_search::search_pos(node, manager, pattern, 0);

      EXPECT_EQ(j, 0);
      EXPECT_EQ(l, 0);
      EXPECT_EQ(has_max, true);
  } // ParraySearchPos EmptyPattern


  TEST(ParraySearchPos, BeforeAll) {
      auto [node, text] = create_sbt_node_and_text<4>(std::array<std::string, 4>{"AAB$", "ABC$", "BBA$", "CCC$"});
      const sbt::text_block* T = string2text(text);

      basic_manager manager{{T, text.size()}};

      std::string pattern = "AAAC";

      auto [j, l, has_max] = sbt::detail::csbt_parray_internal_search::search_pos(node, manager, pattern, 0);

      EXPECT_EQ(j, 0);
      EXPECT_EQ(l, 2);
      EXPECT_EQ(has_max, true);
  } // ParraySearchPos BeforeAll


  TEST(ParraySearchPos, AfterAll) {
      auto [node, text] = create_sbt_node_and_text<4>(std::array<std::string, 4>{"AAB$", "ABC$", "BBA$", "CCC$"});
      const sbt::text_block* T = string2text(text);

      basic_manager manager{{T, text.size()}};

      std::string pattern = "F";

      auto [j, l, has_max] = sbt::detail::csbt_parray_internal_search::search_pos(node, manager, pattern, 0);

      EXPECT_EQ(j, 4);
      EXPECT_EQ(l, 0);
      EXPECT_EQ(has_max, false);
  } // ParraySearchPos AfterAll


  TEST(ParraySearchPos, BeforeLCP) {
      auto [node, text] = create_sbt_node_and_text<4>(std::array<std::string, 4>{"AAB$", "ABC$", "BBA$", "CCC$"});
      const sbt::text_block* T = string2text(text);

      basic_manager manager{{T, text.size()}};

      std::string pattern = "BA";

      auto [j, l, has_max] = sbt::detail::csbt_parray_internal_search::search_pos(node, manager, pattern, 0);

      EXPECT_EQ(j, 2);
      EXPECT_EQ(l, 1);
      EXPECT_EQ(has_max, true);
  } // ParraySearchPos AfterAll


  TEST(ParraySearchPos, AfterLCP) {
      auto [node, text] = create_sbt_node_and_text<4>(std::array<std::string, 4>{"AAB$", "ABC$", "BBA$", "CCC$"});
      const sbt::text_block* T = string2text(text);

      basic_manager manager{{T, text.size()}};

      std::string pattern = "ABD";

      auto [j, l, has_max] = sbt::detail::csbt_parray_internal_search::search_pos(node, manager, pattern, 0);

      EXPECT_EQ(j, 2);
      EXPECT_EQ(l, 2);
      EXPECT_EQ(has_max, false);
  } // ParraySearchPos AfterAll


  TEST(ParraySearchPos, BetweenLCP) {
      auto [node, text] = create_sbt_node_and_text<4>(std::array<std::string, 4>{"AAA$", "ACC$", "BBA$", "CCC$"});
      const sbt::text_block* T = string2text(text);

      basic_manager manager{{T, text.size()}};

      std::string pattern = "ABB";

      auto [j, l, has_max] = sbt::detail::csbt_parray_internal_search::search_pos(node, manager, pattern, 0);

      EXPECT_EQ(j, 1);
      EXPECT_EQ(l, 1);
      EXPECT_EQ(has_max, true);
  } // ParraySearchPos AfterAll


  TEST(ParraySearchPos, WithApriori) {
      auto [node, text] = create_sbt_node_and_text<4>(std::array<std::string, 4>{"BBBAAA$", "BBBAAB$", "BBBAABC$",
                                                                                 "BBD$"});
      const sbt::text_block* T = string2text(text);

      basic_manager manager{{T, text.size()}};

      std::string pattern = "BBBAAC";

      auto [j, l, has_max] = sbt::detail::csbt_parray_internal_search::search_pos(node, manager, pattern, 4);

      EXPECT_EQ(j, 3);
      EXPECT_EQ(l, 5);
      EXPECT_EQ(has_max, false);
  } // ParraySearchPos WithApriori


  TEST(ParraySearchOcc, All) {
      auto [node, text] = create_sbt_node_and_text<4>(std::array<std::string, 4>{"BAAA$", "BAACBD$", "BACCCD$", "BAD$"});
      const sbt::text_block* T = string2text(text);

      basic_manager manager{{T, text.size()}};

      std::string pattern = "BABBFF";

      auto [begin, end, l] = sbt::detail::csbt_parray_internal_search::search_occ(node, manager, pattern, 0);

      EXPECT_EQ(begin, 0);
      EXPECT_EQ(end, 4);
      EXPECT_EQ(l, 2);
  } // ParraySearch Occ All


  TEST(ParraySearchOcc, Single) {
      auto [node, text] = create_sbt_node_and_text<4>(std::array<std::string, 4>{"BAAA$", "BAACBD$", "BACCCD$", "BAD$"});
      const sbt::text_block* T = string2text(text);

      basic_manager manager{{T, text.size()}};

      std::string pattern = "BACAF";

      auto [begin, end, l] = sbt::detail::csbt_parray_internal_search::search_occ(node, manager, pattern, 0);

      EXPECT_EQ(begin, 2);
      EXPECT_EQ(end, 3);
      EXPECT_EQ(l, 3);
  } // ParraySearchOcc Single


  TEST(ParraySearchOcc, WithApriori) {
      auto [node, text] = create_sbt_node_and_text<4>(std::array<std::string, 4>{"BAAA$", "BAACBD$", "BACCCD$", "BAD$"});
      const sbt::text_block* T = string2text(text);

      basic_manager manager{{T, text.size()}};

      std::string pattern = "BACAF";

      auto [begin, end, l] = sbt::detail::csbt_parray_internal_search::search_occ(node, manager, pattern, 2);

      EXPECT_EQ(begin, 2);
      EXPECT_EQ(end, 3);
      EXPECT_EQ(l, 3);
  } // ParraySearchOcc WithApriori

} // namespace
