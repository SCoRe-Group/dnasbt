#ifndef DNASBT_TEST_SA_HPP
#define DNASBT_TEST_SA_HPP

#include <algorithm>
#include <cinttypes>
#include <cstddef>
#include <fstream>
#include <filesystem>
#include <iterator>
#include <string>
#include <vector>

#include <boost/iostreams/device/mapped_file.hpp>


namespace io = boost::iostreams;

namespace sbt::test::detail {

  /// Iterable wapper around pointers for templating and code-resuse.
  ///
  /// (This is not the most elegant solution, but its probably the quickest).
  template<typename T>
  class ptr_iterable {
  public:
      ptr_iterable() : ptr_ {nullptr}, len_ {0} {}

      ptr_iterable(const T* ptr, std::size_t len) : ptr_ {ptr}, len_ {len} {}

      const T* begin() const noexcept {
          return ptr_;
      }

      const T* end() const noexcept {
          return ptr_ + len_;
      }

      std::size_t size() const noexcept {
          return len_;
      }

      const T& operator[](std::size_t i) const noexcept {
          return ptr_[i];
      }

  private:
      const T* ptr_;
      std::size_t len_;
  };

  template <typename T>
  bool read_file(const std::string& name, std::vector<T>& buf) {
      auto sz = std::filesystem::file_size(name);
      buf.resize(sz / sizeof(T));

      std::ifstream f(name, std::ios_base::binary);
      f.read(reinterpret_cast<char*>(buf.data()), sz);

      return true;
  } // read_file


  /// Compares (less than) string at offset into text sa_val with pattern P
  template <typename Iterable = std::vector<char>, typename TType, typename StringType = std::string_view>
  bool sa_str_lt(std::uint64_t sa_val, const StringType& P, const TType& T) noexcept {
      auto p = P.length();

      // compare characters until identifying ordering or exhausting one of the strings
      std::size_t i {0};
      for (; T[sa_val + i] != '$' && i < p; ++i) {
          if (T[sa_val + i] < P[i]) {
              return true;
          } else if (T[sa_val + i] > P[i]) {
              return false;
          }
          // they are the same so far, keep iterating
      }

      // one of the strings has been exhausted; that with the smaller length comes first
      if (T[sa_val + i] == '$' && i == p) {
          // they both have the same length, so string at sa_val does not come before P
          return false;
      } else if (i == p) {
          // P was exhausted, so P comes first
          return false;
      } else {
          // string at sa_val was exhausted first, so this string comes first
          return true;
      }
  } // sa_str_lt

} // namespace sbt::test::detail


namespace sbt::test {

  class suffix_array {
  public:

    template <typename OutputIterator, typename StringType = std::string_view>
    int find(const StringType& P, OutputIterator out, std::size_t threshold = 0, bool report = true) noexcept {
        if (!mmap_) {
            return m_find_impl__(P, out, threshold, T_, SA_, LCP_, L_, report);
        } else {
            return m_find_impl__(P, out, threshold, T_ptr_, SA_ptr_, LCP_ptr_, L_ptr_, report);
        }
    } // find

    bool load(const std::string& text_file, const std::string& sa_file, const std::string& lcp_file,
              const std::string& len_file, bool mmap = false, int limit = 15) noexcept {
        mmap_ = mmap;
        limit_ = limit;
        try {
            if (!mmap) {
                return detail::read_file(text_file, T_)
                        && detail::read_file(sa_file, SA_)
                        && detail::read_file(lcp_file, LCP_)
                        && detail::read_file(len_file, L_);
            } else {
                T_mf_.open(text_file);
                SA_mf_.open(sa_file);
                LCP_mf_.open(lcp_file);
                L_mf_.open(len_file);

                T_ptr_ = {T_mf_.data(), T_mf_.size()};
                SA_ptr_ = {reinterpret_cast<const std::uint64_t*>(SA_mf_.data()), SA_mf_.size() / sizeof(std::uint64_t)};
                LCP_ptr_ = {reinterpret_cast<const std::uint64_t*>(LCP_mf_.data()), LCP_mf_.size() / sizeof(std::uint64_t)};
                L_ptr_ = {reinterpret_cast<const std::uint16_t*>(L_mf_.data()), L_mf_.size() / sizeof (std::uint16_t)};

                return true;
            }
        } catch (...) {
            return false;
        }
    } // load

  private:
    std::vector<char> T_;
    std::vector<std::uint64_t> SA_;
    std::vector<std::uint64_t> LCP_;
    std::vector<std::uint16_t> L_;

    io::mapped_file_source T_mf_;
    io::mapped_file_source SA_mf_;
    io::mapped_file_source LCP_mf_;
    io::mapped_file_source L_mf_;

    detail::ptr_iterable<char> T_ptr_;
    detail::ptr_iterable<std::uint64_t> SA_ptr_;
    detail::ptr_iterable<std::uint64_t> LCP_ptr_;
    detail::ptr_iterable<std::uint16_t> L_ptr_;

    bool mmap_;
    int limit_; // only report suffixes larger than this

    template <typename TType, typename StringType = std::string_view>
    std::size_t m_find_lcp__(const StringType& P, std::uint64_t sa_val, const TType& T) noexcept {
        auto p = P.length();

        std::size_t lcp = 0;
        while (lcp < p && T[sa_val + lcp] == P[lcp]) ++lcp;

        return lcp;
    } // m_find_lcp__

    /// Returns true if the current suffix would be omitted from the SBT during
    /// construction, this way both the SA and the SBT will give the same set of
    /// results
    template <typename TType, typename SaType, typename LcpType, typename LType>
    bool m_should_skip__(std::size_t i, const TType& T, const SaType& SA, const LcpType& LCP, const LType& L) noexcept {
        auto len = L[SA[i]];
        return T[SA[i]] == '$' // ignore empty strings
                || len <= limit_ // only report suffixes that are of a large enough length
                || (i > 0 && len == L[SA[i-1]] && len == LCP[i]); // skip duplicate suffixes
    } // m_should_skip__

    template <typename OutputIterator,
              typename TType,
              typename SaType,
              typename LcpType,
              typename LType,
              typename StringType = std::string_view>
    int m_find_impl__(const StringType& P, OutputIterator out, std::size_t threshold, const TType& T, const SaType& SA,
                      const LcpType& LCP, const LType& L, bool report = true) noexcept {
        // find lower bound value in SA s.t. P <= lower
        auto comp = [&T](std::uint64_t sa_val, const StringType& P) { return detail::sa_str_lt(sa_val, P, T); };
        auto lower = std::lower_bound(SA.begin(), SA.end(), P, comp);

        // compute index in SA corresponding to lower bound
        auto l = static_cast<std::size_t>(std::distance(SA.begin(), lower));

        // max lcp is the largest of the left and right LCPs.
        auto lcp_l = lower == SA.begin() ? 0 : m_find_lcp__(P, SA[l-1], T);
        auto lcp_r = lower == SA.end() ? 0 : m_find_lcp__(P, SA[l], T);
        auto lcp = std::max(lcp_l, lcp_r);

        // adjust l to be at the position of some string with the max lcp
        if (lcp == lcp_l && lcp != lcp_r) --l;

        if (lcp >= threshold && report) {
            if (!m_should_skip__(l, T, SA, LCP, L)) {
                *(out++) = SA[l];
            }

            // scan right
            auto i = l+1;
            while (i < LCP.size() && LCP[i] >= lcp) {
                if (!m_should_skip__(i, T, SA, LCP, L)) {
                    *(out++) = SA[i];
                }
                ++i;
            } // while i

            // scan left
            i = l;
            while (i > 0 && LCP[i] >= lcp) {
                if (!m_should_skip__(i-1, T, SA, LCP, L)) {
                    *(out++) = SA[i-1];
                }
                --i;
            } // while i
        }

        return lcp;
    } // m_find_impl__

  }; // class suffix_array

} // namespace dnasbt::test

#endif // DNASBT_TEST_SA_HPP
