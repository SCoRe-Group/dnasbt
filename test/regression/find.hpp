#ifndef DNASBT_TEST_FIND_HPP
#define DNASBT_TEST_FIND_HPP

#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>


namespace sbt::test {

  template <typename Index>
  void find(Index& index) {
      std::vector<typename sbt::memory_manager<void>::size_type> occ;
      std::string pattern;

      auto query_count = 0;

      while (std::cin >> pattern && std::cin && !std::cin.eof()) {
          auto length = index.find(pattern, std::back_inserter(occ));

          std::sort(occ.begin(), occ.end());

          ++query_count;
          std::cout << "[query " << query_count << "] " << "length:  " << length << std::endl;
          std::cout << "[query " << query_count << "] " << "matches: ";
          for (auto o : occ) {
              std::cout << o << ", ";
          }
          std::cout << std::endl;

          occ.clear();
      } // while cin
  } // find

} // namespace dnasbt::test


#endif // DNASBT_TEST_FIND_HPP
