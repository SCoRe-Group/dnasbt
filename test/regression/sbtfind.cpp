#include <iostream>

#include <dnasbt/dnasbt.hpp>
#include <dnasbt/dnasbt_filenames.hpp>

#include "find.hpp"

template <typename T>
using manager_type = sbt::profile_manager<sbt::direct_io_manager<T>>;

int main(int argc, char* argv[]) {

    if (argc != 2) {
        std::cout << "usage: " <<  argv[0] << " <db>" << std::endl;
        return 0;
    }

    sbt::DNASBTree<sbt::sbtree_suffixes_per_node<DNASBT_PAGE_SIZE>(), manager_type, manager_type, manager_type> index;
    if (!index.load({sbt::text_name(argv[1])}, {sbt::sbt_name(argv[1])}, {sbt::text_ds_name(argv[1])})) {
        std::cerr << argv[0] << "error: failed to load db from " << argv[1];
        return 1;
    }

    sbt::test::find(index);

    return 0;
} // main
