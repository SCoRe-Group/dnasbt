#include <iostream>
#include <string>

#include <dnasbt/dnasbt.hpp>
#include <dnasbt/dnasbt_filenames.hpp>

#include "find.hpp"
#include "sa.hpp"


int main(int argc, char* argv[]) {

    if (argc != 3) {
        std::cout << "usage: " <<  argv[0] << " <db> <limit>" << std::endl;
        return 0;
    }

    sbt::test::suffix_array index;
    if (!index.load(sbt::text_name(argv[1]), sbt::sa_name(argv[1]), sbt::lcp_name(argv[1]),
                    sbt::len_name(argv[1]), true, std::stoi(argv[2]))) {
        std::cerr << argv[0] << "error: failed to load db from " << argv[1];
        return 1;
    }

    sbt::test::find(index);

    return 0;
} // main
