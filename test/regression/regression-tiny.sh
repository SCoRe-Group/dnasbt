#!/bin/bash

THIS_DIR="$(realpath $(dirname $0))"
TOOLS_DIR="$THIS_DIR/../build/tools"
BIN_DIR="$THIS_DIR/../build/test"
DATA_DIR="$THIS_DIR/data"
TMP_DIR="$THIS_DIR/../tmp"

DB_DIR="$TMP_DIR/regression-tiny"
DB="$DB_DIR/regression-tiny"
LIMIT=15

REFERENCE="$DATA_DIR/tiny-random-reference/reference"
READS="$DATA_DIR/reads-random-short/reads.raw"

mkdir -p $DB_DIR

$TOOLS_DIR/dnasbt-text-prepare -i "$REFERENCE" -o $DB
$TOOLS_DIR/dnasbt-suff-extract -o $DB
$TOOLS_DIR/dnasbt-index-create -o $DB -l $LIMIT -m

DIFF="$(diff -u <($BIN_DIR/safind $DB $LIMIT < $READS) <($BIN_DIR/sbtfind $DB < $READS))"
if [[ $DIFF != "" ]]; then
    echo "FAILURE! diff:"
    echo "$DIFF"
    exit 1
fi

echo "pass!"
exit 0
