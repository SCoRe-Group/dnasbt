#!/bin/bash

# get defaults from CMake
DEV_BLOCK_SIZE=`grep DEV_BLOCK_SIZE CMakeLists.txt | awk {'print $2'}`
DNASBT_PAGE_SIZE=`grep DNASBT_PAGE_SIZE CMakeLists.txt | awk {'print $2'}`

usage() {
  echo "usage: $0 [-hvdpt] [-B SIZE] [-s SIZE] [-c SIZE] [-i DIR]"
  echo "options:"
  echo "  -h        display this help"
  echo "  -v        enable verbose mode"
  echo "  -d        build debug version"
  echo "  -p        build profiling version"
  echo "  -t        build tests (only for developers)"
  echo "  -L        use large (64-bit) node offsets"
  echo "  -I        install headers only"
  echo "  -j <JOBS> set number of make jobs to build with     (default: 8)"
  echo "  -B <SIZE> set device block size to SIZE bytes       (default: $DEV_BLOCK_SIZE)"
  echo "  -s <SIZE> set SBT page size to SIZE bytes           (default: $DNASBT_PAGE_SIZE)"
  echo "  -i <DIR>  install in DIR                            (default: ./release/)"
}

DIR=$(pwd)/release
CMAKE_CALL="../"
JOBS=8

while getopts ":hvtdpLIB:s:i:j:r:" arg; do
  case $arg in
    h)
      usage
      exit -1
      ;;
    v)
      VERBOSE="VERBOSE=1"
      ;;
    d)
      CMAKE_CALL="$CMAKE_CALL -DCMAKE_BUILD_TYPE=Debug"
      ;;
    p)
      CMAKE_CALL="$CMAKE_CALL -DCMAKE_BUILD_TYPE=Profile"
      ;;
    j)
      JOBS=$OPTARG
      ;;
    B)
      CMAKE_CALL="$CMAKE_CALL -DDNASBT_DEV_BLOCK_SIZE=$OPTARG"
      ;;
    s)
      CMAKE_CALL="$CMAKE_CALL -DDNASBT_PAGE_SIZE=$OPTARG"
      ;;
    L)
      CMAKE_CALL="$CMAKE_CALL -DDNASBT_USE_LARGE_NODE_OFFSETS=1"
      ;;
    I)
      CMAKE_CALL="$CMAKE_CALL -DENABLE_TOOLS=0"
      ;;
    t)
      TESTING="true"
      ;;
    i)
      if [[ $OPTARG = /* ]]; then
        DIR=$OPTARG
      else
        DIR=$(pwd)/$OPTARG
      fi
      INSTALL="true"
      ;;
    r)
      RPATH="$OPTARG"
      CMAKE_CALL="$CMAKE_CALL -DCMAKE_BUILD_RPATH=$RPATH -DCMAKE_INSTALL_RPATH=$RPATH"
      ;;
    ?)
      usage
      exit -1
      ;;
  esac
done

if ! [ $TESTING ]; then
  CMAKE_CALL="$CMAKE_CALL -DBUILD_TESTING=OFF"
fi

if [ -d build ]; then
  rm -rf build
fi

mkdir -p build/

if [ ! -d build ]; then
  echo "error: unable to create build folder"
  exit -1
fi

echo "Building DNAsbt..."
echo "CMake call: $CMAKE_CALL"

cd build/
cmake $CMAKE_CALL -DCMAKE_INSTALL_PREFIX=$DIR
make -j $JOBS install $VERBOSE
