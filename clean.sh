#!/bin/bash

rm -rf bin/
rm -rf build/
rm -rf release/
rm -rf include/divsufsort*
rm -rf lib/
rm -rf container/singularity/*.simg
rm -rf tmp/
