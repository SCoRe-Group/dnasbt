/***
 *  $Id$
 **
 *  File: dnasbt_config.hpp.in
 *  Author: Andrew Mikalsen <ajmikals@buffalo.edu>
 *
 *  Copyright (c) 2023 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#ifndef DNASBT_CONFIG_HPP
#define DNASBT_CONFIG_HPP

// assumed data transfer block for the device
// on which index will be deployed (most likely 4096B)
#cmakedefine DNASBT_DEV_BLOCK_SIZE @DNASBT_DEV_BLOCK_SIZE@

// page size to store SBT nodes
// too small or too large size will be detected at compile time
// it should be multiple of DNASBT_DEV_BLOCK_SIZE
#cmakedefine DNASBT_PAGE_SIZE @DNASBT_PAGE_SIZE@

// if set we use 64-bit integers for node offsets
// since we use prefix compression, this is rarely necessary even for massive
// databases
#cmakedefine01 DNASBT_USE_LARGE_NODE_OFFSETS

#cmakedefine DNASBT_BUILD_TESTING @DNASBT_BUILD_TESTING@

#cmakedefine DNASBT_VERSION "@PROJECT_VERSION@+@DNASBT_VERSION@"

#cmakedefine DNASBT_PROJECT_VERSION "@PROJECT_VERSION@"

#endif // DNASBT_CONFIG_HPP
