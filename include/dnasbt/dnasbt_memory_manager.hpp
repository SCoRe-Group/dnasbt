/***
 *  $Id$
 **
 *  File: dnasbt_memory_manager.hpp
 *  Author: Andrew Mikalsen <ajmikals@buffalo.edu>
 *          Jaroslaw Zola <jaroslaw.zola@hush.com>
 *
 *  Copyright (c) 2023 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#ifndef DNASBT_MEMORY_MANAGER_HPP
#define DNASBT_MEMORY_MANAGER_HPP

#include <atomic>
#include <cerrno>
#include <cstddef>
#include <cstdlib>
#include <cstring>
#include <map>
#include <memory>
#include <mutex>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include <boost/align/aligned_allocator.hpp>
#include <boost/heap/binomial_heap.hpp>
#include <boost/heap/policies.hpp>

#include <dnasbt/dnasbt_storage_manager.hpp>

#include "detail/free_deleter.hpp"

#include "detail/spin_mutex.hpp"

#ifdef DNASBT_BUILD_TESTING
#ifndef FRIEND_TEST
#define FRIEND_TEST(x, y) struct _dnasbt_friend_test_noop_ {}
#endif // FRIEND_TEST
#endif // DNASBT_BUILD_TESTING


namespace sbt {

  // for internal use ;-)
  namespace priv {

    struct cache_stats {
        std::atomic_uint_fast64_t num_refs_ = 0;
        std::atomic_uint_fast64_t num_hits_ = 0;
        std::atomic_uint_fast64_t num_miss_ = 0;
        std::atomic_uint_fast64_t num_evic_ = 0;
    }; // struct cache_stats

  } // namespace priv


  /// A simple memory manager that simply uses the allocations created by the given storage_manager, and serves as a
  /// concrete example of the memory_manager. In general, a memory_manager abstracts access to an array. See this
  /// class's documentation for the memory_Manager interface specification.
  ///
  /// \tparam StorageManager Type representing the storage containing the logical vector that the memory_manager manages.
  template <typename StorageManager>
  class basic_memory_manager {
  public:
      /// The type of the storage_manager used to load data from storage.
      using storage_manager_type = StorageManager;

      /// The type of managed values.
      using value_type = typename storage_manager_type::value_type;

      /// The type capable of holding the maximum possible number of managed values.
      using size_type = typename storage_manager_type::size_type;

      /// An implementation-specific class. In this case, it's used to managed
      /// data received from the storage_manager.
      class handle {
      public:
          ~handle() {
              if (owned_) {
                  // this const_cast is safe by the specification of storage_manager
                  std::free(const_cast<value_type*>(ptr_));
              }
          } // ~handle

          handle(const handle&) = delete;
          handle& operator=(const handle&) = delete;

          handle(handle&& other) noexcept : handle() {
              swap(*this, other);
          } // handle

          handle& operator=(handle&& other) noexcept {
              swap(*this, other);
              return *this;
          } // operator=

          [[nodiscard]] const value_type& operator*() const {
              return *ptr_;
          } // operator*

          [[nodiscard]] const value_type* operator->() const {
              return ptr_;
          } // operator->

      private:
          friend class basic_memory_manager;

          handle() noexcept : ptr_{nullptr}, owned_{false} {}
          handle(const value_type* ptr, bool owned) noexcept : ptr_{ptr}, owned_{owned} {}

          friend void swap(handle& lhs, handle& rhs) noexcept {
              std::swap(lhs.ptr_, rhs.ptr_);
              std::swap(lhs.owned_, rhs.owned_);
          } // swap

          const value_type* ptr_;
          bool owned_;
      }; // class handle

      /// Represents a constant pointer to a value_type that follows smart-pointer semantics
      using const_pointer = handle;

      /// A memory_manager has a default constructor that, in general, creates an uninitialized storage_manager.
      basic_memory_manager() = default;

      /// A memory_manager can't be copied.
      basic_memory_manager(const basic_memory_manager&) = delete;
      basic_memory_manager& operator=(const basic_memory_manager&) = delete;

      /// A memory_manager can be moved.
      basic_memory_manager(basic_memory_manager&&) = default;
      basic_memory_manager& operator=(basic_memory_manager&&) = default;

      /// Creates a basic_memory_manager. Every memory_manager has an implementation-specific constructor like this one
      /// that properly initializes the memory_manager, each of which takes a storage_manager_type&& as an argument.
      basic_memory_manager(storage_manager_type&& storage) : storage_{std::move(storage)} {}

      /// Returns a const_pointer to the value_type stored at the specified index.
      [[nodiscard]] const_pointer operator[](size_type index) const {
          auto [ptr, own] = storage_.load(index, nullptr);
          return handle{ptr, own};
      } // operator[]

      /// Returns the number of values in the managed storage.
      [[nodiscard]] size_type size() const { return storage_.size(); }

      /// Returns a reference to the storage manager.
      [[nodiscard]] const storage_manager_type& storage_manager() const { return storage_; }

  private:
      storage_manager_type storage_;
  }; // class basic_memory_manager


  /// A memory_manager that does nothing.
  template <typename StorageManager>
  class null_memory_manager {
  public:
      using storage_manager_type = StorageManager;
      using value_type = typename storage_manager_type::value_type;
      using size_type = typename storage_manager_type::size_type;

      class handle {
      public:
          ~handle() {
              if (owned_) {
                  // this const_cast is safe by the specification of storage_manager
                  std::free(const_cast<value_type*>(ptr_));
              }
          } // ~handle

          handle(const handle&) = delete;
          handle& operator=(const handle&) = delete;

          handle(handle&& other) noexcept : handle() {
              swap(*this, other);
          } // handle

          handle& operator=(handle&& other) noexcept {
              swap(*this, other);
              return *this;
          } // operator=

          [[nodiscard]] const value_type& operator*() const {
              return *ptr_;
          } // operator*

          [[nodiscard]] const value_type* operator->() const {
              return ptr_;
          } // operator->

      private:
          friend class null_memory_manager;

          handle() noexcept : ptr_{nullptr}, owned_{false} {}
          handle(const value_type* ptr, bool owned) noexcept : ptr_{ptr}, owned_{owned} {}

          friend void swap(handle& lhs, handle& rhs) noexcept {
              std::swap(lhs.ptr_, rhs.ptr_);
              std::swap(lhs.owned_, rhs.owned_);
          } // swap

          const value_type* ptr_;
          bool owned_;
      }; // class handle

      using const_pointer = handle;

      null_memory_manager() = default;

      null_memory_manager(const null_memory_manager&) = delete;
      null_memory_manager& operator=(const null_memory_manager&) = delete;

      null_memory_manager(null_memory_manager&&) = default;
      null_memory_manager& operator=(null_memory_manager&&) = default;

      null_memory_manager(storage_manager_type&& storage) : storage_{std::move(storage)} {}

      [[nodiscard]] const_pointer operator[](size_type index) const {
          auto [ptr, own] = storage_.load(0, nullptr);
          return handle{ptr, own};
      } // operator[]

      [[nodiscard]] size_type size() const { return storage_.size(); }

      [[nodiscard]] const storage_manager_type& storage_manager() const { return storage_; }

  private:
      storage_manager_type storage_;

  }; // class null_memory_manager


  /// A memory_manager that allocates the data received from the storage_manager on the heap.
  template <typename StorageManager>
  class alloc_memory_manager {
  public:
      using storage_manager_type = StorageManager;
      using value_type = typename storage_manager_type::value_type;
      using size_type = typename storage_manager_type::size_type;
      using const_pointer = std::unique_ptr<const value_type, detail::free_deleter<value_type>>;

      /// Creates an uninitialized alloc_memory_manager.
      alloc_memory_manager() = default;

      alloc_memory_manager(const alloc_memory_manager&) = delete;
      alloc_memory_manager& operator=(const alloc_memory_manager&) = delete;

      alloc_memory_manager(alloc_memory_manager&&) = default;
      alloc_memory_manager& operator=(alloc_memory_manager&&) = default;

      /// Creates a properly initialized alloc_memory_manager.
      alloc_memory_manager(storage_manager_type&& storage) : storage_{std::move(storage)} {}

      [[nodiscard]] const_pointer operator[](size_type index) const {
          auto alloc = static_cast<value_type*>(std::aligned_alloc(storage_.alignment, sizeof(value_type)));
          if (!alloc) {
              throw std::runtime_error(std::string{"memory allocation failed: "} + std::strerror(errno));
          }

          auto data = std::unique_ptr<value_type, detail::free_deleter<value_type>>{alloc,
                                                                                    detail::free_deleter<value_type>()};

          auto [ptr, own] = storage_.load(index, data.get());

          // handle the case that the storage manager didn't use our buffer
          if (ptr != data.get()) {
              *data = *ptr;
              if (own) {
                  std::free(const_cast<value_type*>(ptr));
              }
          }

          return data;
      } // operator[]

      [[nodiscard]] size_type size() const { return storage_.size(); }

      [[nodiscard]] const storage_manager_type& storage_manager() const { return storage_; }

  private:
      storage_manager_type storage_;

  }; // class alloc_memory_manager


  // A thread-safe memory manager with caching.
  template <typename StorageManager>
  class cache_memory_manager {
  private:
      // forward declarations
      struct metadata;
      class metadata_compare;

      using heap_type = boost::heap::binomial_heap<metadata, boost::heap::compare<metadata_compare>>;

      //std::unique_ptr<priv::cache_stats> cstats_;

  public:
      using storage_manager_type = StorageManager;
      using value_type = typename storage_manager_type::value_type;
      using size_type = typename storage_manager_type::size_type;

      using cache_size_type = std::size_t;

      class handle {
      public:
          using const_reference = const value_type&;
          using const_pointer = const value_type*;

          ~handle() {
              if (manager_) {
                  manager_->pq_lock_->lock();

                  m_remove_reference__(manager_->pq_, pq_handle_);
                  assert((*pq_handle_).ref_count >= 0);

                  manager_->pq_lock_->unlock();
              }
          } // ~handle

          handle(const handle&) = delete;
          handle& operator=(const handle&) = delete;

          handle(handle&& other) noexcept : handle() {
              swap(*this, other);
          } // handle

          handle& operator=(handle&& other) noexcept {
              swap(*this, other);
              return *this;
          } // handle

          [[nodiscard]] const_reference operator*() const {
              return manager_->data_[cache_index_];
          } // operator*

          [[nodiscard]] const_pointer operator->() const {
              return &manager_->data_[cache_index_];
          } // operator ->

      private:
          friend class cache_memory_manager;

          using pq_handle_type = typename heap_type::handle_type;

          handle() noexcept : manager_{nullptr}, cache_index_{0} {}

          handle(const cache_memory_manager<storage_manager_type>* manager, cache_size_type cache_index, pq_handle_type pq_handle)
              noexcept : manager_{manager}, cache_index_{cache_index}, pq_handle_{pq_handle} {}

          friend void swap(handle& lhs,handle& rhs) noexcept {
              std::swap(lhs.manager_, rhs.manager_);
              std::swap(lhs.cache_index_, rhs.cache_index_);
              std::swap(lhs.pq_handle_, rhs.pq_handle_);
          } // swap

          const cache_memory_manager<storage_manager_type>* manager_;
          size_type cache_index_;
          pq_handle_type pq_handle_;
      }; // class handle

      using const_pointer = handle;

      /// Creates an uninitialized buffer_manager
      cache_memory_manager() = default;

      cache_memory_manager(const cache_memory_manager&) = delete;
      cache_memory_manager& operator=(const cache_memory_manager&) = delete;

      cache_memory_manager(cache_memory_manager&&) = default;
      cache_memory_manager& operator=(cache_memory_manager&&) = default;

      /// Create a manager with the specified capacity in number of values.
      cache_memory_manager(cache_size_type capacity, storage_manager_type&& storage)
          : storage_{std::move(storage)}, pq_lock_{new lock_type}, dict_lock_{new lock_type},
            pq_{metadata_compare()}, data_locks_(capacity) {
          data_.reserve(capacity);
          //cstats_ = std::unique_ptr<priv::cache_stats>(new priv::cache_stats);
      } // cache_memory_manager

      // ~cache_memory_manager() {
      //     if (cstats_ != nullptr) {
      //         std::cerr << "=== CACHE REFS: " << cstats_->num_refs_ << std::endl;
      //         std::cerr << "=== CACHE HITS: " << cstats_->num_hits_ << std::endl;
      //         std::cerr << "=== CACHE MISS: " << cstats_->num_miss_ << std::endl;
      //         std::cerr << "=== CACHE_EVIC: " << cstats_->num_evic_ << std::endl;
      //     }
      // } // ~cache_memory_manager

      [[nodiscard]] const_pointer operator[](size_type index) const {
          dict_lock_->lock();

          //cstats_->num_refs_.fetch_add(1, std::memory_order_relaxed);

          // item in the cache
          if (auto iter = dict_.find(index); iter != dict_.end()) {
              // we copy the dict value to avoid race conditions w/ more fine-grained locking
              auto dict_val = iter->second;

              // swap pq_ and dict_ locks to ensure no other thread evicts index before the reference count is updated
              pq_lock_->lock();
              dict_lock_->unlock();

              m_add_access_and_reference__(pq_, dict_val.pq_handle);

              pq_lock_->unlock();

              // wait for the data item to be loaded into cache if it's still loading
              // (this is safe since we copied the dict value)
              data_locks_[dict_val.cache_index].lock();
              data_locks_[dict_val.cache_index].unlock();

              //cstats_->num_hits_.fetch_add(1, std::memory_order_relaxed);

              return handle{this, dict_val.cache_index, dict_val.pq_handle};
          }

          // item is not in the cache: we initialize these in each branch to indicate
          // 1. where to load the item
          // 2. the pq element corresponding to the inserted cache value's metadata
          cache_size_type cache_index;
          typename heap_type::handle_type pq_handle;

          //cstats_->num_miss_.fetch_add(1, std::memory_order_relaxed);

          if (data_.size() < data_.capacity()) {
              // unused space in cache -- no need to evict, just use the next open slot
              cache_index = data_.size();
              data_.push_back(value_type{});

              // we need to hold both locks simultaneously to ensure consistency
              pq_lock_->lock();

              pq_handle = pq_.push({1, 1, index});
              dict_[index] = {cache_index, pq_handle};

              pq_lock_->unlock();
          } else {
              //cstats_->num_evic_.fetch_add(1, std::memory_order_relaxed);

              // cache is full -- perform LFU eviction
              // we need to hold both locks simultaneously to ensure consistency
              pq_lock_->lock();

              auto& victim = pq_.top();

              if (victim.ref_count > 0) {
                  throw std::runtime_error("too many handles requested simultaneously (all cache entries in use)");
              }

              assert(pq_.size() == data_.size() && pq_.size() == data_.capacity());
              assert(dict_.contains(victim.key));

              // reconfigure the pq to reuse the victim as the new entry
              auto dict_node = dict_.extract(victim.key);
              auto& dict_val = dict_node.mapped();
              dict_.insert({index, {dict_val.cache_index, dict_val.pq_handle}});
              (*dict_val.pq_handle).ref_count = 1;
              (*dict_val.pq_handle).priority = 1;
              (*dict_val.pq_handle).key = index;
              pq_.update(dict_val.pq_handle);

              pq_handle = dict_val.pq_handle;

              pq_lock_->unlock();

              cache_index = dict_val.cache_index;
          }

          // indicate that the item is being loaded (there should be no contention here)
          data_locks_[cache_index].lock();

          dict_lock_->unlock();

          // load data into cache
          auto data_ptr = data_.data() + cache_index;
          auto [ptr, own] = storage_.load(index, data_ptr);

          if (ptr != data_ptr) {
              *data_ptr = *ptr;
              if (own) {
                  std::free(const_cast<value_type*>(ptr));
              }
          }

          // now the item has been fully loaded
          data_locks_[cache_index].unlock();

          return handle{this, cache_index, pq_handle};
      } // operator[]

      [[nodiscard]] size_type size() const { return storage_.size(); }

      [[nodiscard]] const storage_manager_type& storage_manager() const { return storage_; }


      [[nodiscard]] cache_size_type cache_size() const {
          dict_lock_->lock();
          auto size =  data_.size();
          dict_lock_->unlock();

          return size;
      } // cache_size

      [[nodiscard]] cache_size_type cache_capacity() const {
          dict_lock_->lock();
          auto cap = data_.capacity();
          dict_lock_->unlock();

          return cap;
      } // cache_capacity

  private:
      friend class handle;

      #ifdef DNASBT_BUILD_TESTING
      friend class CacheMemoryManagerTest;
      FRIEND_TEST(CacheMemoryManagerTest, UsesLfuPolicy);
      #endif // DNASBT_BUILD_TESTING

      using lock_type = sbt::detail::spin_mutex;

      // stored in the priority queue
      struct metadata {
          int ref_count;
          int priority;
          size_type key;
      }; // struct metadata

      class metadata_compare {
      public:
          /// Returns true iff lhs is a worse eviction candidate than rhs.
          bool operator()(const metadata& lhs, const metadata& rhs) const noexcept {
              // from the perspective of the heap interface, this computes whether
              // lhs < rhs. but since it's a max-heap and we want to treat it
              // like a min-heap, we determine if lhs > rhs
              return (lhs.ref_count > rhs.ref_count) ||
                     ((lhs.ref_count == rhs.ref_count) && (lhs.priority > rhs.priority));
          } // operator()
      }; // class metadata_compare

      // data stored in the dictionary
      struct dict_value {
          cache_size_type cache_index;
          typename heap_type::handle_type pq_handle;
      }; // struct dict_value

      using dict_type = std::unordered_map<size_type, dict_value>; // this will probably be changed to phmap

      static void m_add_access_and_reference__(heap_type& heap, typename heap_type::handle_type& handle) {
          (*handle).priority += 1;
          (*handle).ref_count += 1;
          heap.decrease(handle); // since we use a max-heap as a min-heap
      } // m_add_access_and_reference__

      static void m_remove_reference__(heap_type& heap, typename heap_type::handle_type& handle) {
          (*handle).ref_count -= 1;
          heap.increase(handle); // since we use a max-heap as a min-heap
      } // m_remove_reference__

      /* Locking Rules (Important):
       * To ensure progress, the order of lock acquisition MUST ALWAYS BE dict_lock_ -> pq_lock_ -> data_locks_[i].
       * When this invariant is upheld, any order of releasing locks should be viable.
       */

      mutable storage_manager_type storage_;

      // operations that modify the contents (i.e., value cached) must update data_, dict_lock_, and pq_lock_ atomically
      // such that they maintain a consistent view of the cache
      mutable std::unique_ptr<lock_type> pq_lock_;
      mutable std::unique_ptr<lock_type> dict_lock_;
      mutable heap_type pq_; // protected by pq_lock_
      mutable dict_type dict_; // protected by dict_lock_

      // pushing is protected by dict_lock_; reading/writing elements is protected by the corresponding data_locks_[i]
      mutable std::vector<value_type, boost::alignment::aligned_allocator<value_type, storage_manager_type::alignment>> data_;
      mutable std::vector<lock_type> data_locks_;

  }; // class cache_memory_manager


  template <typename StorageManager>
  class tree_memory_manager {
  public:
      using storage_manager_type = StorageManager;

      using value_type = typename storage_manager_type::value_type;

      using size_type = typename storage_manager_type::size_type;

      class handle {
      public:
          ~handle() {
              if (owned_) {
                  std::free(const_cast<value_type*>(ptr_));
              }
          } // ~handle

          handle(const handle&) = delete;
          handle& operator=(const handle&) = delete;

          handle(handle&& other) noexcept : handle() {
              swap(*this, other);
          } // handle

          handle& operator=(handle&& other) noexcept {
              swap(*this, other);
              return *this;
          } // operator=

          [[nodiscard]] const value_type& operator*() const {
              return *ptr_;
          } // operator*

          [[nodiscard]] const value_type* operator->() const {
              return ptr_;
          } // operator->

      private:
          friend class tree_memory_manager;

          handle() noexcept : ptr_{nullptr}, owned_{false} { }

          handle(const value_type* ptr, bool owned) noexcept : ptr_{ptr}, owned_{owned} { }

          friend void swap(handle& lhs, handle& rhs) noexcept {
              std::swap(lhs.ptr_, rhs.ptr_);
              std::swap(lhs.owned_, rhs.owned_);
          } // swap

          const value_type* ptr_;
          bool owned_;

      }; // class handle


      using const_pointer = handle;

      using cache_size_type = std::size_t;


      tree_memory_manager() = default;

      tree_memory_manager(const tree_memory_manager&) = delete;
      tree_memory_manager& operator=(const tree_memory_manager&) = delete;

      tree_memory_manager(tree_memory_manager&& mgr) {
          std::swap(storage_, mgr.storage_);
          std::swap(ssize_, mgr.ssize_);
          std::swap(capacity_, mgr.capacity_);
          std::swap(data_, mgr.data_);
      } // tree_manager

      tree_memory_manager& operator=(tree_memory_manager&& mgr) {
          storage_ = std::move(mgr.storage_);

          ssize_ = mgr.ssize_;
          capacity_ = mgr.capacity_;

          std::free(data_);
          data_ = nullptr;

          std::swap(data_, mgr.data_);

          return *this;
      } // operator=

      tree_memory_manager(cache_size_type capacity, storage_manager_type&& storage)
          : storage_{std::move(storage)} {

          ssize_ = storage_.size();
          capacity_ = std::min(ssize_, capacity);

          data_ = static_cast<value_type*>(std::aligned_alloc(storage_manager_type::alignment,
                                                              capacity_ * sizeof(value_type)));

          auto res = storage_.prefetch(ssize_ - capacity_, capacity_, data_);

          // TODO: check res for errors?
      } // tree_memory_manager

      ~tree_memory_manager() { std::free(data_); }


      [[nodiscard]] const_pointer operator[](size_type index) const {
          if (index >= ssize_ - capacity_) return handle{data_ + index - (ssize_ - capacity_), false};
          auto res = storage_.load(index, nullptr);
          return handle{res.first, res.second};
      } // operator[]

      [[nodiscard]] size_type size() const { return storage_.size(); }

      [[nodiscard]] const storage_manager_type& storage_manager() const { return storage_; }


  private:
      storage_manager_type storage_;

      size_type ssize_ = 0;
      cache_size_type capacity_ = 0;

      value_type* data_ = nullptr;

  }; // class tree_memory_manager

} // namespace sbt

#endif // DNASBT_MEMORY_MANAGER_HPP
