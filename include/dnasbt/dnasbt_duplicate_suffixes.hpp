/***
 *  $Id$
 **
 *  File: dnasbt_duplicate_suffixes.hpp
 *  Author: Andrew Mikalsen <ajmikals@buffalo.edu>
 *
 *  Copyright (c) 2023 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#ifndef DNASBT_DUPLICATE_SUFFIXES_HPP
#define DNASBT_DUPLICATE_SUFFIXES_HPP

#include <fstream>
#include <iterator>
#include <unordered_map>
#include <vector>

#include "dnasbt_types.hpp"


namespace sbt {

  class dnasbt_duplicate_suffixes {
  public:
      dnasbt_duplicate_suffixes() = default;

      bool load(const std::string& name) {
          std::ifstream s{name, std::ios::binary};

          // deserialize duplicate suffixes
          while (s) {
              offset_type count{};
              s.read(reinterpret_cast<char*>(&count), sizeof(count));
              --count; // we subtract 1 since we always take off the first (key) suffix position

              offset_type key{};
              s.read(reinterpret_cast<char*>(&key), sizeof(key));

              if (!s) break;

              for (; count > 0; --count) {
                  offset_type val{};
                  s.read(reinterpret_cast<char*>(&val), sizeof(val));
                  dups_.insert({key, val});
              }
          }

          s.close();

          return true;
      } // load

      /// Given a suffix position stored in the SBT, returns an iterator over
      /// all other equivalent positions.
      auto get_duplicates(offset_type pos) {
          return dups_.equal_range(pos);
      } // get_duplicates

      /// Extracts and returns the frequencies of each duplicate suffix.
      auto load_counts(const std::string& name) {
          std::ifstream s{name, std::ios::binary};

          std::vector<std::pair<offset_type, offset_type>> counts{};

          while (s) {
              offset_type count{};
              s.read(reinterpret_cast<char*>(&count), sizeof(count));
              --count; // we subtract 1 since we always take off the first (key) suffix position

              offset_type key{};
              s.read(reinterpret_cast<char*>(&key), sizeof(key));

              if (!s) break;

              counts.push_back({key, count});

              for (; count > 0; --count) {
                  offset_type val{};
                  s.read(reinterpret_cast<char*>(&val), sizeof(val));
              }
          }

          s.close();

          return counts;
      } // load_counts
  private:
      std::unordered_multimap<offset_type, offset_type> dups_;

  }; // class dnasbt_duplicate_suffixes

} // namespace sbt

#endif // DNASBT_DUPLICATE_SUFFIXES_HPP
