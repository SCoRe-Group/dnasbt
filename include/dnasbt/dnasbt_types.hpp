/***
 *  $Id$
 **
 *  File: dnasbt_types.hpp
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *          Andrew Mikalsen <ajmikals@buffalo.edu>
 *
 *  Copyright (c) 2023 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#ifndef DNASBT_TYPES_HPP
#define DNASBT_TYPES_HPP

#include <cinttypes>
#include <cstring>
#include <iostream>
#include <limits>

#include <dnasbt/detail/hash.hpp>
#include <dnasbt/detail/utils.hpp>
#include <dnasbt/dnasbt_config.hpp>


namespace sbt {

#if DNASBT_USE_LARGE_NODE_OFFSETS
  using sbt_index_type = int64_t; // indexing type to represent links between sbt nodes
#else
  using sbt_index_type = int32_t; // indexing type to represent links between sbt nodes
                                  // it is ok to be 32bit as nodes will be packaging many
                                  // sequences
#endif

  using length_type = uint16_t; // length of individual suffixes
                                // it is ok to be 16bit since we are dealing with
                                // generalized suffix array, and input sequences
                                // and hence suffixes will not be that long
                                // and even if we exceed this length, we may force
                                // suffixes to be shorter by breaking them

  using offset_type = int64_t; // must match with saidx_t (or saidx64_t)

  using suffix_index_type = int32_t; // indexing type to represent indicies of
                                     // each suffix stored in an SBT node
                                     // (i.e., representing range [0..b))

  // suffixes should not exceed this length
  inline constexpr auto SUFFIX_LEN_LIMIT = std::numeric_limits<length_type>::max() - 1;


  // A:         pointers to lexicographically sorted strings
  // LCP:       lcp between consecutive strings in A: LCP[i] = lcp(A[i-1], A[i])
  //            we define LCP[0] as the lcp between A[0] of current node and last A[i] of previous node on same level
  //            (or 0 if there is no such previous node)
  // C_L:       left mismatched character for the corresponding LCP value, undefined for C_L[0]
  // C_R:       right mismatched character for the corresponding LCP value, undefined for C_R[0]
  // child:     links to children (-1 means no child)
  //            if the node is a leaf, then instead these are the additive offsets (to dup_start) at which the
  //            corresponding duplicate list begins
  // size:      number of suffixes in the node (could be less than b)
  // dup_start: offset at which the lists of duplicate suffixes begin (for leaf nodes)
  // is_leaf:   true iff the node is a leaf
  template <int b>
  constexpr int dna_sbt_node_child_size() noexcept { return b; }

  // macro hacks (I know, yack) to "simplify" padding calculations
  #define dna_sbt_node_attr_                                    \
      offset_type A[b];                                         \
      length_type LCP[b];                                       \
      char C_L[b];                                              \
      char C_R[b];                                              \
      sbt_index_type child[dna_sbt_node_child_size<b>()];       \
      suffix_index_type size;                                   \
      offset_type dup_start;                                    \
      char is_leaf;                                             \

      // TODO: is_leaf would be better stored as metadata

  template <int b> struct dna_sbt_node_base_ {
      dna_sbt_node_attr_
  }; // dna_sbt_node_base_

  template <int b> struct dna_sbt_node {
      dna_sbt_node_attr_

      // padding to align with device block size
      char padding_[((sizeof(dna_sbt_node_base_<b>) + DNASBT_DEV_BLOCK_SIZE - 1) / DNASBT_DEV_BLOCK_SIZE) * DNASBT_DEV_BLOCK_SIZE - sizeof(dna_sbt_node_base_<b>)];
  }; // struct dna_sbt_node

  // initializes an INTERNAL sbt node
  template <int b>
  inline dna_sbt_node<b> make_sbt_node() noexcept {
      dna_sbt_node<b> u;
      memset(u.A, 0, sizeof(u.A));
      memset(u.LCP, 0, sizeof(u.LCP));
      memset(u.C_L, 0, sizeof(u.C_L));
      memset(u.C_R, 0, sizeof(u.C_R));
      for (int i = 0; i < dna_sbt_node_child_size<b>(); ++i) u.child[i] = -1;
      u.size = 0;
      u.dup_start = -1;
      u.is_leaf = 0;
      return u;
  } // make_sbt_node

  template <int b>
  inline auto check_sbt_node(const dna_sbt_node<b>& node) noexcept {
      offset_type S = 0;

      for (int i = 0; i < b; ++i) {
          S += (node.A[i] + node.LCP[i] + node.C_L[i] + node.C_R[i] + node.child[i]);
          S += node.size;
          S += node.is_leaf;
      }

      detail::fnv1a h;
      return std::make_pair(h(reinterpret_cast<const char*>(&node), sizeof(node)), S);
  } // check_sbt_node


  template <int b, typename T>
  inline void m_print_sbt_attr__(std::ostream& os, const T* attr, int first, int last) {
      for (int i = std::min(first, b); i < std::min(last, b); ++i) {
          os << " " << attr[i];
      }
  } // m_print_sbt_attr__

  template <int b>
  inline void print_sbt_node(std::ostream& os, const dna_sbt_node<b>& node,
                             int first, int last) noexcept {
      os << "A:";
      m_print_sbt_attr__<b>(os, node.A, first, last);
      os << std::endl;

      os << "LCP:";
      m_print_sbt_attr__<b>(os, node.LCP, first, last);
      os << std::endl;

      os << "size: " << node.size << std::endl;
  } // print_sbt_node

  template <int b>
  inline dna_sbt_node<b> make_sbt_leaf() noexcept {
      auto u = make_sbt_node<b>();
      u.is_leaf = 1;
      return u;
  } // make_sbt_leaf

  // returns true iff the given sbt node is a leaf node
  template <int b>
  inline bool is_sbt_leaf(const dna_sbt_node<b>& node) noexcept {
      return (node.is_leaf == 1);
  } // is_sbt_leaf


  struct text_block {
      char T[DNASBT_DEV_BLOCK_SIZE];
  }; // struct text_block

  inline std::pair<int, int> text_block_pos(uint64_t pos) {
      int bsz = sizeof(text_block);
      int bid = pos / bsz;
      int boff = pos % bsz;
      return { bid, boff };
  } // text_block_pos

} // namespace sbt

#endif // DNASBT_TYPES_HPP
