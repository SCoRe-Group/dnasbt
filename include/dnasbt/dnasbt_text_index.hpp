/***
 *  $Id$
 **
 *  File: dnasbt_text_index.hpp
 *  Author: Andrew Mikalsen <ajmikals@buffalo.edu>
 *
 *  Copyright (c) 2023 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#ifndef DNASBT_TEXT_INDEX_HPP
#define DNASBT_TEXT_INDEX_HPP

#include <algorithm>
#include <fstream>
#include <iterator>
#include <string>
#include <vector>

#include "dnasbt_types.hpp"


namespace sbt {

  /// Maps a position in the text to the start index of the reference string containing that position.
  class dnasbt_text_index {
  public:

      bool load(const std::string& name) {
          std::ifstream fs{name, std::ios::binary};

          if (!fs) return false;

          std::string buf{sizeof(offset_type), '\0'};
          while (fs.read(buf.data(), sizeof(offset_type))) {
              idx_.push_back(*reinterpret_cast<const offset_type*>(buf.c_str()));
          }

          fs.close();

          return true;
      } // load


      offset_type find(offset_type pos) {
          // find start position of string (the largest element in idx_ less than or equal to pos)
          auto lower = std::upper_bound(idx_.begin(), idx_.end(), pos);
          --lower;
          auto str_idx = static_cast<offset_type>(std::distance(idx_.begin(), lower));

          return str_idx;
      } // find


      auto begin() {
          return idx_.cbegin();
      } // begin


      auto end() {
          return idx_.cend();
      } // end


  private:
      std::vector<offset_type> idx_;

  }; // class dnasbt_text_index

} // namespace sbt

#endif // DNASBT_TEXT_INDEX_HPP
