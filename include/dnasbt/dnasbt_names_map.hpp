/***
 *  $Id$
 **
 *  File: dnasbt_names_map.hpp
 *  Author: Andrew Mikalsen <ajmikals@buffalo.edu>
 *
 *  Copyright (c) 2023 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#ifndef DNASBT_NAMES_MAP_HPP
#define DNASBT_NAMES_MAP_HPP

#include <algorithm>
#include <fstream>
#include <string>
#include <utility>
#include <vector>

#include "dnasbt_types.hpp"


namespace sbt {

  /// Enables finding the name of a reference string from a position in the text.
  class dnasbt_names_map {
  public:

      bool load(const std::string& name) {
          std::string line{};
          std::ifstream fs{name};

          if (!fs) return false;

          while (std::getline(fs, line)) {
              // have key as position in text and value as string name
              auto split = line.find(" ");
              map_.push_back({std::stoll(line.substr(0, split)),
                              line.substr(split + 1, line.size())});
          }

          fs.close();

          return true;
      } // load


      /// Given a position in the text, returns the name of the corresponding reference string.
      const std::string& find(offset_type pos) {
          // Do binary search for the name of the reference string ending at the given position.
          // Note that .dnamap stores the exclusive end position of the string (i.e., the start position of the next
          // string)
          auto elem = std::upper_bound(map_.begin(), map_.end(), pos, m_pos_key_lt__);

          // return the name of the string
          return std::get<1>(*elem);
      } // find


      auto begin() {
          return map_.cbegin();
      } // begin


      auto end() {
          return map_.cend();
      } // end

  private:
      using value_type = std::pair<offset_type, std::string>;

      std::vector<value_type> map_;

      // compare position to key-value pairs by their key
      static bool m_pos_key_lt__(const sbt::offset_type& pos, const value_type& kv) {
          return pos < std::get<0>(kv);
      } // m_pos_key_lt__

  }; // class dnasbt_names_map

} // namespace sbt

#endif // DNASBT_NAMES_MAP_HPP
