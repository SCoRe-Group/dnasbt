/***
 *  $Id$
 **
 *  File: dnasbt_experimental_memory_manager.hpp
 *  Author: YOUR NAME <YOUR EMAIL>
 *
 *  Copyright (c) 2023 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#ifndef DNASBT_EXPERIMENTAL_MEMORY_MANAGER_HPP
#define DNASBT_EXPERIMENTAL_MEMORY_MANAGER_HPP

#include <cstddef>
#include <vector>

#include <dnasbt/dnasbt_storage_manager.hpp>


namespace sbt::experimental {

  /// A thread-safe memory_manager with caching.
  template <typename StorageManager>
  class experimental_memory_manager {
  public:
      using storage_manager_type = StorageManager;
      using value_type = typename storage_manager_type::value_type;
      using size_type = typename storage_manager_type::size_type;

      class handle {
      public:
          handle() { /* TODO */ }
          ~handle() { /* TODO: release block */ }

          const value_type& operator*() { /* TODO: return reference to allocation */ }
      private:
          /* TODO */
      }; // class handle

      using const_pointer = handle;

      /// The type capable of holding the maximum possible number of cached values.
      using cache_size_type = std::size_t;

      /// Creates an uninitialized experimental_memory_manager.
      experimental_memory_manager() {}

      experimental_memory_manager(const experimental_memory_manager&) = delete;
      experimental_memory_manager& operator=(const experimental_memory_manager&) = delete;

      /* TODO: depending on the implementation, these may need a non-default implementation */
      experimental_memory_manager(experimental_memory_manager&&) = default;
      experimental_memory_manager& operator=(experimental_memory_manager&&) = default;

      /// Creates a new experimental_memory_manager with the given cache capacity, using the given disk_manager
      /// to interface with disk.
      experimental_memory_manager(cache_size_type capacity, storage_manager_type&& disk_manager) { /* TODO */ }

      const_pointer operator[](size_type index) const { /* TODO */ }

      size_type size() const { return storage_.size(); }

      const storage_manager_type& storage_manager() const { return storage_; }


      /* These methods aren't part of the disk manager interface so they're
       * not completely necessary, but they may be convenient to have. */

      /// Returns the number of blocks residing in the cache.
      cache_size_type cache_size() const { /* TODO */ }

      /// Returns the maximum number of blocks that can be stored in the cache.
      cache_size_type cache_capacity() const { /* TODO */ }

  private:
      storage_manager_type storage_; // used to load data from disk
      /* TODO */
  }; // class experimental_memory_manager

} // namespace sbt::experimental

#endif // DNASBT_EXPERIMENTAL_MEMORY_MANAGER_HPP
