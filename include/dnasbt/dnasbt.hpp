/***
 *  $Id$
 **
 *  File: dnasbt.hpp
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *          Andrew Mikalsen <ajmikals@buffalo.edu>
 *
 *  Copyright (c) 2023 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#ifndef DNASBT_HPP
#define DNASBT_HPP

#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdint>
#include <ctime>
#include <filesystem>
#include <limits>
#include <numeric>
#include <stdexcept>
#include <string>
#include <string_view>
#include <tuple>
#include <utility>
#include <vector>

#include <boost/iostreams/device/mapped_file.hpp>
#include <lz4.h>

#include <dnasbt/dnasbt_config.hpp>

#include "detail/dnasbt_algorithms.hpp"
#include "detail/omp_impl.hpp"
#include "detail/hash.hpp"
#include "detail/utils.hpp"

#include "dnasbt_filenames.hpp"
#include "dnasbt_memory_manager.hpp"
#include "dnasbt_storage_manager.hpp"
#include "dnasbt_helpers.hpp"
#include "dnasbt_types.hpp"


namespace io = boost::iostreams;

namespace sbt {


  template <int b,
            template<typename> typename TextStorageManager = basic_storage_manager,
            template<typename> typename CsbtStorageManager = basic_storage_manager,
            template<typename> typename DupStorageManager = basic_storage_manager,
            template<typename> typename TextMemoryManager = basic_memory_manager,
            template<typename> typename CsbtMemoryManager = basic_memory_manager,
            template<typename> typename DupMemoryManager = basic_memory_manager>
  class CSBTree;


  namespace detail {
    inline length_type pair_lcp(const char* T, const length_type*, // L
                                offset_type pos1, offset_type pos2) {
        length_type l = std::min(pos1, pos2);
        length_type i = 0;
        for (; i < l; ++i) { if (T[pos1 + i] != T[pos2 + i]) break; }
        return i;
    } // pair_lcp

    inline length_type suf_pair_lcp(const char* T, offset_type suf1, offset_type suf2) {
        length_type lcp = 0;
        while ((T[suf1 + lcp] != '$') && (T[suf2 + lcp] != '$') && (T[suf1 + lcp] == T[suf2 + lcp])) {
            ++lcp;
        } // while T
        return lcp;
    } // suf_pair_lcp

    // Returns the major, minor, and patch versions given a string of the form
    // "MAJOR.MINOR.PATCH".
    inline auto parse_version(const std::string version) {
        auto begin = 0;
        auto end = version.find(".", 0);
        std::string major = version.substr(begin, end - begin);

        begin = end + 1;
        end = version.find(".", begin);
        std::string minor = version.substr(begin, end - begin);

        begin = end + 1;
        end = version.size();
        std::string patch = version.substr(begin, end - begin);

        return std::make_tuple(major, minor, patch);
    } // parse_version


    struct csbt_sidecar {
        int node_size;
        int b;
        int limit;
        long long int cache;
        int large_offsets;
        int layer;
        std::uint64_t hash;

        bool write(const std::string& name) {
            std::ofstream of(name);
            if (!of) return false;

            auto time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
            of << "Date:\t" << std::ctime(&time); // ctime adds a newline

            auto [major, minor, patch] = parse_version(DNASBT_PROJECT_VERSION);

            of << "Major:\t" << major << std::endl;
            of << "Minor:\t" << minor << std::endl;
            of << "Patch:\t" << patch << std::endl;
            of << "Commit:\t" << DNASBT_VERSION << std::endl;
            of << "Block:\t" << node_size << std::endl;
            of << "Width:\t" << b << std::endl;
            of << "BHash:\t" << hash << std::endl;
            of << "Limit:\t" << limit << std::endl;
            of << "Cache:\t" << cache << std::endl;
            of << "Large:\t" << large_offsets << std::endl;
            of << "Layer:\t" << layer << std::endl;

            return true;
        } // write

    }; // struct csbt_sidecar

  } // namespace detail

  // csbt_name: where to store csbt
  // dup_name: where to store duplicate prefixes
  // sidecar_name: where to store sidecar info
  // T: text to index
  // L: length of suffixes
  // SA: suffix array
  // LCP: LCP array
  // l: size of SA and LCP
  // lim: min length of suffix to index
  // cache: cache size to use in bytes
  // log: logger
  template <int b, typename Logger>
  inline int create_csbtree(const std::string& csbt_name, const std::string& dup_name,
                            const std::string& sidecar_name, const char* T, const length_type* L,
                            const offset_type* SA, const offset_type* LCP, offset_type l,
                            length_type lim, long long int cache, Logger log) {
      log->debug("DNAsbt::create_csbtree ver. {}", DNASBT_VERSION);

      if (lim < 1) {
        log->error("limit parameter ({}) must be at least 1...", lim);
        return -1;
      }

      std::fstream of{csbt_name, of.binary | of.trunc | of.in | of.out};

      if (!of) {
          log->error("unable to create {} file!", csbt_name);
          return -1;
      }

      detail::csbt_sidecar sidecar;

      if (DNASBT_USE_LARGE_NODE_OFFSETS) {
        log->warn("using large node offsets, use only if you know what you are doing...");
      }

      sidecar.node_size = CSBTree<b>::node_size();
      sidecar.b = b;
      sidecar.limit = lim;
      sidecar.cache = cache;
      sidecar.large_offsets = DNASBT_USE_LARGE_NODE_OFFSETS;

      // assess number of threads
      int num_thr = 1;

      #pragma omp parallel shared(num_thr)
      #pragma omp single
      num_thr = omp_get_num_threads();

      auto lbcount = std::ceil(std::ceil((1.0 * l) / b) / b);

      if (lbcount < num_thr) {
          log->debug("number of threads changed from default {} to {}", num_thr, lbcount);
          num_thr = lbcount;
      }


      // 1. we check the number of suffixes to pack into the bottom of the tree
      log->info("checking input suffixes...");

      offset_type suff_count = 0;

      #pragma omp parallel for num_threads(num_thr)                     \
          default(none) shared(L, SA, LCP, l, lim) reduction(+:suff_count)
      for (offset_type pos = 0; pos < l; ++pos) {
          auto sa_pos = SA[pos];
          auto lcp_pos = LCP[pos];

          length_type len = L[sa_pos];

          // we care only about suffixes longer than the threshold
          if ((lim <= len) && (lcp_pos < len)) suff_count++;
      } // for pos

      log->info("found {} valid suffixes of {}", suff_count, l);

      if (suff_count < 1) {
          log->error("no valid suffixes found! terminating index creation...");
          return -1;
      }

      if (suff_count < b) {
          log->warn("all suffixes fit into a single node, are you sure it makes sense?");
      }

      // 2. now we find partitions

      // we set number of threads/partitions such that each
      // thread works on a fixed number of nodes in the next layer
      offset_type curr_bcount = std::ceil((1.0 * suff_count) / b);
      offset_type next_bcount = std::ceil((1.0 * curr_bcount) / b);

      log->info("expected number of leaves: {}", curr_bcount);

      // block part is the number of blocks in the layer 0 (i.e., one layer above leaves)
      // that each thread should produces
      offset_type block_part = std::ceil((1.0 * next_bcount) / num_thr);
      offset_type suff_part = block_part * b * b;

      log->debug("using {} threads", num_thr);
      log->debug("blocks in layer 0 per thread: {}", block_part);
      log->debug("suffixes in partition: {} ({} leaves)", suff_part, suff_part / b);

      log->info("partitioning {} valid suffixes...", suff_count);

      // if no garbage this would be ideal partitions
      std::vector<offset_type> part_limits(num_thr + 1, 0);
      int loc = 0;

      // if we are off in calculations, we may need more partitions than num_thr
      // it is ok if the last partition is larger
      for (offset_type i = 0; ((i < l) && (loc < num_thr)); i += suff_part) {
          part_limits[loc++] = i;
      }

      part_limits.resize(loc + 1);
      part_limits[loc] = l;

      if (loc < num_thr) {
          log->debug("number of threads changed from {} to {}", num_thr, loc);
          num_thr = loc;
      }

      // now we must check how much partitions must be changed because of garbage
      std::vector<offset_type> part_offset(num_thr, 0);

      #pragma omp parallel num_threads(num_thr)                         \
          default(none) shared(L, SA, LCP, l, lim, part_limits, part_offset)
      {
          int tid = omp_get_thread_num();

          offset_type pos_first = part_limits[tid];
          offset_type pos_last = part_limits[tid + 1];

          offset_type poff = 0;

          for (offset_type pos = pos_first; pos < pos_last; ++pos) {
              auto sa_pos = SA[pos];
              auto lcp_pos = LCP[pos];

              length_type len = L[sa_pos];

              if (!((lim <= len) && (lcp_pos < len))) poff++;
          } // for pos

          part_offset[tid] = poff;
      }

      // report initial partitions
      for (int i = 0; i < num_thr; ++i) {
          auto psz = (part_limits[i + 1] - part_limits[i]);
          log->debug("initial partition {}: [{}, {}), size: {}", i, part_limits[i], part_limits[i + 1], psz);
      }

      log->debug("adjusting partitions...");

      // and now we can update each partition
      // last thread does not need adjustment
      for (int i = 1; i < num_thr; ++i) {
          // we set pos_last to account for cases where
          // ideal partition has too much garbage
          offset_type pos_first = part_limits[i];
          offset_type pos_last = part_limits[num_thr];

          offset_type off = 0;

          for (offset_type pos = pos_first; pos < pos_last; ++pos) {
              auto sa_pos = SA[pos];
              auto lcp_pos = LCP[pos];

              length_type len = L[sa_pos];

              if (pos == part_limits[i + 1]) {
                  log->debug("partition {} expanding into next partition...", i);
              }

              if ((lim <= len) && (lcp_pos < len)) {
                  off++;

                  // the last suffix in a partition is guaranteed to correct :-)
                  if (off == part_offset[i - 1]) {
                      log->debug("moving end of partition {} from {} to {}", i - 1, part_limits[i], pos + 1);

                      part_limits[i] = pos + 1;
                      part_offset[i] += off;

                      break;
                  } // if off
              }
          } // for pos

          if (off < part_offset[i - 1]) {
              log->debug("partitions exhausted, reducing number of threads to {}", i);
              part_limits.resize(i + 1);
              part_limits[i] = l;
              num_thr = i;
              break;
          }
      } // for i

      if (part_limits[num_thr - 1] == l) {
          // need to resize
          --num_thr;
          log->debug("last partition empty, reducing number of threads to {}", num_thr);
          part_limits.resize(num_thr);
      }

      // report partitions
      for (int i = 0; i < num_thr; ++i) {
          auto psz = (part_limits[i + 1] - part_limits[i]);
          log->debug("partition {}: [{}, {}), size: {}", i, part_limits[i], part_limits[i + 1], psz);
      }


      // 3. compute the number of duplicate suffix entries that will be processed by
      // each thread and the starting position of each thread's duplicates
      // in the duplicate suffixes file
      log->debug("determining distribution of duplicate suffixes...");

      // index of the ith thread's first duplicate suffix in the duplicate suffix file
      std::vector<offset_type> dup_size(num_thr);
      #pragma omp parallel num_threads(num_thr) \
          default(none) shared(L, SA, LCP, l, lim, part_limits, dup_size)
      {
          int tid = omp_get_thread_num();

          offset_type pos_first = part_limits[tid];
          offset_type pos_last = part_limits[tid + 1];

          offset_type size_all = 0; // total number of entries
          offset_type size_cur = 0; // number of duplicate suffixes for the current valid suffix

          // simulate the process of detecting duplicate suffixes that we follow in construction
          for (offset_type pos = pos_first; pos < pos_last; ++pos) {
              auto sa_pos = SA[pos];
              auto lcp_pos = LCP[pos];

              length_type len = L[sa_pos];

              if (lim <= len) {
                  if (lcp_pos < len) {
                      // this is a new valid suffix
                      if (size_cur > 0) {
                          // count duplicates for last valid suffix
                          size_all += 1 + size_cur;
                          size_cur = 0;
                      }
                      assert(size_cur == 0);
                  } else {
                      // this is a new duplicate suffix
                      ++size_cur;
                  }
              }
          } // for pos

          // count duplicates for the last suffix
          size_all += (size_cur != 0) + size_cur;

          dup_size[tid] = size_all;
      } // omp

      // compute the index of the first duplicate suffix entry associated with
      // each thread
      std::vector<offset_type> dup_start(num_thr, 0);
      std::partial_sum(dup_size.begin(), dup_size.end() - 1, dup_start.begin() + 1);

      // report duplicate suffix file partitions
      for (int i = 0; i < num_thr; ++i) {
          log->debug("dup file partition {}: [{}, {}), size: {}", i, dup_start[i], dup_start[i] + dup_size[i], dup_size[i]);
      }


      // 4. start processing leaves
      // prepare cache
      long long int PACK_SIZE = cache / (sizeof(dna_sbt_node<b>) * num_thr);
      if (PACK_SIZE < 4) {
          log->warn("too small cache for this data, increasing it!");
          PACK_SIZE = 4;
      }
      std::size_t csz = PACK_SIZE * sizeof(dna_sbt_node<b>) * num_thr;


      log->info("allocating {} of cache for tree nodes...", detail::format_size(csz));

      std::vector<dna_sbt_node<b>> nodes(num_thr * PACK_SIZE);

      log->info("total cache size: {} tree nodes", nodes.size());
      log->info("creating csbtree leaves...");

      // suffixes to store in next layer
      struct node_info {
          offset_type sa_pos;
          sbt_index_type node_id;
      }; // struct node_info

      // we allocate more than we need (since last of next_bcount nodes may not be full)
      // we shrink next later
      std::vector<node_info> next(next_bcount * b);

      // here we store nodes that require correction after main processing loop
      std::vector<std::tuple<int, sbt_index_type, sbt_index_type>> fix_nodes;

      // global list of duplicate suffixes for each thread
      std::vector<std::vector<offset_type>> g_dup_all(num_thr);

      // total written nodes
      sbt_index_type g_wrt_nodes = 0;

      // total created nodes
      sbt_index_type g_num_nodes = 0;

      int plrd_last = -1;
      bool of_err = false;

      // we first pack nodes, taking care of duplicate suffixes
      #pragma omp parallel num_threads(num_thr) default(none)           \
          shared(log, T, L, SA, LCP, lim, part_limits,                  \
          curr_bcount, next_bcount, block_part, suff_part,              \
          nodes, next, fix_nodes, g_dup_all, g_wrt_nodes, g_num_nodes,  \
                 num_thr, PACK_SIZE, of, of_err, dup_start, plrd_last)
      {
          int tid = omp_get_thread_num();

          // number of nodes handled by the preceding threads
          sbt_index_type num_nodes_base = tid * block_part * b;

          sbt_index_type num_nodes = 0; // nodes produced by thread
          sbt_index_type wrt_nodes = 0; // nodes written by thread

          offset_type pos_first = part_limits[tid];
          offset_type pos_last = part_limits[tid + 1];

          sbt_index_type next_beg = tid * block_part * b;
          sbt_index_type next_pos = next_beg;

          // duplicates description for the current suffix
          std::vector<offset_type> dup_cur;

          // all duplicate entries produced by this thread
          std::vector<offset_type> dup_all;

          // index in dup_all of the first entry associated with the current node
          // that is, the starting point LOCAL TO THE THREAD, NOT THE GLOBAL
          // starting point. for the global starting point, you need to add
          // dup_start[tid]
          offset_type node_dup_start = 0;

          sbt_index_type node_size = 0;

          offset_type node_pos_base = tid * PACK_SIZE; // starting position in cache
          offset_type node_pos = 0; // position in cache to store node

          nodes[node_pos_base + node_pos] = make_sbt_leaf<b>();

          // set the dup_start of the node to the global starting point of the node's duplicate lists
          nodes[node_pos_base + node_pos].dup_start = node_dup_start + dup_start[tid];

          for (offset_type pos = pos_first; pos < pos_last; ++pos) {
              auto sa_pos = SA[pos];
              auto lcp_pos = LCP[pos];

              length_type len = L[sa_pos];

              // we care only about suffixes longer than the threshold
              if (lim <= len) {
                  // the condition holds true only if two consecutive suffixes are different
                  if (lcp_pos < len) {
                      // store duplicates (if any)
                      // (these will be associated with the previous entry)
                      if (!dup_cur.empty()) {
                          // we store the offset from the first duplicate entry associated with the node
                          // note that this is a NODE LOCAL offset (which is what we want here)
                          auto dup_offset = dup_all.size() - node_dup_start;

                          // child pointers in leaves corresponding to a suffix
                          // point to all duplicate suffixes if any exist
                          if (nodes[node_pos_base + node_pos].size == 0) {
                              if (node_pos == 0) {
                                  // here we want to modify nodes[node_pos_base + node_pos - 1]
                                  // but because it has been flushed we will do it later
                                  #pragma omp critical
                                  {
                                      log->debug("thread {} accessing node from flushed buffer", tid);
                                      fix_nodes.push_back({tid, num_nodes_base + num_nodes - 1, dup_offset});
                                  } // omp
                              } else {
                                  auto nd = node_pos_base + node_pos;
                                  nodes[nd - 1].child[nodes[nd - 1].size - 1] = dup_offset;
                              }
                          } else {
                              auto nd = node_pos_base + node_pos;
                              nodes[nd].child[nodes[nd].size - 1] = dup_offset;
                          }

                          // we store all duplicates, excluding the representative suffix stored in the main index
                          dup_all.push_back(dup_cur.size());
                          dup_all.insert(dup_all.end(), dup_cur.begin(), dup_cur.end());

                          if (dup_all.size() - node_dup_start > std::numeric_limits<sbt_index_type>::max()) {
                              log->warn("there are {} duplicate suffix entries for node {} but the sbt_index_type can only index up to {} entries!",
                                        dup_all.size() - node_dup_start, num_nodes_base + num_nodes, std::numeric_limits<sbt_index_type>::max());
                          }
                      } // if dup_cur

                      // start new count of duplicates
                      dup_cur.clear();

                      // add suffix to node
                      node_size = nodes[node_pos_base + node_pos].size;
                      nodes[node_pos_base + node_pos].A[node_size] = sa_pos;

                      if (node_size > 0) {
                          auto nd = node_pos_base + node_pos;

                          auto prev_sa_pos = nodes[nd].A[node_size - 1];
                          auto cur_lcp = detail::suf_pair_lcp(T, prev_sa_pos, sa_pos);

                          nodes[nd].LCP[node_size] = cur_lcp;
                          nodes[nd].C_L[node_size] = T[prev_sa_pos + cur_lcp];
                          nodes[nd].C_R[node_size] = T[sa_pos + cur_lcp];
                      } else {
                          auto nd = node_pos_base + node_pos;

                          // here node_size == 0
                          if (next_pos == next_beg) {
                              nodes[nd].LCP[node_size] = 0;
                          } else if ((next_pos > 0) && (next_pos == next_beg)) {
                              // we use this 666 for sanity check
                              log->debug("thread {} marks leaf {} for lcp[{}] correction...",
                                         tid, num_nodes_base + num_nodes, node_size);
                              nodes[nd].LCP[node_size] = 666;
                          } else {
                              nodes[nd].LCP[node_size] = detail::suf_pair_lcp(T, next[next_pos - 1].sa_pos, sa_pos);
                          }
                      }

                      nodes[node_pos_base + node_pos].size++;

                      // node is filled in
                      if (nodes[node_pos_base + node_pos].size == b) {
                          // this suffix will be propagated to the next layer
                          next[next_pos++] = {sa_pos, num_nodes_base + num_nodes};

                          num_nodes++;
                          node_pos++;

                          // if the next node has any duplicates, this will be the THREAD LOCAL position of the first entry
                          node_dup_start = dup_all.size();

                          #pragma omp critical
                          g_num_nodes++;

                          // cache is filled, let's write
                          if (node_pos == PACK_SIZE) {
                              #pragma omp critical
                              {
                                  offset_type boff = (tid * (suff_part / b) + wrt_nodes);

                                  of.seekp(boff * sizeof(dna_sbt_node<b>), std::ios::beg);
                                  of.write(reinterpret_cast<const char*>(nodes.data() + node_pos_base),
                                           PACK_SIZE * sizeof(dna_sbt_node<b>));

                                  if (!of) {
                                      log->error("cache write failed at offset {} nodes!", boff);
                                      of_err = true;
                                  }

                                  g_wrt_nodes += PACK_SIZE;

                                  int plrd = (100.0 * g_wrt_nodes) / curr_bcount;

                                  if (plrd - plrd_last >= 1) {
                                      log->info("{} leaves ready, {}%", g_wrt_nodes, plrd);
                                      plrd_last = plrd;
                                  }
                              }

                              wrt_nodes += PACK_SIZE;
                              node_pos = 0;
                          } // if node_pos == PACK_SIZE

                          nodes[node_pos_base + node_pos] = make_sbt_leaf<b>();
                          nodes[node_pos_base + node_pos].dup_start = dup_start[tid] + node_dup_start;
                      }
                  } else {
                      dup_cur.push_back(sa_pos);
                  }
              } // if lim
          } // for pos

          // write leftovers
          // a. add last suffix
          node_size = nodes[node_pos_base + node_pos].size;

          if (node_size > 0) {
              log->debug("thread {} has remaining node with {} suffixes", tid, node_size);

              next[next_pos++] = {nodes[node_pos_base + node_pos].A[node_size - 1], num_nodes_base + num_nodes};
              num_nodes++;

              #pragma omp critical
              g_num_nodes++;
          }

          // b. cover boundary case where we may have nothing to write
          node_pos += (node_size > 0);

          if (node_pos > 0) {
              #pragma omp critical
              {
                  log->debug("thread {} last write, {} nodes", tid, node_pos);

                  offset_type boff = (tid * (suff_part / b) + wrt_nodes);

                  of.seekp(boff * sizeof(dna_sbt_node<b>), std::ios::beg);
                  of.write(reinterpret_cast<const char*>(nodes.data() + node_pos_base), node_pos * sizeof(dna_sbt_node<b>));

                  if (!of) {
                      log->error("cache write failed at offset {} nodes!", boff);
                      of_err = true;
                  }

                  g_wrt_nodes += node_pos;
              }
          }

          // c. we need to add duplicates for the last suffix
          // doing this in the correction phase is the easiest implementation-wise
          if (!dup_cur.empty()) {
              #pragma omp critical
              {
                  log->debug("thread {} has duplicates remaining for its last suffix", tid);
                  auto dup_offset = dup_all.size() - node_dup_start;
                  fix_nodes.push_back({tid, num_nodes_base + num_nodes - 1, dup_offset});
              } // omp

              // we store all duplicates, excluding the suffix stored in the main index
              dup_all.push_back(dup_cur.size());
              dup_all.insert(dup_all.end(), dup_cur.begin(), dup_cur.end());

              if (dup_all.size() - node_dup_start > std::numeric_limits<sbt_index_type>::max()) {
                  log->warn("there are {} duplicate suffix entries for node {} but the sbt_index_type can only index up to {} entries!",
                            dup_all.size() - node_dup_start, num_nodes_base + num_nodes, std::numeric_limits<sbt_index_type>::max());
              }
          }

          // last thread trims next to the actual correct size
          #pragma omp barrier
          if (tid == (num_thr - 1)) next.resize(next_pos);

          g_dup_all[tid] = std::move(dup_all);
      } // #pragma omp

      if (of_err) return -1;


      if (g_wrt_nodes != g_num_nodes) {
          log->warn("whooops! {} nodes created but {} written :-(", g_num_nodes, g_wrt_nodes);
      }

      if (g_wrt_nodes != curr_bcount) {
          log->warn("whooops! mismatch between expected {} and {} written nodes :-(", curr_bcount, g_wrt_nodes);
      }

      // we now need to correct nodes affected by partitioning between threads
      if (num_thr > 1) {
          log->info("correcting partition boundary leaves...");

          for (int tid = 1; tid < num_thr; ++tid) {
              offset_type pos = tid * (suff_part / b);

              of.seekg(pos * sizeof(dna_sbt_node<b>), std::ios::beg);
              of.read(reinterpret_cast<char*>(nodes.data()), sizeof(dna_sbt_node<b>));

              if (!of) {
                  log->error("tree read failed at offset {} nodes!", pos);
                  return -1;
              }

              auto sa_pos = nodes[0].A[0];

              // if there is an issue it must be here ;-)
              auto next_pos = tid * block_part * b;
              auto nlcp = detail::suf_pair_lcp(T, next[next_pos - 1].sa_pos, sa_pos);

              log->debug("partition {}, node {}: correcting lcp from {} to {}", tid, pos, nodes[0].LCP[0], nlcp);

              nodes[0].LCP[0] = nlcp;

              of.seekp(pos * sizeof(dna_sbt_node<b>), std::ios::beg);
              of.write(reinterpret_cast<const char*>(nodes.data()), sizeof(dna_sbt_node<b>));

              if (!of) {
                  log->error("tree write failed at offset {} nodes!", pos);
                  return -1;
              }
          } // for tid
      } // if num_thr

      if (!fix_nodes.empty()) {
          log->info("correcting cache boundary leaves...");

          for (const auto& fix : fix_nodes) {
              auto tid = std::get<0>(fix);
              auto num_nodes = std::get<1>(fix);
              auto dup_size = std::get<2>(fix);

              offset_type pos = num_nodes * sizeof(dna_sbt_node<b>);

              of.seekg(pos, std::ios::beg);
              of.read(reinterpret_cast<char*>(nodes.data()), sizeof(dna_sbt_node<b>));

              if (!of) {
                  log->error("tree read failed at offset {} nodes!", pos);
                  return -1;
              }

              log->debug("correcting child from tid {} and node {}: from {} to {}",
                         tid, num_nodes, nodes[0].child[nodes[0].size - 1], dup_size);

              nodes[0].child[nodes[0].size - 1] = dup_size;

              of.seekp(pos, std::ios::beg);
              of.write(reinterpret_cast<const char*>(nodes.data()), sizeof(dna_sbt_node<b>));

              if (!of) {
                  log->error("tree write failed at offset {} nodes!", pos);
                  return -1;
              }
          } // for fix
      } // if fix_nodes

      log->info("{} tree leaves ready!", g_wrt_nodes);


      sidecar.layer = g_wrt_nodes;


      // compute total number of duplicate suffix entries
      offset_type dup_count_total = 0;

      for (int tid = 0; tid < num_thr; ++tid) {
          auto dup_count_pred = dup_size[tid];
          auto dup_count_real = g_dup_all[tid].size();

          if (static_cast<decltype(dup_count_real)>(dup_count_pred) != dup_count_real) {
              log->error("whooops! thread {} predicted {} duplicate suffix entries, but there are actually {} :-(",
                         tid, dup_count_pred, dup_count_real);
          }

          dup_count_total += dup_count_real;
      }

      // store duplicate suffixes
      log->info("storing {} duplicate suffixes...", dup_count_total);
      std::ofstream ds_of{dup_name};
      if (!ds_of) {
          log->error("unable to create {} file!", dup_name);
          return false;
      }

      for (int tid = 0; tid < num_thr; ++tid) {
          auto data = reinterpret_cast<const char*>(g_dup_all[tid].data());
          auto size = g_dup_all[tid].size() * sizeof(offset_type);
          ds_of.write(data, size);

          if (!ds_of) {
              log->error("writing {} failed!", dup_name);
              return -1;
          }
      }

      ds_of.close();

      auto ds_sz = std::filesystem::file_size(dup_name);
      log->info("{} file size: {}B ({})", dup_name, ds_sz, detail::format_size(ds_sz));


      // 2. create top layers
      log->info("processing remaining csbtree layers...");


      // go to the end :-)
      of.seekp(0, std::ios::end);

      sbt_index_type num_nodes = curr_bcount;

      sbt_index_type cur_node = 0;
      sbt_index_type node_size = 0;

      int layer = 0;

      std::vector<node_info> curr;
      curr.swap(next);

      l = curr.size();

      while (l != 1) {
          log->info("processing {} suffixes at layer {}...", l, layer);

          cur_node = 0;
          nodes[cur_node] = make_sbt_node<b>();

          for (sbt_index_type pos = 0; pos < l; ++pos) {
              auto sa_pos = curr[pos].sa_pos;
              auto child_pos = curr[pos].node_id;
              auto cur_lcp = (pos == 0) ? 0 : detail::suf_pair_lcp(T, curr[pos - 1].sa_pos, sa_pos);

              node_size = nodes[cur_node].size;
              nodes[cur_node].A[node_size] = sa_pos;
              nodes[cur_node].LCP[node_size] = cur_lcp;

              if (node_size > 0) {
                  nodes[cur_node].C_L[node_size] = T[nodes[cur_node].A[node_size - 1] + cur_lcp];
                  nodes[cur_node].C_R[node_size] = T[sa_pos + cur_lcp];
              }

              nodes[cur_node].child[node_size] = child_pos;
              nodes[cur_node].size++;

              if (nodes[cur_node].size == b) {
                  next.push_back({sa_pos, num_nodes});

                  cur_node++;
                  num_nodes++;

                  // cache is filled, let's write
                  if (cur_node == PACK_SIZE) {
                      of.write(reinterpret_cast<const char*>(nodes.data()),
                               PACK_SIZE * sizeof(dna_sbt_node<b>));
                      cur_node = 0;
                  }

                  nodes[cur_node] = make_sbt_node<b>();
              } // if nodes
          } // for pos

          // write leftovers
          // a. add last suffixame
          node_size = nodes[cur_node].size;

          if (node_size > 0) {
              next.push_back({nodes[cur_node].A[node_size - 1], num_nodes});
              num_nodes++;
          }

          // b. cover boundary case where we may have nothing to write
          cur_node += (node_size > 0);
          of.write(reinterpret_cast<const char*>(nodes.data()), cur_node * sizeof(dna_sbt_node<b>));

          curr.swap(next);
          next.clear();

          l = curr.size();

          log->info("layer {} ready, current tree size: {} nodes", layer, num_nodes);

          if (num_nodes > std::numeric_limits<sbt_index_type>::max()) {
              log->warn("there are {} tree nodes, but the sbt_index_type can only index up to {} entries!",
                        num_nodes, std::numeric_limits<sbt_index_type>::max());
          }

          layer++;
      } // while curr.size()

      // finalize
      of.close();

      log->info("tree is complete!");

      auto sz = std::filesystem::file_size(csbt_name);
      log->info("{} file size: {}B ({})", csbt_name, sz, detail::format_size(sz));


      // compute hash of first node
      std::vector<char> buf(CSBTree<b>::node_size(), 0);

      of.clear();
      of.open(csbt_name, of.binary | of.in);
      of.read(buf.data(), buf.size());
      if (!of) {
          log->error("whooops, the index disappeared :-(");
          return -1;
      }
      of.close();


      // handle sidecar remaining data
      detail::murmur64A hasher;
      sidecar.hash = hasher(buf.data(), buf.size());

      if (!sidecar.write(sidecar_name)) {
          log->warn("unable to create sidecar file {}!", sidecar_name);
      }

      log->info("csbtree construction routine done!");

      return num_nodes;
  } // create_csbtree


  template <int b, typename Logger>
  inline int create_csbtree(const std::string& name, const std::string& dup_name,
                            const std::string& sidecar_name, const char* T, const length_type* L,
                            const std::string& SA_name, const std::string& LCP_name,
                            int lim, long long int cache, Logger log) {
      // get pointer to SA
      io::mapped_file_source SA_mf;

      log->info("mapping suffix array...");

      try {
          SA_mf.open(SA_name);
      } catch (...) {
          log->error("unable to map suffix array!");
          return -1;
      }

      const offset_type* SA = reinterpret_cast<const offset_type*>(SA_mf.data());
      offset_type l = SA_mf.size() / sizeof(offset_type);

      log->info("suffix array ready!");

      log->info("mapping lcp array...");

      // get pointer to LCP
      io::mapped_file_source LCP_mf;

      try {
          LCP_mf.open(LCP_name);
      } catch (...) {
          log->error("unable to map lcp array!");
          return -1;
      }

      const offset_type* LCP = reinterpret_cast<const offset_type*>(LCP_mf.data());

      log->info("lcp array ready!");

      return create_csbtree<b>(name, dup_name, sidecar_name, T, L, SA, LCP, l, lim, cache, log);
  } // create_csbtree


  template <int b>
  inline bool csbtree_to_dot(const std::string& in, const std::string& out) {
      io::mapped_file_source BT_mf;
      BT_mf.open(in);

      auto n = (std::filesystem::file_size(in) / sizeof(dna_sbt_node<b>));
      const dna_sbt_node<b>* BT = reinterpret_cast<const dna_sbt_node<b>*>(BT_mf.data());

      return csbtree_to_dot(out, BT, n);
  } // csbtree_to_dot


  /// Returns reasonable b given desired page size.
  /// The choice of PageSize is tricky:
  /// 4KB is old-school and probably too small though it matches usual device block size.
  /// gRPC recommends 16KB-64KB message size.
  /// Keep in mind that using page above 64KB most likely will overwhelm stack anyway.
  template <int PageSize>
  constexpr int csbtree_suffixes_per_node() {
      // b is calculated based on the components of dna_sbt_node<b>.
      constexpr int b = ((PageSize - sizeof(suffix_index_type) - sizeof(offset_type) - sizeof(char))
                        / (sizeof(offset_type) + sizeof(length_type) + sizeof(sbt_index_type) + (2 * sizeof(char)))) & ~1;

      static_assert(b > 3, "PageSize too small!");
      static_assert(b <= std::numeric_limits<suffix_index_type>::max(), "PageSize too large! Too many suffixes per node!");
      static_assert(sizeof(dna_sbt_node<b>) <= PageSize, "Whooops, it seems we messed up node capacity calculations...");

      return b;
  } // csbtree_suffixes_per_node


  // Validates that the program version, the page size, and the hash of the first node of the given CSBT file match
  // those found in the given sidecar file.
  template <typename Logger>
  inline bool csbtree_validate(const std::string& csbt_name, const std::string& sidecar_name,
                               int page_size, bool packed, Logger log) {
      std::ifstream sf{sidecar_name};

      if (!sf) {
          log->error("unable to validate, sidecar file {} missing!", sidecar_name);
          return false;
      }

      std::unordered_map<std::string, std::string> scv;

      while (!sf.eof()) {
          std::string key, val;
          sf >> key;
          sf >> val;
          scv[key] = val;
      }

      sf.close();


      std::ifstream cf{csbt_name, cf.binary | cf.in};

      if (!cf) {
          log->error("validation failed, tree file {} missing!", csbt_name);
          return false;
      }

      try {
          auto [major, minor, patch] = detail::parse_version(DNASBT_PROJECT_VERSION);

          auto major_sc = scv["Major:"];

          if (major != major_sc) {
              log->error("validation failed! the major version {} does not match {}", major, major_sc);
              return false;
          }

          auto minor_sc = scv["Minor:"];

          if (minor != minor_sc) {
              log->error("validation failed! the minor version {} does not match {}", minor, minor_sc);
              return false;
          }

          auto page_size_sc = std::stoi(scv["Block:"]);

          if (page_size != page_size_sc) {
              log->error("validation failed! the page size {} does not match {}", page_size, page_size_sc);
              return false;
          }

          std::uint64_t hash = 0;

          if (packed) {
              // read metadata then skip over padding
              int cb_size = 0;
              cf.read(reinterpret_cast<char*>(&cb_size), sizeof(int));
              cf.ignore(DNASBT_DEV_BLOCK_SIZE - sizeof(int));

              // read and decompress first node
              std::vector<char> buf_c(cb_size);
              std::vector<char> buf_d(page_size);

              cf.read(buf_c.data(), cb_size);
              int bs = *reinterpret_cast<int*>(&buf_c[0]);

              auto res = LZ4_decompress_safe(buf_c.data() + sizeof(int), buf_d.data(), bs, page_size);

              if (res < 0) {
                  log->error("validation failed! unable to decompress packed CSBT node");
                  return false;
              }

              detail::murmur64A hasher;
              hash = hasher(buf_d.data(), buf_d.size());
          } else {
              std::vector<char> buf(page_size, 0);
              cf.read(buf.data(), buf.size());

              detail::murmur64A hasher;
              hash = hasher(buf.data(), buf.size());
          }

          auto hash_sc = std::stoull(scv["BHash:"]);

          if (hash != hash_sc) {
              log->error("validation failed! the check hash {} does match {}", hash, hash_sc);
              return false;
          }

          auto large_sc = std::stoi(scv["Large:"]);

          if (DNASBT_USE_LARGE_NODE_OFFSETS != large_sc) {
              log->error("validation failed! node offset size switch {} does not match {}", large_sc, DNASBT_USE_LARGE_NODE_OFFSETS);
              return false;
          }

      } catch (const std::exception& e) {
          log->error("validation failed with exception: {}", e.what());
          return false;
      }

      return true;
  } // csbtree_validate

  inline bool csbtree_validate(const std::string& csbt_name, const std::string& sidecar_name,
                               int page_size, bool packed) {
      return csbtree_validate(csbt_name, sidecar_name, page_size, packed, std::make_shared<null_logger>());
  } // csbtree_validate


  /// Implementation of the Compacted String B-Tree (CSBT).
  ///
  /// The load() member function must be called before calling any other member functions on the CSBTree, otherwise
  /// it's undefined behavior. Additionally, calling load() more than once is also undefined behavior.
  ///
  /// Thread Safety:
  /// Assuming the above requirements are satisfied, all member functions on a CSBTree are thread-safe.
  template <int b,
            template<typename> typename TextStorageManager,
            template<typename> typename CsbtStorageManager,
            template<typename> typename DupStorageManager,
            template<typename> typename TextMemoryManager,
            template<typename> typename CsbtMemoryManager,
            template<typename> typename DupMemoryManager>
  class CSBTree {
  public:
      using node_type = dna_sbt_node<b>;

      using text_manager_type = TextMemoryManager<TextStorageManager<text_block>>;
      using sbt_manager_type = CsbtMemoryManager<CsbtStorageManager<node_type>>;
      using dup_manager_type = DupMemoryManager<DupStorageManager<offset_type>>;

      static constexpr int node_size() { return sizeof(node_type); }
      static constexpr int suffixes_per_node() { return b; }


      CSBTree() = default;

      CSBTree(CSBTree&& other) = default;
      CSBTree& operator=(CSBTree&& other) = default;


      /// Initializes the CSBT using the provided initialized memory managers. Using the CSBT without
      /// calling this method first is undefined behavior. The arguments text_manager, sbt_manager, and dup_manager
      /// correspond to the memory managers for the text, CSBT nodes, and duplicate suffix files respectively.
      /// Calling load for memory managers referencing an invalid CSBT (e.g., a CSBT built for a different block size)
      /// is undefined behavior. You can use the function csbtree_validate to perform validation.
      ///
      /// Additionally, calling load() more than once is undefined behavior.
      bool load(text_manager_type&& text_manager, sbt_manager_type&& sbt_manager, dup_manager_type&& dup_manager) {
          text_manager_ = std::move(text_manager);
          csbt_manager_ = std::move(sbt_manager);
          dup_manager_ = std::move(dup_manager);
          return true;
      } // load


      /// Given a pattern P and an output iterator out, identifies all suffixes in the CSBT that share the longest
      /// prefix with P. Takes an optional threshold argument. If provided, occurrences will only be reported when
      /// the length of the match (i.e., the lcp) is at least the threshold. If the second optional argument, report, is
      /// set to false, then only the length of the match will be reported, and not any of the occurrences. All
      /// occurrences are pushed to the back of the given iterator and the order in which occurrences are pushed is
      /// unspecified. Calling this method without properly loading the CSBT first will result in undefined behavior.
      ///
      /// \tparam OutputIterator    Iterator type supporting appending outputs to the end of the iterator.
      /// \tparam StringType        String-like type supporting `size const`, `length const`, and `operator[] const`.
      template <typename OutputIterator, typename StringType>
      int find(const StringType& P, OutputIterator out, int threshold = 0, bool report = true) const {
          return detail::longest_prefix_search<b>(csbt_manager_.size() - 1, csbt_manager_, text_manager_,
                                                  dup_manager_, P, out, threshold, report);
      } // find


      /// Calls find over a char* pattern P. Note that this requires computing the length of P. If the length is known,
      /// a more efficient approach is to construct some other string type (e.g., `std::string_view`), passing that
      /// value into the template version of `find`.
      template <typename OutputIterator>
      int find(const char* P, OutputIterator out, int threshold = 0, bool report = true) const {
          return find(std::string_view{P}, out, threshold, report);
      } // find


      [[nodiscard]] const text_manager_type& text_manager() const {
          return text_manager_;
      } // text_manager


      [[nodiscard]] const sbt_manager_type& csbt_manager() const {
          return csbt_manager_;
      }  // sbt_manager


      [[nodiscard]] const dup_manager_type& dup_manager() const {
          return dup_manager_;
      } // dup_manager


  private:
      text_manager_type text_manager_;
      sbt_manager_type csbt_manager_;
      dup_manager_type dup_manager_;

  }; // class CSBTree

} // namespace sbt

#endif // DNASBT_HPP
