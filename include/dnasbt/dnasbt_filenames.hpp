/***
 *  $Id$
 **
 *  File: dnasbt_filenames.hpp
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *          Andrew Mikalsen <ajmikals@buffalo.edu>
 *
 *  Copyright (c) 2023 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#ifndef DNASBT_FILENAMES_HPP
#define DNASBT_FILENAMES_HPP

#include <string>

#define DNASBT_CSBT_NAME ".dnasbt_csbt"
#define DNASBT_PCSBT_NAME ".dnasbt_pcsbt"
#define DNASBT_TEXT_NAME ".dnasbt_text"
#define DNASBT_DSUF_NAME ".dnasbt_dsuf64"
#define DNASBT_INFO_NAME ".dnasbt_info"

#define DNASBT_TIDX_NAME ".dnasbt_tidx"
#define DNASBT_TNAMES_NAME ".dnasbt_tnames"

#define DNASBT_SA_NAME ".sa64"
#define DNASBT_LCP_NAME ".lcp64"
#define DNASBT_LEN_NAME ".len16"

namespace sbt {

  // file names
  [[nodiscard]] inline std::string csbt_name(const std::string& name) noexcept { return name + DNASBT_CSBT_NAME; }
  [[nodiscard]] inline std::string pcsbt_name(const std::string& name) noexcept { return name + DNASBT_PCSBT_NAME; }
  [[nodiscard]] inline std::string text_name(const std::string& name) noexcept { return name + DNASBT_TEXT_NAME; }
  [[nodiscard]] inline std::string dsuf_name(const std::string& name) noexcept { return name + DNASBT_DSUF_NAME; }
  [[nodiscard]] inline std::string info_name(const std::string& name) noexcept { return name + DNASBT_INFO_NAME; }

  [[nodiscard]] inline std::string tidx_name(const std::string& name) noexcept { return name + DNASBT_TIDX_NAME; }
  [[nodiscard]] inline std::string tnames_name(const std::string& name) noexcept { return name + DNASBT_TNAMES_NAME; }

  // these files are temporary and can be removed once sbt is available
  [[nodiscard]] inline std::string sa_name(const std::string& name) noexcept { return name + DNASBT_SA_NAME; }
  [[nodiscard]] inline std::string lcp_name(const std::string& name) noexcept { return name + DNASBT_LCP_NAME; }
  [[nodiscard]] inline std::string len_name(const std::string& name) noexcept { return name + DNASBT_LEN_NAME; }

} // namespace sbt

#endif // DNASBT_FILENAMES_HPP
