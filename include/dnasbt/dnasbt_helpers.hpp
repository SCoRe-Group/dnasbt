/***
 *  $Id$
 **
 *  File: dnasbt_helpers.hpp
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *          Andrew Mikalsen <ajmikals@buffalo.edu>
 *
 *  Copyright (c) 2023 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#ifndef DNASBT_HELPERS_HPP
#define DNASBT_HELPERS_HPP

#include <fstream>
#include <iostream>
#include <queue>
#include <string>

#include "dnasbt_types.hpp"


namespace sbt {

  inline int char2index(char c) {
      switch (c) {
        case '$': return 0;
        case 'A': return 1;
        case 'C': return 2;
        case 'G': return 3;
        case 'T': return 4;
      }
      return 666;
  } // return char2index

  inline char index2char(int index) {
      switch (index) {
        case 0: return '$';
        case 1: return 'A';
        case 2: return 'C';
        case 3: return 'G';
        case 4: return 'T';
      }
      return '?';
  } // return index2char


  template <int b>
  inline bool csbtree_to_dot(const std::string& name, const dna_sbt_node<b>* BT, int n) {
      std::ofstream of(name);
      if (!of) return false;

      of << "digraph g {\n";
      of << "node [shape = record, fontname = Helvetica];\n";

      for (int i = 0; i < n; ++i) {
          of << "node" << i;
          of << "[label=\""
             << "{<f0>id: " << i << "\\l|"
             << "<f1>size: " << BT[i].size << "\\l|";
          of << "{";

          if (BT[i].size <= 8) {
              for (int j = 0; j < BT[i].size; ++j) {
                  of << BT[i].A[j] << "|";
              }
          } else {
              of << BT[i].A[0] << "|";
              of << "...|";
              of << BT[i].A[BT[i].size - 1];
          }

          of << "}}\"];\n";

          for (int j = 0; j < b; ++j) {
              if (BT[i].child[j] != -1 && !is_sbt_leaf(BT[i])) {
                  of << "\"node" << i << "\" -> "
                     << "\"node" << BT[i].child[j] << "\"\n";
              }
          } // for j

      } // for i

      of << "}";

      return true;
  } // csbtree_to_dot


  struct null_logger {
      template <typename ...Ts> void error(Ts...) { }
      template <typename ...Ts> void info(Ts...) { }
  }; // struct null_logger

} // namespace sbt

#endif // DNASBT_HELPERS_HPP
