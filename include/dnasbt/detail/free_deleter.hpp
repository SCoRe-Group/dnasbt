/***
 *  $Id$
 **
 *  File: free_deleter.hpp
 *  Author: Andrew Mikalsen <ajmikals@buffalo.edu>
 *
 *  Copyright (c) 2023 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#ifndef DNASBT_DETAIL_FREE_DELETER
#define DNASBT_DETAIL_FREE_DELETER

#include <cstdlib>


namespace sbt::detail {

  /// A deleter for POD allocations that uses free instead of delete
  template <typename T>
  class free_deleter {
  public:
      void operator()(const T* ptr) {
          std::free(const_cast<T*>(ptr));
      } // operator()
  }; // class free_deleter

} // namespace sbt::detail

#endif // DNASBT_DETAIL_FREE_DELETER
