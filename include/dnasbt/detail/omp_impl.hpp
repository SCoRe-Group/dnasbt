/***
 *  $Id$
 **
 *  File: omp_impl.hpp
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *
 *  Copyright (c) 2020 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#ifndef OMP_IMPL_HPP
#define OMP_IMPL_HPP

#ifdef _OPENMP
#include <omp.h>
#else
inline int omp_get_num_threads() { return 1; }
inline int omp_get_thread_num() { return 0; }
#endif

#endif // OMP_IMPL_HPP
