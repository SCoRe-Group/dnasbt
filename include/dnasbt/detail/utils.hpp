/***
 *  $Id$
 **
 *  File: utils.hpp
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *
 *  Copyright (c) 2023 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#ifndef DNASBT_DETAIL_UTILS_HPP
#define DNASBT_DETAIL_UTILS_HPP

#include <cinttypes>
#include <cmath>
#include <cstdint>
#include <filesystem>
#include <fstream>
#include <string>
#include <vector>


namespace sbt::detail {

  inline std::string format_size(long long int sz) {
      int i = 0;
      double S = sz;
      for (; (S >= 1024.0) && (i < 7); S /= 1024.0, ++i);
      std::string s = std::to_string(S);
      s.erase(std::next(s.begin(), s.find('.')) + 3, s.end());
      s += "BKMGTPE!"[i];
      return s;
  } // format_size


  template <typename LogType>
  inline bool write_file(LogType log, const std::string& name, const char* data, uint64_t size) {
      std::ofstream of(name);

      if (!of) {
          log->error("unable to create {} file!", name);
          return false;
      }

      of.write(data, size);

      if (!of) {
          log->error("writing {} failed!", name);
          return false;
      }

      of.close();

      auto sz = std::filesystem::file_size(name);
      log->info("{} file size: {}B ({})", name, sz, format_size(sz));
      return true;
  } // write_file


  template <typename LogType, typename KeyType, typename ValueType>
  inline bool write_map(LogType log, const std::string& name, const std::vector<std::pair<KeyType, ValueType>>& dict) {
      std::ofstream of(name);

      if (!of) {
          log->error("unable to create {} file!", name);
          return false;
      }

      for (const auto& s : dict) {
          of << s.first << " " << s.second << "\n";
      }

      if (!of) {
          log->error("writing {} failed!", name);
          return false;
      }

      of.close();

      auto sz = std::filesystem::file_size(name);
      log->info("{} file size: {}B ({})", name, sz, format_size(sz));

      return true;
  } // write_map

} // namespace sbt::detail

#endif // DNASBT_DETAIL_UTILS_HPP
