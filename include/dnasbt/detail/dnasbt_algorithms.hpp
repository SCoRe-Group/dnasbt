/***
 *  $Id$
 **
 *  File: dnasbt_algorithms.hpp
 *  Author: Andrew Mikalsen <ajmikals@buffalo.edu>
 *
 *  Copyright (c) 2023 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#ifndef DETAIL_DNASBT_ALGORITHMS_HPP
#define DETAIL_DNASBT_ALGORITHMS_HPP

#include <algorithm>
#include <cassert>
#include <cstddef>
#include <iterator>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include <dnasbt/dnasbt_helpers.hpp>
#include <dnasbt/dnasbt_types.hpp>


namespace sbt::detail {


  /// Compute length of lcp between given pattern and suffix pointer into text, given we know the first l characters
  /// match. The string pointed to by suffix must be $-terminated.
  template <typename StringType, typename TextBlockManager>
  inline std::pair<length_type, char>
  pattern_suffix_lcp(const TextBlockManager& text_manager, const StringType& pattern, offset_type suffix, length_type l) {
      length_type lcp = l;

      int block_size = sizeof(typename TextBlockManager::value_type);

      int blk_id = (suffix + lcp) / block_size;
      int pos = (suffix + lcp) % block_size;

      auto curr_block = text_manager[blk_id];
      const char* curr_char = curr_block->T;

      while ((lcp < pattern.size()) && (pattern[lcp] == curr_char[pos])) {
          pos++;
          lcp++;

          if (pos == block_size) {
              blk_id++;
              curr_block = text_manager[blk_id];
              curr_char = curr_block->T;
              pos = 0;
          }
      } // while lcp

      return {lcp, curr_char[pos]};
  } // pattern_suffix_lcp


  /// Concept for class providing static functions abstracting the internal search of a CSBT node.
  class csbt_internal_search_concept {
  public:
      /// Returns the position of P among the suffixes in the current node, i.e., the largest j s.t. P <= suffix j,
      /// where j lies in the range [0..b). Also returns the length of the LCP between P and the suffixes in the current
      /// node. Finally, returns has_max which is true iff the suffix returned (j) has the max lcp with P.
      template <int b, typename StringType, typename TextBlockManager>
      static std::tuple<suffix_index_type, length_type, bool> search_pos(const dna_sbt_node<b>& node,
                                                                         const TextBlockManager& T,
                                                                         const StringType& P,
                                                                         length_type l);

      /// Returns two integers in [0..b) denoting the exclusive range of suffixes in the current node that have the LCP
      /// with P, and a third integer denoting the length of the LCP.
      template <int b, typename StringType, typename TextBlockManager>
      static std::tuple<suffix_index_type, suffix_index_type, length_type> search_occ(const dna_sbt_node<b>& node,
                                                                                      const TextBlockManager& T,
                                                                                      const StringType& P,
                                                                                      length_type l);

      template <int b>
      static offset_type get_suffix(const dna_sbt_node<b>& node, suffix_index_type i);
  }; // csbt_internal_search_concept


  /// Internal search over a CSBT node via a single scan over LCP and C_R.
  class csbt_parray_internal_search {
  public:
      template <int b, typename StringType, typename TextBlockManager>
      static std::tuple<suffix_index_type, length_type, bool> search_pos(const dna_sbt_node<b>& node,
                                                                         const TextBlockManager& T,
                                                                         const StringType& P,
                                                                         length_type l) {
          auto j = m_blind_search__(node, P);
          auto [lcp, c] = pattern_suffix_lcp(T, P, node.A[j], l);

          // start by assuming j is correct (and thus has max lcp), and correct this as we correct j
          bool has_max = true;

          if (P.size() == lcp || P[lcp] < c) { // then P comes before A[j], so scan left
              // we skip all A[j] values with c at A[j][lcp]. when we come across a new character, we check if it comes
              // before P[lcp]. if so, this character is "virtually assigned" to c and we continue scanning
              while (j > 0 && (node.LCP[j] > lcp || (node.LCP[j] == lcp && node.C_L[j] > P[lcp]))) {
                  if (node.LCP[j] < lcp) {
                      has_max = false;
                  }
                  --j;
              } // while j
          } else { // then P comes after A[j], so scan right
              // we know P's position is after j, so we shift j up, and then continue scanning using the same idea as
              // before
              ++j;
              if (j == node.size || node.LCP[j] < lcp) {
                  has_max = false;
              }
              while (j < node.size && (node.LCP[j] > lcp || (node.LCP[j] == lcp && node.C_R[j] < P[lcp]))) {
                  ++j;
                  if (j == node.size || node.LCP[j] < lcp) {
                      has_max = false;
                  }
              } // while j
          }

          return {j, lcp, has_max};
      } // search_pos


      template <int b, typename StringType, typename TextBlockManager>
      static std::tuple<suffix_index_type, suffix_index_type, length_type> search_occ(const dna_sbt_node<b>& node,
                                                                                      const TextBlockManager& T,
                                                                                      const StringType& P,
                                                                                      length_type l) {
          auto j = m_blind_search__(node, P);
          auto [lcp, c] = pattern_suffix_lcp(T, P, node.A[j], l);

          // scan left for occurrences
          auto begin = j;
          while (begin > 0 && node.LCP[begin] >= lcp) --begin;

          // scan right for occurrences
          auto end = j + 1;
          while (end < node.size && node.LCP[end] >= lcp) ++end;

          return {begin, end, lcp};
      } // search_occ


      template <int b>
      static offset_type get_suffix(const dna_sbt_node<b>& node, suffix_index_type i) {
          return node.A[i];
      } // get_suffix


      template <int b, typename StringType>
      static suffix_index_type m_blind_search__(const dna_sbt_node<b>& node, const StringType& P) {
          suffix_index_type j = 0;
          suffix_index_type k = 1;

          while (k < node.size) {
              auto l = node.LCP[k];
              if (l < P.size() && P[l] == node.C_R[k]) {
                  j = k;
              } else {
                  while (k + 1 < node.size && node.LCP[k + 1] > l) {
                      ++k;
                  } // while k + 1
              }
              ++k;
          } // while k

          return j;
      } // m_blind_search__
  }; // csbt_parray_internal_search


  /// Search over csbt node with binary search in the suffix array.
  class csbt_sa_bs_internal_search {
  public:
      /// Returns the position of P among the suffixes in the current CSBT node, i.e., the largest j s.t. P <= suffix j,
      /// where j lies in the range [0..b). Also returns the length of the LCP between P and the suffixes in the current
      /// CSBT node.
      template <int b, typename StringType, typename TextBlockManager>
      static std::pair<suffix_index_type, length_type> search_pos(const dna_sbt_node<b>& node,
                                                                  const TextBlockManager& T,
                                                                  const StringType& P,
                                                                  length_type) {
          // do binary search over internal suffix array
          //auto max_l = l;
          auto comp = [&T](const offset_type& s, const StringType& p) { return m_s_p_lt__(T, s, p); };
          auto lower = std::lower_bound(std::begin(node.A), std::begin(node.A) + node.size, P, comp);
          auto j = lower - std::begin(node.A);

          // either the left or right of P is guaranteed to have the LCP, but we don't know which one, so we check both
          auto right_lcp = j < node.size ? m_s_p_lcp__(T, node.A[j], P) : 0;
          auto left_lcp = j > 0 ? m_s_p_lcp__(T, node.A[j - 1], P) : 0;
          auto lcp = std::max(right_lcp, left_lcp);

          return {j, lcp};
      }

      /// Returns two integers in [0..b) denoting the exclusive range of suffixes in the current node that have the LCP
      /// with P, and a third integer denoting the length of the LCP.
      template <int b, typename StringType, typename TextBlockManager>
      static std::tuple<suffix_index_type, suffix_index_type, length_type> search_occ(const dna_sbt_node<b>& node,
                                                                                      const TextBlockManager& T,
                                                                                      const StringType& P,
                                                                                      length_type) {
          auto comp = [&T](const offset_type& s, const StringType& p) { return m_s_p_lt__(T, s, p); };
          auto lower = std::lower_bound(std::begin(node.A), std::begin(node.A) + node.size, P, comp);

          auto j = lower - std::begin(node.A);

          // either the left or right of P is guaranteed to have the LCP, but we don't know which one, so we check both
          auto right_lcp = j < node.size ? m_s_p_lcp__(T, node.A[j], P) : 0;
          auto left_lcp = j > 0 ? m_s_p_lcp__(T, node.A[j - 1], P) : 0;
          auto lcp = std::max(right_lcp, left_lcp);
          auto scan_start = (lcp == right_lcp && j < node.size) ? j : j - 1;

          // scan right for occurrences
          auto end = scan_start + 1;
          while (end < node.size && node.LCP[end] >= lcp) ++end;

          // scan left for occurrences
          auto begin = scan_start;
          while (begin > 0 && node.LCP[begin] >= lcp) --begin;

          return {begin, end, lcp};
      }

      template <int b>
      static offset_type get_suffix(const dna_sbt_node<b>& node, suffix_index_type i) {
          return node.A[i];
      } // get_suffix

  private:
      /// Compute LCP length between suffix pointer s and pattern P.
      template <typename StringType, typename TextBlockManager>
      static length_type m_s_p_lcp__(const TextBlockManager& T,
                                     offset_type s,
                                     const StringType& P) {

          // get the id of the text block we should be working with
          // and the position within the text
          int block_size = sizeof(typename TextBlockManager::value_type);

          int blk_id = s / block_size;
          int pos = s % block_size;

          // get handle to the block
          // and pointer to the current char
          auto curr_block = T[blk_id];
          const char* curr_char = curr_block->T;

          length_type lcp = 0;

          while (lcp < P.size() && curr_char[pos] != '$') {
              if (P[lcp] != curr_char[pos]) break;

              pos++;
              lcp++;

              // load new block if needed
              if (pos == block_size) {
                  blk_id++;
                  curr_block = T[blk_id];
                  curr_char = curr_block->T;
                  pos = 0;
              }

          } // while lcp

          return lcp;
      } // m_s_p_lcp__

      template <typename StringType, typename TextBlockManager>
      static bool m_s_p_lt__(const TextBlockManager& T, const offset_type& s, const StringType& P) {
          int block_size = sizeof(typename TextBlockManager::value_type);

          int blk_id = s / block_size;
          int pos = s % block_size;

          auto curr_block = T[blk_id];
          const char* curr_char = curr_block->T;

          // compute length of LCP
          length_type lcp = 0;

          while (lcp < P.size() && curr_char[pos] != '$') {
              if (P[lcp] != curr_char[pos]) break;

              pos++;
              lcp++;

              // load new block if needed
              if (pos == block_size) {
                  blk_id++;
                  curr_block = T[blk_id];
                  curr_char = curr_block->T;
                  pos = 0;
              }

          } // while lcp

          // compare mismatched character
          return curr_char[pos] < P[lcp];
      } // m_s_p_lt__

  }; // class csbt_sa_bs_internal_search


  /// Finds the leftmost leaf descending from the specified node in the given CSBT.
  ///
  /// \param nodes CSBT to search.
  /// \param i     Index of the node in the CSBT.
  ///
  /// \return      Index of `i`'s leftmost descending leaf.
  template<typename CsbtBlockManager>
  inline auto sbt_leftmost_leaf(const CsbtBlockManager& nodes,
                                typename CsbtBlockManager::size_type i) {
      auto node_page = nodes[i];
      while (!is_sbt_leaf(*node_page)) {
          i = node_page->child[0];
          node_page = nodes[i];
      } // while
      return i;
  } // sbt_leftmost_leaf


  /// Using the given CSBT, reports all indexed suffixes that have the longest matching prefix with the given pattern P.
  /// Optionally takes a threshold which, if provided, results in reporting occurrences only if the length of the
  /// match is at least the specified threshold. Pushes all positions to the back of out with no ordering guarantees
  /// and returns the length of the match.
  ///
  /// \tparam b Maximum number of suffixes packed into each CSBT node.
  /// \tparam StringType String-like type supporting `size`, `length`, and `operator[]`.
  /// \tparam TextBlockManager Type of the block manager for the text.
  /// \tparam CsbtBlockManager Type of the block manager for the CSBT nodes.
  /// \tparam DupBlockManager Type of the block manager for the duplicate suffixes.
  /// \tparam Debug If true, debugging features are enabled.
  ///
  /// \param root Index of the SBT root node.
  /// \param nodes Nodes of the SBT to search.
  /// \param T Text the SBT is built over.
  /// \param P Pattern to search for.
  /// \param out Iterator used to output occurrences of the longest matching prefix of P.
  /// \param threshold Minimum length of a match to report occurrences.
  /// \param report If false, only the length of the match will be reported, and occurrences won't be output.
  ///
  /// \return Length of the longest matching prefix of P.
  template <int b,
            typename OutputIterator,
            typename StringType,
            typename TextBlockManager,
            typename CsbtBlockManager,
            typename DupBlockManager,
            typename InternalSearch = csbt_parray_internal_search,
            bool Debug = false>
  inline length_type longest_prefix_search(sbt_index_type root,
                                           const CsbtBlockManager& nodes,
                                           const TextBlockManager& T,
                                           const DupBlockManager& dups,
                                           const StringType& P,
                                           OutputIterator& out,
                                           int threshold = 0,
                                           bool report = true) {

      using internal_search_type = InternalSearch;

      // index of current node and the loaded page for that node
      auto curr_node = root;
      auto curr_page = nodes[curr_node];

      // get position of P in current node's suffixes (j) and length of current LCP (l)
      auto [j, l, has_max] = internal_search_type::search_pos(*curr_page, T, P, 0);

      if (j == curr_page->size) {
          // P positioned after the last string: descend to rightmost node of root
          while (!is_sbt_leaf(*curr_page)) {
              // find number of children (since not a leaf, we know there is at least one child)
              int len = 1;
              while (len < b && curr_page->child[len] != -1) ++len;

              // descend to last child
              curr_node = curr_page->child[len - 1];
              curr_page = nodes[curr_node];
          }
      } else if (!is_sbt_leaf(*curr_page)) { // j < curr_page->size
          // search down CSBT until reaching a leaf
          while (true) {
              // descend to node corresponding to current position
              curr_node = curr_page->child[j];
              curr_page = nodes[curr_node];

              // stop descending once a leaf is reached
              if (is_sbt_leaf(*curr_page)) break;

              // if the current node doesn't have the previous level's max lcp, then P comes before the leftmost suffix
              // in this subtree
              if (!has_max && curr_page->LCP[0] < l) {
                  // find leftmost descending leaf
                  while (!is_sbt_leaf(*curr_page)) {
                      curr_node = curr_page->child[0];
                      curr_page = nodes[curr_node];
                  } // while !is_sbt_leaf
                  break;
                  // TODO: this edge case will result in +1 I/O, since all occurrences will be left of P's position.
                  //       if we store the depth of the tree, then we can descend to curr_page->child[0] - 1 and avoid
                  //       this extra I/O (storing depth could be a header to the CSBT file and come at virtually no cost)
              }

              // not at a leaf yet, so continue search
              std::tie(j, l, has_max) = internal_search_type::search_pos(*curr_page, T, P, l);
          } // while true
      } // if j

      // find all occurrences in the current leaf that share the max lcp
      auto [first, last, lcp] = InternalSearch::search_occ(*curr_page, T, P, 0);

      // the LCP is guaranteed to be in either the current leaf or the leaf to its left. if in the leaf to the current
      // leaf's left, since all occurrences of the LCP must be contiguous with P's position and because the next left
      // suffix (last suffix in the left leaf) was in the suffixes of the node on the last-searched level, it can be seen
      // that this occurs if and only if lcp < l
      if (lcp < l) {
          curr_page = nodes[--curr_node];
          std::tie(first, last, lcp) = InternalSearch::search_occ(*curr_page, T, P, l);
      }

      if ((lcp >= threshold) && report) {
          // push all occurrences to output iterator
          for (int i = first; i < last; ++i) {
              *(out++) = internal_search_type::get_suffix(*curr_page, i);

              if (curr_page->child[i] != -1) {
                  auto d_count = dups[curr_page->dup_start + curr_page->child[i]];
                  for (sbt_index_type d_idx = 0; d_idx < *d_count; ++d_idx) {
                      auto d = dups[curr_page->dup_start + curr_page->child[i] + 1 + d_idx];
                      *(out++) = *d;
                  } // while d_idx
              }
          } // for i

          if (nodes.size() > 1) { // this check handles the edge case that the root is the only node in the tree, and thus
                                  // also the only leaf, since the code below assumes the last leaf will be followed by a
                                  // non-leaf node
              bool check_right = last == curr_page->size;
              bool check_left = first == 0;

              // scan right: repeat until there is a break in occurrences
              auto next_first = first;
              auto next_last = last;
              auto next = curr_node;
              while (check_right) {
                  // go to next right leaf
                  curr_page = nodes[++next];
                  if (!is_sbt_leaf(*curr_page)) break;

                  length_type next_lcp;
                  std::tie(next_first, next_last, next_lcp) = internal_search_type::search_occ(*curr_page, T, P, 0);
                  if (next_lcp < lcp) break; // no more occurrences

                  // push all occurrences to output iterator
                  for (int i = next_first; i < next_last; ++i) {
                      *(out++) = internal_search_type::get_suffix(*curr_page, i);

                      // report matches with duplicate suffixes
                      if (curr_page->child[i] != -1) {
                          auto d_count = dups[curr_page->dup_start + curr_page->child[i]];
                          for (sbt_index_type d_idx = 0; d_idx < *d_count; ++d_idx) {
                              auto d = dups[curr_page->dup_start + curr_page->child[i] + 1 + d_idx];
                              *(out++) = *d;
                          } // while d_idx
                      }
                  } // for i

                  check_right = next_last == curr_page->size;
              } // while check_right

              // scan left
              next_first = first;
              next_last = last;
              next = curr_node;
              while (check_left) {
                  // go to next left leaf
                  if (next == 0) break;
                  curr_page = nodes[--next];

                  length_type next_lcp;
                  std::tie(next_first, next_last, next_lcp) = internal_search_type::search_occ(*curr_page, T, P, 0);
                  if (next_lcp < lcp) break; // no more occurrences

                  // push all occurrences to output iterator
                  for (int i = next_first; i < next_last; ++i) {
                      *(out++) = internal_search_type::get_suffix(*curr_page, i);

                      // report matches with duplicate suffixes
                      if (curr_page->child[i] != -1) {
                          auto d_count = dups[curr_page->dup_start + curr_page->child[i]];
                          for (sbt_index_type d_idx = 0; d_idx < *d_count; ++d_idx) {
                              auto d = dups[curr_page->dup_start + curr_page->child[i] + 1 + d_idx];
                              *(out++) = *d;
                          } // while d_idx
                      }
                  } // for i

                  check_left = next_first == 0;
              } // while check_left
          } // if nodes
      } // if lcp

      return lcp;
  } // longest_prefix_search

} // namespace sbt::detail

#endif // DETAIL_DNASBT_ALGORITHMS_HPP
