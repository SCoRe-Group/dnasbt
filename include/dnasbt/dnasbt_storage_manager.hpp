/***
 *  $Id$
 **
 *  File: dnasbt_storage_manager.hpp
 *  Author: Andrew Mikalsen <ajmikals@buffalo.edu>
 *          Jaroslaw Zola <jaroslaw.zola@hush.com>
 *
 *  Copyright (c) 2023 SCoRe Group
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of the DNAsbt package.
 */

#ifndef DNASBT_STORAGE_MANAGER_HPP
#define DNASBT_STORAGE_MANAGER_HPP

#include <cerrno>
#include <cstddef>
#include <cstdlib>
#include <cstring>
#include <exception>
#include <filesystem>
#include <new>
#include <string>
#include <utility>

#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#include <boost/iostreams/device/mapped_file.hpp>
#include <lz4.h>

#include <dnasbt/dnasbt_config.hpp>


namespace sbt {

  /// An alignment to which any pointer is properly aligned.
  constexpr std::size_t universal_alignment = 1;

  /// An alignment to the I/O block device.
  constexpr std::size_t block_alignment = DNASBT_DEV_BLOCK_SIZE;


  /// A simple storage manager abstracting access to a pointer, and serves as a concrete example of the storage_manager
  /// interface. In general, a storage manager abstracts the interfacing with storage to access a file-backed array.
  /// See this class's documentation for the storage_manager interface specification.
  ///
  /// \tparam T Type of value stored. Must be a POD type
  template <typename T>
  class basic_storage_manager {
  public:
      /// The type of managed values.
      using value_type = T;

      /// The type capable of holding the maximum possible number of managed values.
      using size_type = std::size_t;

      /// The byte alignment that the address passed to load must be aligned to.
      static constexpr inline std::size_t alignment = universal_alignment;

      /// A storage_manager has a default constructor that, in general, creates an uninitialized storage_manager.
      basic_storage_manager() = default;

      /// A storage_manager can't be copied.
      basic_storage_manager(const basic_storage_manager&) = delete;
      basic_storage_manager& operator=(const basic_storage_manager&) = delete;

      /// A storage manager can be moved.
      basic_storage_manager(basic_storage_manager&& other) noexcept : data_{other.data_}, size_{other.size_} {
          other.data_ = nullptr;
          other.size_ = 0;
      } // basic_storage_manager

      basic_storage_manager& operator=(basic_storage_manager&& other) noexcept {
          data_ = other.data_;
          size_ = other.size_;
          other.data_ = nullptr;
          other.size_ = 0;
          return *this;
      } // basic_storage_manager

      /// Creates a basic_storage_manager managing a pointer. The manager doesn't aquire ownership, so the owner is
      /// responsible for freeing memory if necessary. Every storage_manager has an implementation-specific constructor
      /// like this one that properly initializes the storage_manager.
      basic_storage_manager(const value_type* data, size_type size) : data_{data}, size_{size} { }

      /// Takes an index in [0..size()-1] and a possibly null pointer address. If address is non-null, then it's assumed
      /// that address doesn't alias any internal data used by the manager and that address is aligned to the alignment
      /// specified by the alignment static. Loads the value from the storage managed by this object at the specified
      /// index, returning a pair (ptr, own). If address is non-null and ptr == address, then the loaded data resides at
      /// the memory pointed to by address. Otherwise, if ptr isn't null, then the data resides in the memory location
      /// referred to by ptr. In this case, own == true indicates that the caller is responsible for managing the pointer
      /// ptr (e.g., freeing memory), otherwise ptr is managed by and valid for the lifetime of this object. If own is
      /// true, then ptr must be released using free (this is for portability reasons). The value of own has no meaning
      /// when ptr == address != null, but should be false for sane implementations. If an error occurs during the call
      /// to load, then the returned ptr will be null.
      ///
      /// For basic_storage_manager specifically, we will always have ptr != address and own == false.
      [[nodiscard]] std::pair<const value_type*, bool> load(size_type index, [[maybe_unused]] value_type* address) const {
          return std::make_pair(data_ + index, false);
      } // load

      [[nodiscard]] std::pair<const value_type*, bool> prefetch(size_type index, [[maybe_unused]] size_type count, [[maybe_unused]] value_type* address) const {
          return std::make_pair(data_ + index, false);
      } // prefetch

      /// Returns the number of values in the managed storage.
      [[nodiscard]] size_type size() const { return size_; }

  private:
      const value_type* data_;
      size_type size_;
  }; // class basic_storage_manager


  /// A storage manager emulating actual storage. The manager always returns a copy of the same object.
  ///
  /// \tparam T Type of value stored. Must be a POD type
  template <typename T>
  class null_storage_manager {
  public:
      using value_type = T;
      using size_type = std::size_t;
      static constexpr inline std::size_t alignment = universal_alignment;

      null_storage_manager() = default;

      null_storage_manager(const null_storage_manager&) = delete;
      null_storage_manager& operator=(const null_storage_manager&) = delete;

      null_storage_manager(null_storage_manager&& other) noexcept : size_{other.size_} {
          other.size_ = 0;
      } // null_storage_manager

      null_storage_manager& operator=(null_storage_manager&& other) noexcept {
          other.size_ = 0;
          return *this;
      } // null_storage_manager

      null_storage_manager(size_type size) : size_{size} { }

      [[nodiscard]] std::pair<const value_type*, bool> load(size_type index, [[maybe_unused]] value_type* address) const {
          auto ptr = static_cast<value_type*>(std::malloc(sizeof(value_type)));
          return std::make_pair(ptr, true);
      } // load

      [[nodiscard]] std::pair<const value_type*, bool> prefetch(size_type index, [[maybe_unused]] size_type count, [[maybe_unused]] value_type* address) const {
          auto ptr = static_cast<value_type*>(std::malloc(count * sizeof(value_type)));
          return std::make_pair(ptr, true);
      } // prefetch

      /// Returns the number of values in the managed storage.
      [[nodiscard]] size_type size() const { return size_; }

  private:
      size_type size_;
  }; // class null_storage_manager


  /// A storage manager abstracting access to storage backed by a memory mapped file.
  template <typename T>
  class mmap_storage_manager {
  public:
      using value_type = T;
      using size_type = std::size_t;

      static constexpr inline std::size_t alignment = universal_alignment;

      mmap_storage_manager() = default;

      mmap_storage_manager(const mmap_storage_manager&) = delete;
      mmap_storage_manager& operator=(const mmap_storage_manager&) = delete;

      mmap_storage_manager(mmap_storage_manager&& other) noexcept
          : mf_{other.mf_}, byte_count_{other.byte_count_} {
          other.mf_ = {};
          other.byte_count_ = 0;
      } // mmap_storage_manager

      mmap_storage_manager& operator=(mmap_storage_manager&& other) noexcept {
          mf_ = other.mf_;
          byte_count_ = other.byte_count_;

          other.mf_ = {};
          other.byte_count_ = 0;

          return *this;
      } // mmap_storage_manager

      /// Create a manager backed by the given file.
      mmap_storage_manager(const std::string& filename) {
          if (std::filesystem::file_size(filename) > 0) {
              mf_ = boost::iostreams::mapped_file_source{filename};
              byte_count_ = mf_.size();
              madvise(static_cast<void*>(const_cast<char*>(mf_.data())), byte_count_, MADV_RANDOM);
          } else {
              byte_count_ = 0;
          }
      } // mmap_manager

      [[nodiscard]] std::pair<const value_type*, bool> load(size_type index, value_type*) const {
          return {reinterpret_cast<const value_type*>(mf_.data()) + index, false};
      } // load

      [[nodiscard]] std::pair<const value_type*, bool> prefetch(size_type index, size_type count, value_type* address) const {
          uint64_t read_size = 1ULL * count * sizeof(value_type);

          value_type* allocation = address ? address
                                           : static_cast<value_type*>(std::aligned_alloc(alignment, read_size));

          if (!address && !allocation) {
              throw std::runtime_error(std::string{"memory allocation failed: "} + std::strerror(errno));
          }

          std::memcpy(address, mf_.data() + index, read_size);

          return {allocation, allocation != address};
      } // prefetch

      [[nodiscard]] size_type size() const { return (byte_count_ + sizeof(value_type) - 1) / sizeof(value_type); }

  private:
      boost::iostreams::mapped_file_source mf_;
      size_type byte_count_;

  }; // class mmap_storage_manager


  /// Abstracts access to a memory mapped file of compressed data blocks.
  template <typename T>
  class lz4_mmap_storage_manager {
  public:
      using value_type = T;
      using size_type = std::size_t;

      static constexpr inline std::size_t alignment = universal_alignment;

      lz4_mmap_storage_manager() = default;

      lz4_mmap_storage_manager(const lz4_mmap_storage_manager&) = delete;
      lz4_mmap_storage_manager& operator=(const lz4_mmap_storage_manager&) = delete;

      lz4_mmap_storage_manager(lz4_mmap_storage_manager&& other) noexcept : mf_{other.mf_}, cb_size_{other.cb_size_} {}

      lz4_mmap_storage_manager& operator=(lz4_mmap_storage_manager&& other) noexcept {
          mf_ = other.mf_;
          cb_size_ = other.cb_size_;
          return *this;
      } // lz4_mmap_storage_manager

      // Creates a new manager for the given file
      lz4_mmap_storage_manager(const std::string& filename)
          : mf_{filename}, cb_size_{*reinterpret_cast<const int*>(mf_.data())} {
          madvise(static_cast<void*>(const_cast<char*>(mf_.data())), mf_.size(), MADV_RANDOM);
      } // lz4_mmap_storage_manager

      [[nodiscard]] std::pair<const value_type*, bool> load(size_type index, value_type* address) const {
          // index of first byte of the serialized requested compressed block
          // (skips over a block of meta data)
          uint64_t i_c = DNASBT_DEV_BLOCK_SIZE + (1ULL * index * cb_size_);

          // read size of the requested unpadded compressed block (first int of serialized data)
          int bs = 0;
          std::memcpy(&bs, mf_.data() + i_c, sizeof(int));

          // we either use the provided address or create a new allocation
          // must ensure that the allocation is freed in the case of an error
          value_type* buffer = address ? address : static_cast<value_type*>(std::malloc(sizeof(value_type)));

          // we skip i_c bytes to get to the start of the serialized block, then skip over the bs int to get to
          // the compressed block
          auto res = LZ4_decompress_safe(mf_.data() + i_c + sizeof(int), reinterpret_cast<char*>(buffer),
                                         bs, sizeof(value_type));

          if (res < 0) {
              if (buffer != address) {
                  std::free(buffer);
              }
              throw std::runtime_error("failed to decompress block");
          }

          // if we made a new allocation, the caller must free it
          return {buffer, buffer != address};
      } // load

      [[nodiscard]] std::pair<const value_type*, bool> prefetch(size_type, size_type, value_type*) const {
          throw(std::logic_error("sorry, method not implemented!"));
          return {nullptr, false};
      } // prefetch

      [[nodiscard]] size_type size() const { return (mf_.size() - DNASBT_DEV_BLOCK_SIZE) / cb_size_; }

  private:
      boost::iostreams::mapped_file_source mf_;
      int cb_size_; // size of a compressed block
  }; // class lz4_mmap_storage_manager


#ifdef __linux__

  /// A storage manager abstracting access to storage backed by a file, accessed via direct I/O.
  template <typename T>
  class direct_io_storage_manager {
  public:
      using value_type = T;
      using size_type = std::size_t;

      static constexpr inline std::size_t alignment = block_alignment;

      /// Creates an uninitialized disk_manager
      direct_io_storage_manager() noexcept : byte_count_{0}, fd_{-1} {};

      ~direct_io_storage_manager() {
          [[maybe_unused]] auto res_close = ::close(fd_);
          assert(fd_ == -1 || res_close >= 0);
      } // ~direct_io_storage_manager

      direct_io_storage_manager(const direct_io_storage_manager&) = delete;
      direct_io_storage_manager& operator=(const direct_io_storage_manager&) = delete;

      direct_io_storage_manager(direct_io_storage_manager&& other) noexcept
          : direct_io_storage_manager() {
          swap(*this, other);
      } // direct_io_storage_manager

      direct_io_storage_manager& operator=(direct_io_storage_manager&& other) noexcept {
          swap(*this, other);
          return *this;
      } // operator=

      direct_io_storage_manager(const std::string& filename)
          : byte_count_{std::filesystem::file_size(filename)}, fd_{::open(filename.c_str(), O_RDONLY | O_LARGEFILE | O_DIRECT)} {
          if (fd_ < 0) throw std::runtime_error{"could not open file " + filename + ": " + std::strerror(errno)};
      } // direct_io_storage_manager

      [[nodiscard]] std::pair<const value_type*, bool> load(size_type index, value_type* address) const {
          return prefetch(index, 1, address);
      } // load

      [[nodiscard]] std::pair<const value_type*, bool> prefetch(size_type index, size_type count, value_type* address) const {
          // direct I/O won't work if the value_type isn't properly aligned
          static_assert(sizeof(value_type) % alignment == 0,
                        "The value_type for direct_io_storage_manager must be a multiple of its alignment");

          uint64_t alloc_size = 1ULL * count * sizeof(value_type);

          // we either use the given address or create a new allocation
          // if a new allocation, must ensure that the allocation is freed in the case of an error
          value_type* allocation = address ? address
                                           : static_cast<value_type*>(std::aligned_alloc(alignment, alloc_size));

          if (!address && !allocation) {
              throw std::runtime_error(std::string{"memory allocation failed: "} + std::strerror(errno));
          }

          std::memset(allocation, 0, alloc_size);

          uint64_t read_size = 1ULL * count * sizeof(value_type);
          uint64_t offset = 1ULL * index * sizeof(value_type);
          uint64_t buf_pos = 0;

          char* buffer = reinterpret_cast<char*>(allocation);

          while (read_size > 0) {
              auto res_read = ::pread64(fd_, buffer + buf_pos, read_size, offset + buf_pos);

              if (res_read < 0) {
                  if (allocation != address) {
                      std::free(allocation);
                  }
                  throw std::runtime_error{std::string{"could not load block, read size=" + std::to_string(read_size)
                                                       + " offset=" + std::to_string(offset + buf_pos)
                                                       + ", reading failed: " + std::strerror(errno)}};
              }

              if (res_read < read_size) break;

              buf_pos += res_read;
              read_size -= res_read;
          } // while read

          return {allocation, allocation != address};
      } // prefetch

      [[nodiscard]] size_type size() const { return (byte_count_ + sizeof(value_type) - 1) / sizeof(value_type); }

  private:
      friend void swap(direct_io_storage_manager& lhs, direct_io_storage_manager& rhs) noexcept {
          std::swap(lhs.byte_count_, rhs.byte_count_);
          std::swap(lhs.fd_, rhs.fd_);
      } // swap

      size_type byte_count_;
      int fd_;

  }; // class direct_io_storage_manager


  /// A storage manager abstracting access to a file of compressed data blocks, accessed via direct I/O.
  template <typename T>
  class lz4_direct_io_storage_manager {
  public:
      using value_type = T;
      using size_type = std::size_t;

      static constexpr inline std::size_t alignment = 1;

      /// Creates an uninitialized disk_manager
      lz4_direct_io_storage_manager() noexcept : byte_count_{0}, fd_{-1}, cb_size_{0} {}

      ~lz4_direct_io_storage_manager() {
          [[maybe_unused]] auto res_close = ::close(fd_);
          assert(fd_ == -1 || res_close >= 0);
      } // ~lz4_direct_io_storage_manager

      lz4_direct_io_storage_manager(const lz4_direct_io_storage_manager&) = delete;

      lz4_direct_io_storage_manager& operator=(const lz4_direct_io_storage_manager&) = delete;

      lz4_direct_io_storage_manager(lz4_direct_io_storage_manager&& other) noexcept : lz4_direct_io_storage_manager() {
          swap(*this, other);
      } // lz4_direct_io_storage_manager

      lz4_direct_io_storage_manager& operator=(lz4_direct_io_storage_manager&& other) noexcept {
          swap(*this, other);
          return *this;
      } // operator=

      lz4_direct_io_storage_manager(const std::string& filename)
          : byte_count_{std::filesystem::file_size(filename)}, fd_{::open(filename.c_str(), O_RDONLY | O_LARGEFILE | O_DIRECT)} {

          if (fd_ < 0) throw std::runtime_error{"could not open file " + filename + ": " + std::strerror(errno)};

          static_assert(block_alignment >= sizeof(int), "Device block size is too small!");

          // must ensure that the buffer is freed in the case of an error
          void* buffer = std::aligned_alloc(block_alignment, block_alignment);

          if (!buffer) {
              throw std::runtime_error(std::string{"memory allocation failed: "} + std::strerror(errno));
          }

          auto res_read = ::pread64(fd_, buffer, block_alignment, 0);

          if (res_read < 0) {
              std::free(buffer);
              throw std::runtime_error{std::string{"could not load block, reading failed: "} + std::strerror(errno)};
          }

          cb_size_ = *static_cast<int*>(buffer);
          std::free(buffer);
      } // lz4_direct_io_storage_manager

      [[nodiscard]] std::pair<const value_type*, bool> load(size_type index, value_type* address) const {
          return prefetch(index, 1, address);
      } // load

      [[nodiscard]] std::pair<const value_type*, bool> prefetch(size_type index, size_type count, value_type* address) const {
          // direct I/O won't work if the value_type isn't properly aligned
          static_assert(sizeof(value_type) % alignment == 0,
                        "The value_type for direct_io_storage_manager must be a multiple of its alignment");

          uint64_t alloc_size = 1ULL * count * sizeof(value_type);

          // we either use the given address or create a new allocation
          // must ensure that the allocation is freed in the case of an error
          value_type* allocation = address ? address
                                           : static_cast<value_type*>(std::malloc(alloc_size));

          uint64_t read_size = 1ULL * count * cb_size_;

          // we use a temporary heap-allocated buffer to load the compressed data from disk
          // must ensure that the buffer is freed in the case of an error
          char* buffer = static_cast<char*>(std::aligned_alloc(block_alignment, read_size));

          if (!buffer) {
              throw std::runtime_error(std::string{"memory allocation failed: "} + std::strerror(errno));
          }

          uint64_t offset = 1ULL * DNASBT_DEV_BLOCK_SIZE + (index * cb_size_);
          uint64_t buf_pos = 0;

          // we have to account for transfer limit in pread64
          while (read_size > 0) {
              auto res_read = ::pread64(fd_, buffer + buf_pos, read_size, offset + buf_pos);

              if (res_read < 0) {
                     if (!address) {
                         std::free(allocation);
                     }
                     std::free(buffer);
                     throw std::runtime_error{std::string{"could not load block, reading failed: "} + std::strerror(errno)};
              } else {
                  read_size -= res_read;
                  buf_pos += res_read;
              }
          } // while read

          for (size_type i = 0; i < count; ++i) {
              auto buf_i = buffer + i * cb_size_;

              // read the size of the requested unpadded compressed block (first int of serialized data)
              int bs = 0;
              std::memcpy(&bs, buf_i, sizeof(int));

              // skip over the bs int to get to the compressed block
              auto res = LZ4_decompress_safe(buf_i + sizeof(int), reinterpret_cast<char*>(allocation + i),
                                             bs, sizeof(value_type));

              if (res < 0) {
                  if (!address) {
                      std::free(allocation);
                  }
                  std::free(buffer);
                  throw std::runtime_error{"failed to decompress block " + std::to_string(i)};
              }
          } // for i

          std::free(buffer);

          return {allocation, allocation != address};
      } // prefetch

      [[nodiscard]] size_type size() const { return (byte_count_ - DNASBT_DEV_BLOCK_SIZE) / cb_size_; }

  private:
      friend void swap(lz4_direct_io_storage_manager& lhs, lz4_direct_io_storage_manager& rhs) {
          std::swap(lhs.byte_count_, rhs.byte_count_);
          std::swap(lhs.fd_, rhs.fd_);
          std::swap(lhs.cb_size_, rhs.cb_size_);
      } // swap

      size_type byte_count_;
      int fd_;
      int cb_size_; // size of a compressed block

  }; // class lz4_direct_io_storage_manager

#endif // __linux__

} // namespace sbt

#endif // DNASBT_STORAGE_MANAGER_HPP
